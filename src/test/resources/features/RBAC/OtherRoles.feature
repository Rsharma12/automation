Feature: Other permissions for RBAC roles
  
@otherPerms
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo

@otherPerms
Scenario: Create role for all other permissions like Manager case approver, Case Shredding
When user go to administration tab and select roles
And user creates an active new role
When user enables configuration admin role
And user enables Data Integration Admin role
And user enables manager case approver
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify Configuration Admin permission
And verify Data Import Admin permission
And verify Manager Case Approver permission