@tag
Feature: Verifing the role can access insights reporting
  
@ReportingInsights
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	
@ReportingInsights
Scenario: Create role who can access insights reporting
When user go to administration tab and select roles
And user creates an active new role
When user enables insights reporting
And user selects option insights reporting "Case type Limited" and edit details
And user selects Case Type options for Insights Reporting
|ER|PH|INV|
And save insights reporting "Case Type" permissions
And user selects "Insights Reporting" Group access as "All"
And user selects "Insights Reporting" Location access as "All"
And user selects "Insights Reporting" Issue access as "All"
And user enables Insights Analytics
And user enables Insights Dashboard 
And user enables Insights Benchmark
And user enables Insights Reporting Involved Party Details
And user enables Insights Reporting Event Based Insights
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify Case Type permissions for "Insights Reporting" role
Then verify "Insights Reporting" role has Limited Group Access with "All" option
And verify "Insights Reporting" role has Limited Issue Access with "All" option
And verify "Insights Reporting" role has Limited Location Access with "All" option
	
When user click on Edit Permissions
And user selects option insights reporting "Case type Limited" and edit details
And user selects Case Type options for Insights Reporting
|ER|PH|
And save insights reporting "Case Type" permissions
And user selects Group access as Limited and edit details for "Insights Reporting" role
When user selects "Insights Reporting" role can view cases with "only Group selected"
And save insights reporting "Group access" permissions
And user selects Issue access as Limited and edit details for "Insights Reporting" role
When user selects "Insights Reporting" role can view cases with "only Issue selected"
And save insights reporting "Issue access" permissions
When user selects Location access as Limited and edit details for "Insights Reporting" role
And user selects "Insights Reporting" role can view cases with "only Location selected"
And save insights reporting "Location access" permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify Case Type permissions for "Insights Reporting" role
Then verify "Insights Reporting" role has Limited Group Access with "only Group selected" option
And verify "Insights Reporting" role has Limited Issue Access with "only Issue selected" option
And verify "Insights Reporting" role has Limited Location Access with "only Location selected" option

When user click on Edit Permissions
And user selects option insights reporting "Case type Limited" and edit details
And user selects Case Type options for Insights Reporting
|ER|INV|
And save insights reporting "Case Type" permissions
And user selects Group access as Limited and edit details for "Insights Reporting" role
When user selects "Insights Reporting" role can view cases with "All Group Except selected"
And save insights reporting "Group access" permissions
And user selects Issue access as Limited and edit details for "Insights Reporting" role
When user selects "Insights Reporting" role can view cases with "All Issues Except selected"
And save insights reporting "Issue access" permissions
When user selects Location access as Limited and edit details for "Insights Reporting" role
And user selects "Insights Reporting" role can view cases with "All Location Except selected"
And save insights reporting "Location access" permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify Case Type permissions for "Insights Reporting" role
Then verify "Insights Reporting" role has Limited Group Access with "All Group Except selected" option
And verify "Insights Reporting" role has Limited Issue Access with "All Issues Except selected" option
And verify "Insights Reporting" role has Limited Location Access with "All Location Except selected" option
#


