@tag
Feature: Roles and permissions to view cases

@ViewCases
Scenario: Login into HR Acuity site
Given User is in HRAcuity demo

@ViewCases
Scenario: Create User role who can view ALL Cases and Case Type is ER, PH
When user go to administration tab and select roles
And user creates an active new role
When user enables view cases
And user selects option view cases "Case type Limited" and edit details
And user selects Case Type options for View Case
|ER|PH|
And save view case "Case Type" permissions
And user selects "View Cases" Group access as "All"
And user selects "View Cases" Location access as "All"
And user selects "View Cases" Issue access as "All"
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "View Cases" role has Limited Group Access with "All" option
And verify "View Cases" role has Limited Issue Access with "All" option
And verify "View Cases" role has Limited Location Access with "All" option
#	
When user click on Edit Permissions
And user selects option view cases "Case type Limited" and edit details
And user selects Case Type options for View Case
|ER|INV|EI|
And save view case "Case Type" permissions
And user selects Group access as Limited and edit details for "View Cases" role
When user selects "View Cases" role can view cases with "only Group selected"
And save view case "Group access" permissions
And user selects Issue access as Limited and edit details for "View Cases" role
When user selects "View Cases" role can view cases with "only Issue selected"
And save view case "Issue access" permissions
When user selects Location access as Limited and edit details for "View Cases" role
And user selects "View Cases" role can view cases with "only Location selected"
And save view case "Location access" permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify Case Type permissions for "View Cases" role
Then verify "View Cases" role has Limited Group Access with "only Group selected" option
And verify "View Cases" role has Limited Issue Access with "only Issue selected" option
And verify "View Cases" role has Limited Location Access with "only Location selected" option
#
When user click on Edit Permissions
And user selects option view cases "Case type Limited" and edit details
And user selects Case Type options for View Case
|ER|INV|
And save view case "Case Type" permissions
And user selects Group access as Limited and edit details for "View Cases" role
When user selects "View Cases" role can view cases with "All Group Except selected"
And save view case "Group access" permissions
And user selects Issue access as Limited and edit details for "View Cases" role
When user selects "View Cases" role can view cases with "All Issues Except selected"
And save view case "Issue access" permissions
When user selects Location access as Limited and edit details for "View Cases" role
And user selects "View Cases" role can view cases with "All Location Except selected"
And save view case "Location access" permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "View Cases" role has Limited Group Access with "All Group Except selected" option
And verify "View Cases" role has Limited Issue Access with "All Issues Except selected" option
And verify "View Cases" role has Limited Location Access with "All Location Except selected" option