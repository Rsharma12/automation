@tag
Feature: Roles and permission for new user

@MPRoles
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	
  @MPRoles
  Scenario: Create Manager Portal User role with Group and Location access
  When user go to administration tab and select roles
  And user creates an active new role
  And user enables Manager Portal User role
  And user selects "Manager Portal" Group access as "All"
  And user selects "Manager Portal" Location access as "All"
  And user saves role permissions
  And user go to administration tab and select roles
  And user search for the role
  And user click on quick view permissions
  Then verify "manager portal" role has Group Access "All"
  And verify "manager portal" role has Location Access "All"
 #
  When user click on Edit Permissions
  And user selects Group access as Limited and edit details for "Manager Portal" role
  When user selects "Manager Portal" role can view cases with "only Group selected"
  And save manager portal "group" permissions
  When user selects Location access as Limited and edit details for "Manager Portal" role
  And user selects "Manager Portal" role can view cases with "only Location selected"
  And save manager portal "location" permissions
  And user saves role permissions
  When user go to administration tab and select roles
  And user search for the role
  And user click on quick view permissions 
  Then verify "manager portal" role has Limited Group Access with "only Group selected" option
  And verify "manager portal" role has Limited Location Access with "only Location selected" option
  
  When user click on Edit Permissions
  And user selects Group access as Limited and edit details for "Manager Portal" role
  When user selects "Manager Portal" role can view cases with "All Group Except selected"
  And save manager portal "group" permissions
  When user selects Location access as Limited and edit details for "Manager Portal" role
  And user selects "Manager Portal" role can view cases with "All Location Except selected"
  And save manager portal "location" permissions
  And user saves role permissions
  When user go to administration tab and select roles
  And user search for the role
  And user click on quick view permissions 
  Then verify "manager portal" role has Limited Group Access with "All Group Except selected" option
  And verify "manager portal" role has Limited Location Access with "All Location Except selected" option
  
  @MPRoles
  Scenario: Create Manager Portal User role with Group as ALL Access and Location access as Limited
  When user go to administration tab and select roles
  And user creates an active new role
  And user enables Manager Portal User role
  And user selects "Manager Portal" Group access as "All"
  When user selects Location access as Limited and edit details for "Manager Portal" role
  And user selects "Manager Portal" role can view cases with "only Location selected"
  And save manager portal "location" permissions
  And user saves role permissions
  When user go to administration tab and select roles
  And user search for the role
  And user click on quick view permissions
  Then verify "manager portal" role has Group Access "All"
  And verify "manager portal" role has Limited Location Access with "only Location selected" option
  
  @MPRoles
  Scenario: Create Manager Portal User role with Group Access is Limited and Location access All
  When user go to administration tab and select roles
  And user creates an active new role
  And user enables Manager Portal User role
   And user selects Group access as Limited and edit details for "Manager Portal" role
  When user selects "Manager Portal" role can view cases with "only Group selected"
  And save manager portal "group" permissions
  And user saves role permissions
  When user go to administration tab and select roles
  And user search for the role
  And user click on quick view permissions
  Then verify "manager portal" role has Limited Group Access with "only Group selected" option
  And verify "manager portal" role has Location Access "All"
  
  