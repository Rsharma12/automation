@tag
Feature: Validating ribbon for ER cases
 @CiRibbonER
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	And user click on "ER" tab
	And select "ER" case to open case detail page

   @CiRibbonER
 Scenario Outline: Validating ribbon icons with for ER cases
   When user click on "Chat" icon
   And type a "<message>" and press enter to send message
   And close "Chat" popup window
   When user click on "Case Health" icon
   And user get health percentage
   And close "Case Health" popup window
   When user click on "Case Log" icon
   And fill mandatory information of case log "Case Log"
   And close "Case log" popup window
   When user click on "Expense" icon
   And fill mandatory information of expenses "Expense"
   And close "Expense" popup window
   When user click on "Letter Template" icon
   And fill mandatory information to generate "Letter Template"
   And close "Letter Template" popup window
   When user click on "Schedule Reminder" icon
   And fill mandatory info to set reminder
   #When user click on "Additional Information" icon
   #Then validate additional information is added
   #And close "Additional Information" popup window
   
    Examples: 
     |message|
     |@kapinder vats Hello|

 @CiRibbonINV
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	And user click on "INV" tab
	And select "INV" case to open case detail page

   @CiRibbonINV
 Scenario Outline: Validating ribbon icons with for INV cases
   #When user click on "Chat" icon
   #And type a "<message>" and press enter to send message
   #And close "Chat" popup window
   #When user click on "Case Health" icon
   #And user get health percentage
   #And close "Case Health" popup window
   #When user click on "Case Log" icon
   #And fill mandatory information of case log "Case Log"
   #And close "Case log" popup window
   #When user click on "Expense" icon
   #And fill mandatory information of expenses "Expense"
   #And close "Expense" popup window
   #When user click on "Letter Template" icon
   #And fill mandatory information to generate "Letter Template"
   #And close "Letter Template" popup window
   #When user click on "Schedule Reminder" icon
   #And fill mandatory info to set reminder
   When user click on "Case Folder" icon
   And user click on Involved party to add Involved party
   And fill mandetory information for Involved party
   And user click on Issues to add Issues
   And fill mandetory information for Issue popup
   And close "Case Folder" popup window
   #When user click on "Additional Information" icon
   #Then validate additional information is added
   #And close "Additional Information" popup window



    Examples: 
     |message|
     |@kapinder vats Hello|
  
