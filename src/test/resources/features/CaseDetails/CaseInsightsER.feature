Feature: Dashboard for Case Insights for ER cases and Tiles validation

@CaseInsights
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	And user click on case category "My cases" tab
	And user click on "ER" tab
	And select "ER" case to open case detail page
  
  @CaseInsights
  Scenario Outline: Case Insights for ER cases and Tiles validations including Filtered Results on Selected tags
    When user select case type "ER" from case insights section
    Then verify selected case type "ER" on the case insights bar
    When user selects Time Frame "3"
    Then verify rolling month "3" is visible on the case insights bar
    When user click on "<Tiles>" and get the count of cases
    Then verify "<Tiles>" is selected as a tag
    When user click on apply in Selected Tags section
    Then verify output in filtered results section
    When user selects Time Frame "6"
    Then verify rolling month "6" is visible on the case insights bar
    When user get the total count of cases from "<Tiles>" tiles
    And user click on apply in Selected Tags section
    Then verify output in filtered results section
    When user selects Time Frame "12"
    Then verify rolling month "12" is visible on the case insights bar
    When user get the total count of cases from "<Tiles>" tiles
    And user click on apply in Selected Tags section
    Then verify output in filtered results section
    When user selects Time Frame "24"
    Then verify rolling month "24" is visible on the case insights bar
    When user get the total count of cases from "<Tiles>" tiles
    And user click on apply in Selected Tags section
    Then verify output in filtered results section
    When user removes "<Tiles>" from selected tag
    
    Examples: 
 		|Tiles|
    |Group|
   	|Location|
   	|Issue Category|
   	|Involved Party|
   	|Action|
   	|Notification Method|
			