package com.HRA.pageObjects;
import static com.HRA.testBase.TestBase.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.HRA.helper.HighlightElementManager;
import com.HRA.helper.ReportingUtils;
import com.HRA.or.CiRibbonOR;
import com.relevantcodes.extentreports.LogStatus;
import lombok.Data;
@Data

public class CiRibbonPage  {
	
	WebDriver driver;

	public CiRibbonPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = CiRibbonOR.selectDate) public WebElement selectDate;
	
	@FindBy(xpath = CiRibbonOR.chat) public WebElement chat;
	@FindBy(xpath = CiRibbonOR.chatTextBox) public WebElement chatTextBox;
	@FindBy(xpath = CiRibbonOR.reply) public WebElement reply;
	@FindBy(xpath = CiRibbonOR.replyBox) public WebElement replyBox;
	
	@FindBy(xpath = CiRibbonOR.caseHealth) public WebElement caseHealth;
	@FindBy(xpath = CiRibbonOR.caseHealthPercentage) public WebElement caseHealthPercentage;
	
	@FindBy(xpath = CiRibbonOR.caseLog) public WebElement caseLog;
	@FindBy(xpath = CiRibbonOR.calendarIcon) public WebElement calendarIcon;
	@FindBy(xpath = CiRibbonOR.caseLogCategory) public WebElement caseLogCategory;
	@FindBy(xpath = CiRibbonOR.hoursSpent) public WebElement hoursSpent;
	@FindBy(xpath = CiRibbonOR.notes) public WebElement notes;
	@FindBy(xpath = CiRibbonOR.saveCaseLog) public WebElement saveCaseLog;
	
	@FindBy(xpath = CiRibbonOR.expense) public WebElement expense;
	@FindBy(xpath = CiRibbonOR.expenseCalenderIcon) public WebElement expenseCalenderIcon;
	@FindBy(xpath = CiRibbonOR.expenseCategory) public WebElement expenseCategory;
	@FindBy(xpath = CiRibbonOR.expenseAmount) public WebElement expenseAmount;
	@FindBy(xpath = CiRibbonOR.expenseSpecific) public WebElement expenseSpecific;
	@FindBy(xpath = CiRibbonOR.expenseLetterTmplt) public WebElement expenseLetterTmplt;
	@FindBy(xpath = CiRibbonOR.expenseSave) public WebElement expenseSave;
	
	@FindBy(xpath = CiRibbonOR.letterTemplate) public WebElement letterTemplate;
	@FindBy(xpath = CiRibbonOR.template) public WebElement template;
	@FindBy(xpath = CiRibbonOR.involvedParty) public WebElement involvedParty;
	@FindBy(xpath = CiRibbonOR.generateLetter) public WebElement generateLetter;
	@FindBy(xpath = CiRibbonOR.sendOption) public WebElement sendOption;
	@FindBy(xpath = CiRibbonOR.optionEmail) public WebElement optionEmail;
	@FindBy(xpath = CiRibbonOR.sendEmailToOthers) public WebElement sendEmailToOthers;
	@FindBy(xpath = CiRibbonOR.personEmailId) public WebElement personEmailId;
	@FindBy(xpath = CiRibbonOR.sendMail) public WebElement sendMail;
	@FindBy(xpath = CiRibbonOR.mailOk) public WebElement mailOk;
	@FindBy(xpath = CiRibbonOR.letterSave) public WebElement letterSave;
	
	
	@FindBy(xpath = CiRibbonOR.scheduleReminder) public WebElement scheduleReminder;
	@FindBy(xpath = CiRibbonOR.reminderDate) public WebElement reminderDate;
	@FindBy(xpath = CiRibbonOR.selectReminderDate) public WebElement selectReminderDate;
	@FindBy(xpath = CiRibbonOR.reminderNotes) public WebElement reminderNotes;
	@FindBy(xpath = CiRibbonOR.sendToOthers) public WebElement sendToOthers;
	@FindBy(xpath = CiRibbonOR.personEmail) public WebElement personEmail;
	@FindBy(xpath = CiRibbonOR.reminderMessage) public WebElement reminderMessage;
	@FindBy(xpath = CiRibbonOR.reminderSave) public WebElement reminderSave;
	
	@FindBy(xpath = CiRibbonOR.caseReport) public WebElement caseReport;
	
	@FindBy(xpath = CiRibbonOR.caseFolder) public WebElement caseFolder;
	@FindBy(xpath = CiRibbonOR.caseFolderInvolvedParty) public WebElement caseFolderInvolvedParty;
	@FindBy(xpath = CiRibbonOR.involvedPartyOpen) public WebElement involvedPartyOpen;
	@FindBy(xpath = CiRibbonOR.involvedPartyClose) public WebElement involvedPartyClose;
	@FindBy(xpath = CiRibbonOR.involvedPartyGearIcon) public WebElement involvedPartyGearIcon;
	@FindBy(xpath = CiRibbonOR.invPrtyEditView) public WebElement invPrtyEditView;
	@FindBy(xpath = CiRibbonOR.invPrtyRoleList) public WebElement invPrtyRoleList;
	@FindBy(xpath = CiRibbonOR.invPrtyContactNum) public WebElement invPrtyContactNum;
	@FindBy(xpath = CiRibbonOR.invPrtySave) public WebElement invPrtySave;
	
	@FindBy(xpath = CiRibbonOR.issueAllegation) public WebElement issueAllegation;
	@FindBy(xpath = CiRibbonOR.issuesOpen) public WebElement issuesOpen;
	@FindBy(xpath = CiRibbonOR.issuesClose) public WebElement issuesClose;
	@FindBy(xpath = CiRibbonOR.issueExplain) public WebElement issueExplain;
	
	@FindBy(xpath = CiRibbonOR.documents) public WebElement documents;
	@FindBy(xpath = CiRibbonOR.addDocuments) public WebElement addDocuments;
	@FindBy(xpath = CiRibbonOR.selectDoc) public WebElement selectDoc;
	@FindBy(xpath = CiRibbonOR.uploadDoc) public WebElement uploadDoc;
	@FindBy(xpath = CiRibbonOR.selectTypeOfDoc) public WebElement selectTypeOfDoc;
	@FindBy(xpath = CiRibbonOR.saveCaseFolderDoc) public WebElement saveCaseFolderDoc;
	
	@FindBy(xpath = CiRibbonOR.addPolicy) public WebElement addPolicy;
	@FindBy(xpath = CiRibbonOR.selectPolicy) public WebElement selectPolicy;
	@FindBy(xpath = CiRibbonOR.policySpecificTxt) public WebElement policySpecificTxt;
	@FindBy(xpath = CiRibbonOR.uploadDocLib) public WebElement uploadDocLib;
	@FindBy(xpath = CiRibbonOR.selectDocLib) public WebElement selectDocLib;
	@FindBy(xpath = CiRibbonOR.savePolicy) public WebElement savePolicy;
	
	@FindBy(xpath = CiRibbonOR.additionalInformation) public WebElement additionalInformation;
//	@FindBy(xpath = CiRibbonOR.chat) public WebElement chat;
//	@FindBy(xpath = CiRibbonOR.chat) public WebElement chat;
//	@FindBy(xpath = CiRibbonOR.chat) public WebElement chat;

	public void clickOnRibbonIcon(String icon) throws IOException, Throwable {
		if(icon.equalsIgnoreCase("Chat")){
			HighlightElementManager.highlightElement(driver, chat);
			click(chat);
			HighlightElementManager.unHighlightElement(driver, chat);
		}else if(icon.equalsIgnoreCase("Case Health")){
			HighlightElementManager.highlightElement(driver, caseHealth);
			click(caseHealth);
			HighlightElementManager.unHighlightElement(driver, caseHealth);
		}else if(icon.equalsIgnoreCase("Case Log")){
			HighlightElementManager.highlightElement(driver, caseLog);
			click(caseLog);
			HighlightElementManager.unHighlightElement(driver, caseLog);
		}else if(icon.equalsIgnoreCase("Expense")){
			HighlightElementManager.highlightElement(driver, expense);
			click(expense);
			HighlightElementManager.unHighlightElement(driver, expense);
		}else if(icon.equalsIgnoreCase("Letter Template")){
			HighlightElementManager.highlightElement(driver, letterTemplate);
			click(letterTemplate);
			HighlightElementManager.unHighlightElement(driver, letterTemplate);
		}else if(icon.equalsIgnoreCase("Schedule Reminder")){
			HighlightElementManager.highlightElement(driver, scheduleReminder);
			click(scheduleReminder);
			HighlightElementManager.unHighlightElement(driver, scheduleReminder);
		}else if(icon.equalsIgnoreCase("Case Report")){
			HighlightElementManager.highlightElement(driver, caseReport);
			click(caseReport);
			HighlightElementManager.unHighlightElement(driver, caseReport);
		}else if(icon.equalsIgnoreCase("Case Folder")){
			HighlightElementManager.highlightElement(driver, caseFolder);
			click(caseFolder);
			HighlightElementManager.unHighlightElement(driver, caseFolder);
		}else if(icon.equalsIgnoreCase("Additional Information")){
			HighlightElementManager.highlightElement(driver, additionalInformation);
			click(additionalInformation);
			HighlightElementManager.unHighlightElement(driver, additionalInformation);
		}
	ReportingUtils.log(driver, LogStatus.PASS, "User clicked on " + icon, false);
	
		
	}

	public void chatBox(String message) throws InterruptedException, IOException {
		try{
			input(chatTextBox, message);
			chatTextBox.sendKeys(Keys.ENTER);
			TimeUnit.SECONDS.sleep(3);
			click(reply);
			input(replyBox, "Reply message as user");
			replyBox.sendKeys(Keys.ENTER);
			ReportingUtils.log(driver, LogStatus.PASS, "Chat as user successful ", true);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Chat as user failed ", true);
		}
	
	}

	public void closePopupWindow(String icon) throws Throwable {
		ReportingUtils.log(driver, LogStatus.INFO, "Popup window of " + icon + " before closing.", true);
		if(icon.equalsIgnoreCase("Chat")){
			HighlightElementManager.highlightElement(driver, chat);
			click(chat);
			HighlightElementManager.unHighlightElement(driver, chat);
		}else if(icon.equalsIgnoreCase("Case Health")){
			click(caseHealth);
		}else if(icon.equalsIgnoreCase("Case Log")){
			click(caseLog);
		}else if(icon.equalsIgnoreCase("Expense")){
			click(expense);
		}else if(icon.equalsIgnoreCase("Letter Template")){
			click(letterTemplate);
		}else if(icon.equalsIgnoreCase("Schedule Reminder")){
			click(scheduleReminder);
		}else if(icon.equalsIgnoreCase("Case Report")){
			click(caseReport);
		}else if(icon.equalsIgnoreCase("Case Folder")){
			click(caseFolder);
		}else if(icon.equalsIgnoreCase("Additional Information")){
			click(additionalInformation);
		}
	ReportingUtils.log(driver, LogStatus.PASS, "User closed the popup window of " + icon + " successfully.", false);
	
	}

	public void getHealthPercentage() throws IOException {
		try{
			System.out.println("case health percentage is " + caseHealthPercentage.getText());
			HighlightElementManager.highlightElement(driver, caseHealthPercentage);
			ReportingUtils.log(driver, LogStatus.PASS, "Case health percentage is - " + caseHealthPercentage.getText(), true);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Case health percentage is - " + caseHealthPercentage.getText(), true);
		}

	}

	public void fillInfoCaseLog(String icon) throws IOException {
		try{
			click(calendarIcon);
			click(selectDate);
			Select selectCategory = new Select(caseLogCategory);
			selectCategory.selectByVisibleText("Document Review");
			click(hoursSpent);
			input(hoursSpent, "10.00");
			
			click(notes);
			input(notes,"Test Notes");
			
			ReportingUtils.log(driver, LogStatus.PASS, "User filled the Case Log information ", true);
			click(saveCaseLog);
			
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User filled the Case Log information ", true);
		}
	}

	public void fillExpenseInfo(String expense) throws IOException {
		try{
			click(expenseCalenderIcon);
			click(selectDate);
						
			Select selectCategory = new Select(expenseCategory);
			selectCategory.selectByVisibleText("Travel");
						
			click(expenseAmount);
			input(expenseAmount, "1000.00");
			
			click(expenseSpecific);
			expenseSpecific.clear();
			input(expenseSpecific,"Traveled from Boston to California");
			
			ReportingUtils.log(driver, LogStatus.PASS, "User filled the Expense information  ", true);
			click(expenseSave);
			
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User filled the Expense information  ", true);
		}
		
	}

	public void genrateLetterTmplt(String letterTemplate2) throws IOException {
		try{
			
			Select selectTemplate = new Select(template);
			selectTemplate.selectByVisibleText("Manager New Template");
			
			Select selectInvovedParty = new Select(involvedParty);
			selectInvovedParty.selectByIndex(1);
					
			click(generateLetter);
			ReportingUtils.log(driver, LogStatus.PASS, "User generated letter templated sucessfully for involved party ", true);
			HighlightElementManager.highlightElement(driver, letterSave);
			click(letterSave);
				
	/*		click(sendOption);
			click(optionEmail);
			
			click(sendEmailToOthers);
			click(personEmailId);
		
			input(personEmailId,"Renu");
			
			personEmailId.sendKeys(Keys.ARROW_DOWN);
			personEmailId.sendKeys(Keys.ENTER);
			
			HighlightElementManager.highlightElement(driver, sendMail);
			ReportingUtils.log(driver, LogStatus.PASS, "Generate letter and send Email ", true);
			click(sendMail);
			ReportingUtils.log(driver, LogStatus.PASS, "Confirm OK to send Email ", true);
			click(mailOk);*/
			
		}catch(Exception e){
			
			ReportingUtils.log(driver, LogStatus.FAIL, "Failed to generate letter template information - " + e.getMessage(), true);
		}
		
	}

	public void setReminderInfo() throws IOException {
		
		try{
		
			click(reminderDate);
			click(selectReminderDate);
			
			click(reminderNotes);
			reminderNotes.clear();
			input(reminderNotes, "Test reminder to take interview of involved party");
			
			click(sendToOthers);
			click(personEmail);
			input(personEmail,"Renu");
			TimeUnit.SECONDS.sleep(2);
			personEmail.sendKeys(Keys.ARROW_DOWN);
			personEmail.sendKeys(Keys.ENTER);
			TimeUnit.SECONDS.sleep(1);
			click(reminderMessage);
			input(reminderMessage,"Reminder to join the traning");
			HighlightElementManager.highlightElement(driver, reminderSave);
			ReportingUtils.log(driver, LogStatus.PASS, "Setting reminders ", true);
			click(reminderSave);
	
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Setting the reminder information is failed ", true);
		}
	}

	public void addInvParty() {
		try{
			if(involvedPartyOpen.isDisplayed()){
				click(involvedPartyGearIcon);
				click(invPrtyEditView);
				click(invPrtyRoleList);
				Select selectInvPartyRole = new Select(invPrtyRoleList);
				selectInvPartyRole.selectByValue("Witness");
				input(invPrtyContactNum, "3215436767");
				ReportingUtils.log(driver, LogStatus.PASS, "Edited Involved party succesfully ", true);
				click(invPrtySave);
			}
		}catch(Exception e){
			
		}
		
	}
	
	
	
 }



