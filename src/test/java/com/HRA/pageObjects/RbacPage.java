package com.HRA.pageObjects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.HRA.helper.HighlightElementManager;
import com.HRA.helper.ReportingUtils;
import com.HRA.or.RbacOR;
import com.HRA.testBase.TestBase;
import com.relevantcodes.extentreports.LogStatus;

public class RbacPage extends TestBase {

	WebDriver driver;

	public RbacPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = RbacOR.mpEditPermi)	public WebElement mpEditPermi;

	@FindBy(xpath = RbacOR.mpUserRoleYes) public WebElement mpUserRoleYes;
	@FindBy(xpath = RbacOR.mpUserRoleNo) public WebElement mpUserRoleNo;
	@FindBy(xpath = RbacOR.mpGrpAccessAll) public WebElement mpGrpAccessAll;
	@FindBy(xpath = RbacOR.mpGrpAccessLmt) public WebElement mpGrpAccessLmt;
	@FindBy(xpath = RbacOR.mpViewDetGrpLmt) public WebElement mpViewDetGrpLmt;
	@FindBy(xpath = RbacOR.mpViewDetLocLmt) public WebElement mpViewDetLocLmt;
	@FindBy(xpath = RbacOR.mpLocAccessAll) public WebElement mpLocAccessAll;
	@FindBy(xpath = RbacOR.mpLocAccessLmt) public WebElement mpLocAccessLmt;
	@FindBy(xpath = RbacOR.mpGrpOnlyAccess)	public WebElement mpGrpOnlyAccess;
	@FindBy(xpath = RbacOR.mpGrpExceptAccess) public WebElement mpGrpExceptAccess;
	@FindBy(xpath = RbacOR.mpSelectGrp)	public WebElement mpSelectGrp;
	@FindBy(xpath = RbacOR.mpGrpNoChild) public WebElement mpGrpNoChild;
	@FindBy(xpath = RbacOR.mpGrpHasChild) public WebElement mpGrpHasChild;
	@FindBy(xpath = RbacOR.mpGrpSave) public WebElement mpGrpSave;
	@FindBy(xpath = RbacOR.mpGrpClearAll) public WebElement mpGrpClearAll;
	@FindBy(xpath = RbacOR.mpLocOnly) public WebElement mpLocOnly;
	@FindBy(xpath = RbacOR.mpLocExcept) public WebElement mpLocExcept;
	@FindBy(xpath = RbacOR.mpSelectLoc) public WebElement mpSelectLoc;
	@FindBy(xpath = RbacOR.mpLocNoChild) public WebElement mpLocNoChild;
	@FindBy(xpath = RbacOR.mpLocHasChild) public WebElement mpLocHasChild;
	@FindBy(xpath = RbacOR.mpLocSave) public WebElement mpLocSave;
	@FindBy(xpath = RbacOR.mpLocClearAll) public WebElement mpLocClearAll;
	@FindBy(xpath = RbacOR.qvMpGrpPerm) public WebElement qvMpGrpPerm;
	@FindBy(xpath = RbacOR.qvMpLocPerm) public WebElement qvMpLocPerm;
	@FindBy(xpath = RbacOR.qvLmtOtherPerms) public WebElement qvLmtOtherPerms;

	// Role is a Limited user
	@FindBy(xpath = RbacOR.limitedRoleYes) public WebElement limitedRoleYes;
	@FindBy(xpath = RbacOR.limitedRoleNo) public WebElement limitedRoleNo;
	@FindBy(xpath = RbacOR.lmtProfileAdminYes) public WebElement lmtProfileAdminYes;
	@FindBy(xpath = RbacOR.lmtProfileAdminNo) public WebElement lmtProfileAdminNo;
	@FindBy(xpath = RbacOR.lmtPAGrpAccessAll) public WebElement lmtPAGrpAccessAll;
	@FindBy(xpath = RbacOR.lmtPAGrpAccessLmt) public WebElement lmtPAGrpAccessLmt;
	@FindBy(xpath = RbacOR.lmtPAGrpAccessViewDet) public WebElement lmtPAGrpAccessViewDet;
	@FindBy(xpath = RbacOR.lmtPAGrpOnly) public WebElement lmtPAGrpOnly;
	@FindBy(xpath = RbacOR.lmtPAGrpExcept) public WebElement lmtPAGrpExcept;
	@FindBy(xpath = RbacOR.lmtPASelect) public WebElement lmtPASelect;
	@FindBy(xpath = RbacOR.lmtPAGrpNoChild) public WebElement lmtPAGrpNoChild;
	@FindBy(xpath = RbacOR.lmtPAGrpHasChild) public WebElement lmtPAGrpHasChild;
	@FindBy(xpath = RbacOR.lmtPAGrpChildren) public WebElement lmtPAGrpChildren;
	@FindBy(xpath = RbacOR.lmtPAGrpSelected) public WebElement lmtPAGrpSelected;
	@FindBy(xpath = RbacOR.lmtPAGrpPermSave) public WebElement lmtPAGrpPermSave;
	@FindBy(xpath = RbacOR.lmtPALocAccessAll) public WebElement lmtPALocAccessAll;
	@FindBy(xpath = RbacOR.lmtPALocAccessLmt) public WebElement lmtPALocAccessLmt;
	@FindBy(xpath = RbacOR.lmtPALocAccessViewDet) public WebElement lmtPALocAccessViewDet;
	@FindBy(xpath = RbacOR.lmtPALocOnly) public WebElement lmtPALocOnly;
	@FindBy(xpath = RbacOR.lmtPALocExcept)public WebElement lmtPALocExcept;
	@FindBy(xpath = RbacOR.lmtPALocSelect) public WebElement lmtPALocSelect;
	@FindBy(xpath = RbacOR.lmtPALocNoChild) public WebElement lmtPALocNoChild;
	@FindBy(xpath = RbacOR.lmtPALocpHasChild) public WebElement lmtPALocpHasChild;
	@FindBy(xpath = RbacOR.lmtPALocChildren) public WebElement lmtPALocChildren;
	// @FindBy(xpath = RbacOR.lmtPALocSelected) public WebElement
	
	// lmtPALocSelected;
	@FindBy(xpath = RbacOR.lmtConfigAdminYes) public WebElement lmtConfigAdminYes;
	@FindBy(xpath = RbacOR.lmtConfigAdminNo) public WebElement lmtConfigAdminNo;
	@FindBy(xpath = RbacOR.lmtDataInteAdminYes) public WebElement lmtDataInteAdminYes;
	@FindBy(xpath = RbacOR.lmtDataInteAdminNo) public WebElement lmtDataInteAdminNo;
	@FindBy(xpath = RbacOR.lmtPAGrpClearAll) public WebElement lmtPAGrpClearAll;
	@FindBy(xpath = RbacOR.lmtPALocClearAll) public WebElement lmtPALocClearAll;
	@FindBy(xpath = RbacOR.lmtPALocPermSave) public WebElement lmtPALocPermSave;
	
	@FindBy(xpath = RbacOR.qvLmtPAGrpPerm) public WebElement qcLmtPAGrpPerm;
	@FindBy(xpath = RbacOR.qvLmtPALocPerm) public WebElement qcLmtPALocPerm;

	// This Role can Enter Cases
	@FindBy(xpath = RbacOR.enterCasesRoleYes) public WebElement enterCasesRoleYes;
	@FindBy(xpath = RbacOR.enterCasesRoleNo) public WebElement enterCasesRoleNo;
	@FindBy(xpath = RbacOR.enterCaseViewDetails) public WebElement enterCaseViewDetails;
	@FindBy(xpath = RbacOR.enterCaseER)public WebElement enterCaseER;
	@FindBy(xpath = RbacOR.enterCaseERInvPartyYes) public WebElement enterCaseERInvPartyYes;
	@FindBy(xpath = RbacOR.enterCaseERInvPartyNo) public WebElement enterCaseERInvPartyNo;
	@FindBy(xpath = RbacOR.enterCaseInv) public WebElement enterCaseInv;
	@FindBy(xpath = RbacOR.enterCaseInvIntakeOnly) public WebElement enterCaseInvIntakeOnly;
	@FindBy(xpath = RbacOR.enterCaseInvInvTeam) public WebElement enterCaseInvInvTeam;
	@FindBy(xpath = RbacOR.enterCaseInvLegalAdv) public WebElement enterCaseInvLegalAdv;
	@FindBy(xpath = RbacOR.enterCaseEI) public WebElement enterCaseEI;
	@FindBy(xpath = RbacOR.enterCasePHInt) public WebElement enterCasePHInt;
	@FindBy(xpath = RbacOR.enterCaseSave) public WebElement enterCaseSave;
	@FindBy(xpath = RbacOR.enterCaseCancel) public WebElement enterCaseCancel;
	@FindBy(xpath = RbacOR.qvEnterCasePerms) public WebElement qvEnterCasePerms;
	
	// This Role can View cases
	
	@FindBy(xpath = RbacOR.viewCasesYes) public WebElement viewCasesYes;
	@FindBy(xpath = RbacOR.viewCasesNo) public WebElement viewCasesNo;
	@FindBy(xpath = RbacOR.vcCaseTypeAll) public WebElement vcCaseTypeAll;
	@FindBy(xpath = RbacOR.vcCaseTypeLmt) public WebElement vcCaseTypeLmt;
	@FindBy(xpath = RbacOR.vcCaseTypeViewDetails) public WebElement vcCaseTypeViewDetails;
	@FindBy(xpath = RbacOR.vcCaseTypeER) public WebElement vcCaseTypeER;
	@FindBy(xpath = RbacOR.vcCaseTypeINV) public WebElement vcCaseTypeINV;
	@FindBy(xpath = RbacOR.vcCaseTypeEI) public WebElement vcCaseTypeEI;
	@FindBy(xpath = RbacOR.vcCaseTypePH) public WebElement vcCaseTypePH;
	@FindBy(xpath = RbacOR.vcCaseTypeSave) public WebElement vcCaseTypeSave;
	@FindBy(xpath = RbacOR.vcGrpAccessLmt) public WebElement vcGrpAccessLmt;
	@FindBy(xpath = RbacOR.vcGrpAccessLmtViewDetails) public WebElement vcGrpAccessLmtViewDetails;
	@FindBy(xpath = RbacOR.vcGrpAccessAll) public WebElement vcGrpAccessAll;
	@FindBy(xpath = RbacOR.vcGrpOnlyAccess) public WebElement vcGrpOnlyAccess;
	@FindBy(xpath = RbacOR.vcGrpExceptAccess) public WebElement vcGrpExceptAccess;
	@FindBy(xpath = RbacOR.vcGrpAccessclearAll) public WebElement vcGrpAccessclearAll;
	@FindBy(xpath = RbacOR.vcSelectGrp) public WebElement vcSelectGrp;
	@FindBy(xpath = RbacOR.vcGrpNoChild) public WebElement vcGrpNoChild;
	@FindBy(xpath = RbacOR.vcGrpHasChild) public WebElement vcGrpHasChild;
	@FindBy(xpath = RbacOR.vcGrpChildren) public WebElement vcGrpChildren;
	@FindBy(xpath = RbacOR.vcGrpSelected) public WebElement vcGrpSelected;
	@FindBy(xpath = RbacOR.vcGrpSave) public WebElement vcGrpSave;
	@FindBy(xpath = RbacOR.vcGrpClearAll) public WebElement vcGrpClearAll;
	@FindBy(xpath = RbacOR.vcIssueAccessAll) public WebElement vcIssueAccessAll;
	@FindBy(xpath = RbacOR.vcIssueAccessLmt) public WebElement vcIssueAccessLmt;
	@FindBy(xpath = RbacOR.vcIssueAccessLmtViewDetails) public WebElement vcIssueAccessLmtViewDetails;
	@FindBy(xpath = RbacOR.vcIssuesOnlyAccess) public WebElement vcIssuesOnlyAccess;
	@FindBy(xpath = RbacOR.vcIssuesExceptAccess) public WebElement vcIssuesExceptAccess;
	@FindBy(xpath = RbacOR.vcSelectIssues) public WebElement vcSelectIssues;
	@FindBy(xpath = RbacOR.vcIssueAccessClearAll) public WebElement vcIssueAccessClearAll;
	@FindBy(xpath = RbacOR.vcIssuesNoChild) public WebElement vcIssuesNoChild;
	@FindBy(xpath = RbacOR.vcIssuesHasChild) public WebElement vcIssuesHasChild;
	@FindBy(xpath = RbacOR.vcIssuesChildren) public WebElement vcIssuesChildren;
	@FindBy(xpath = RbacOR.vcIssuesSelected) public WebElement vcIssuesSelected;
	@FindBy(xpath = RbacOR.vcIssuesSave) public WebElement vcIssuesSave;
	@FindBy(xpath = RbacOR.vcLocAccessLmt) public WebElement vcLocAccessAll;
	@FindBy(xpath = RbacOR.vcLocAccessLmt) public WebElement vcLocAccessLmt;
	@FindBy(xpath = RbacOR.vcLocAccessLmtViewDetails) public WebElement vcLocAccessLmtViewDetails;
	@FindBy(xpath = RbacOR.vcLocOnlyAccess) public WebElement vcLocOnlyAccess;
	@FindBy(xpath = RbacOR.vcLocExceptAccess) public WebElement vcLocExceptAccess;
	@FindBy(xpath = RbacOR.vcSelectLoc) public WebElement vcSelectLoc;
	@FindBy(xpath = RbacOR.vcLocHasChild) public WebElement vcLocHasChild;
	@FindBy(xpath = RbacOR.vcLocNoChild) public WebElement vcLocNoChild;
	@FindBy(xpath = RbacOR.vcLocSelected) public WebElement vcLocSelected;
	@FindBy(xpath = RbacOR.vcLocSave) public WebElement vcLocSave;
	@FindBy(xpath = RbacOR.vcLocClearAll) public WebElement vcLocClearAll;
	@FindBy(xpath = RbacOR.qvVcCaseTypePerms) public WebElement qvVcCaseTypePerms;
	@FindBy(xpath = RbacOR.qvVcGrpPerm) public WebElement qvVcGrpPerm;
	@FindBy(xpath = RbacOR.qvVcIssueAccessPerm) public WebElement qvVcIssueAccessPerm;
	@FindBy(xpath = RbacOR.qvVcLocPerm) public WebElement qvVcLocPerm;
	
	// This Role is a case admin
	@FindBy(xpath = RbacOR.caViewCasePermYes) public WebElement caViewCasePermYes;
	@FindBy(xpath = RbacOR.caViewCasePermNo) public WebElement caViewCasePermNo;
	@FindBy(xpath = RbacOR.caGrpAccessAll) public WebElement caGrpAccessAll;
	@FindBy(xpath = RbacOR.caGrpAccessLimited) public WebElement caGrpAccessLimited;
	@FindBy(xpath = RbacOR.caGrpAccessLmtViewDetail) public WebElement caGrpAccessLmtViewDetail;
	@FindBy(xpath = RbacOR.caGrpOnlyAccess) public WebElement caGrpOnlyAccess;
	@FindBy(xpath = RbacOR.caGrpExceptAccess) public WebElement caGrpExceptAccess;
	@FindBy(xpath = RbacOR.caSelectGrp) public WebElement caSelectGrp;
	@FindBy(xpath = RbacOR.caGrpClearAll) public WebElement caGrpClearAll;
	@FindBy(xpath = RbacOR.caGrpNoChild) public WebElement caGrpNoChild;
	@FindBy(xpath = RbacOR.caGrpHasChild) public WebElement caGrpHasChild;
	@FindBy(xpath = RbacOR.caGrpChildren) public WebElement caGrpChildren;
	@FindBy(xpath = RbacOR.caGrpSelected) public WebElement caGrpSelected;
	@FindBy(xpath = RbacOR.caGrpSave) public WebElement caGrpSave;
	@FindBy(xpath = RbacOR.caLocAccessAll) public WebElement caLocAccessAll;
	@FindBy(xpath = RbacOR.caLocAccessLimited) public WebElement caLocAccessLimited;
	@FindBy(xpath = RbacOR.caLocAccessLmtViewDetail) public WebElement caLocAccessLmtViewDetail;
	@FindBy(xpath = RbacOR.caLocOnlyAccess) public WebElement caLocOnlyAccess;
	@FindBy(xpath = RbacOR.caLocExceptAccess) public WebElement caLocExceptAccess;
	@FindBy(xpath = RbacOR.caSelectLoc) public WebElement caSelectLoc;
	@FindBy(xpath = RbacOR.caLocNoChild) public WebElement caLocNoChild;
	@FindBy(xpath = RbacOR.caLocHasChild) public WebElement caLocHasChild;
	@FindBy(xpath = RbacOR.caLocSelected) public WebElement caLocSelected;
	@FindBy(xpath = RbacOR.caLocSave) public WebElement caLocSave;
	@FindBy(xpath = RbacOR.caLocClearAll) public WebElement caLocClearAll;
	@FindBy(xpath = RbacOR.qvCaGrpPerm) public WebElement qvCaGrpPerm;
	@FindBy(xpath = RbacOR.qvCaLocPerm) public WebElement qvCaLocPerm;
	
	// This role is a Profile Admin
	@FindBy(xpath = RbacOR.profileAdminYes) public WebElement profileAdminYes;
	@FindBy(xpath = RbacOR.profileAdminNo) public WebElement profileAdminNo;
	@FindBy(xpath = RbacOR.paCaseViewYes) public WebElement paCaseViewYes;
	@FindBy(xpath = RbacOR.paCaseViewNo) public WebElement paCaseViewNo;
	@FindBy(xpath = RbacOR.paGrpAccessAll) public WebElement paGrpAccessAll;
	@FindBy(xpath = RbacOR.paGrpAccessLmt) public WebElement paGrpAccessLmt;
	@FindBy(xpath = RbacOR.paGrpAccessViewDetail) public WebElement paGrpAccessViewDetail;
	@FindBy(xpath = RbacOR.paGrpOnlyAccess) public WebElement paGrpOnlyAccess;
	@FindBy(xpath = RbacOR.paGrpExceptAccess) public WebElement paGrpExceptAccess;
	@FindBy(xpath = RbacOR.paSelectGrp) public WebElement paSelectGrp;
	@FindBy(xpath = RbacOR.paGrpNoChild) public WebElement paGrpNoChild;
	@FindBy(xpath = RbacOR.paGrpHasChild) public WebElement paGrpHasChild;
	@FindBy(xpath = RbacOR.paGrpChildren) public WebElement paGrpChildren;
	@FindBy(xpath = RbacOR.paGrpSelected) public WebElement paGrpSelected;
	@FindBy(xpath = RbacOR.paGrpSave) public WebElement paGrpSave;
	@FindBy(xpath = RbacOR.paGrpClearAll) public WebElement paGrpClearAll;
	@FindBy(xpath = RbacOR.paLocAccessAll) public WebElement paLocAccessAll;
	@FindBy(xpath = RbacOR.paLocAccessLmt) public WebElement paLocAccessLmt;
	@FindBy(xpath = RbacOR.paLocAccessViewDetail) public WebElement paLocAccessViewDetail;
	@FindBy(xpath = RbacOR.paLocSave) public WebElement paLocSave;
	@FindBy(xpath = RbacOR.paLocOnlyAccess) public WebElement paLocOnlyAccess;
	@FindBy(xpath = RbacOR.paLocExceptAccess) public WebElement paLocExceptAccess;
	@FindBy(xpath = RbacOR.paSelectLoc) public WebElement paSelectLoc;
	@FindBy(xpath = RbacOR.paLocNoChild) public WebElement paLocNoChild;
	@FindBy(xpath = RbacOR.paLocHasChild) public WebElement paLocHasChild;
	@FindBy(xpath = RbacOR.paLocClearAll) public WebElement paLocClearAll;
	@FindBy(xpath = RbacOR.qvPaGrpPerm) public WebElement qvPaGrpPerm;
	@FindBy(xpath = RbacOR.qvPaLocPerm) public WebElement qvPaLocPerm;

	// This role can access Insights reporting
	@FindBy(xpath = RbacOR.rptAccessYes) public WebElement rptAccessYes;
	@FindBy(xpath = RbacOR.rptAccessNo) public WebElement rptAccessNo;
	@FindBy(xpath = RbacOR.rptCaseTypeAll) public WebElement rptCaseTypeAll;
	@FindBy(xpath = RbacOR.rptCaseTypeLmt) public WebElement rptCaseTypeLmt;
	@FindBy(xpath = RbacOR.rptCaseTypeLmtViewDetail) public WebElement rptCaseTypeLmtViewDetail;
	@FindBy(xpath = RbacOR.rptCaseTypeER) public WebElement rptCaseTypeER;
	@FindBy(xpath = RbacOR.rptCaseTypeINV) public WebElement rptCaseTypeINV;
	@FindBy(xpath = RbacOR.rptCaseTypeEI) public WebElement rptCaseTypeEI;
	@FindBy(xpath = RbacOR.rptCaseTypePH) public WebElement rptCaseTypePH;
	@FindBy(xpath = RbacOR.rptCaseTypeSave) public WebElement rptCaseTypeSave;
	@FindBy(xpath = RbacOR.rptInsightAnltsNo) public WebElement rptInsightAnltsNo;
	@FindBy(xpath = RbacOR.rptInsightAnltsYes) public WebElement rptInsightAnltsYes;
	@FindBy(xpath = RbacOR.rptInsightDBNo) public WebElement rptInsightDBNo;
	@FindBy(xpath = RbacOR.rptInsightDBYes) public WebElement rptInsightDBYes;
	@FindBy(xpath = RbacOR.rptInsightBMYes) public WebElement rptInsightBMYes;
	@FindBy(xpath = RbacOR.rptInsightBMNo) public WebElement rptInsightBMNo;
	@FindBy(xpath = RbacOR.rptInvPrtDtlsYes) public WebElement rptInvPrtDtlsYes;
	@FindBy(xpath = RbacOR.rptInvPrtDtlsNo) public WebElement rptInvPrtDtlsNo;
	@FindBy(xpath = RbacOR.rptEvntBsdInstYes) public WebElement rptEvntBsdInstYes;
	@FindBy(xpath = RbacOR.rptEvntBsdInstNo) public WebElement rptEvntBsdInstNo;
	@FindBy(xpath = RbacOR.rptCaseViewAccessYes) public WebElement rptCaseViewAccessYes;
	@FindBy(xpath = RbacOR.rptCaseViewAccessNo) public WebElement rptCaseViewAccessNo;
	@FindBy(xpath = RbacOR.rptGrpAccessAll) public WebElement rptGrpAccessAll;
	@FindBy(xpath = RbacOR.rptGrpAccessLmt) public WebElement rptGrpAccessLmt;
	@FindBy(xpath = RbacOR.rptGrpAccessViewDet) public WebElement rptGrpAccessViewDet;
	@FindBy(xpath = RbacOR.rptGrpOnlyAccess) public WebElement rptGrpOnlyAccess;
	@FindBy(xpath = RbacOR.rptGrpExceptAccess) public WebElement rptGrpExceptAccess;
	@FindBy(xpath = RbacOR.rptSelectGrp) public WebElement rptSelectGrp;
	@FindBy(xpath = RbacOR.rptGrpNoChild) public WebElement rptGrpNoChild;
	@FindBy(xpath = RbacOR.rptGrpHasChild) public WebElement rptGrpHasChild;
	@FindBy(xpath = RbacOR.rptGrpChildren) public WebElement rptGrpChildren;
	@FindBy(xpath = RbacOR.rptGrpSelected) public WebElement rptGrpSelected;
	@FindBy(xpath = RbacOR.rptGrpClearAll) public WebElement rptGrpClearAll;
	@FindBy(xpath = RbacOR.rptGrpSave) public WebElement rptGrpSave;
	@FindBy(xpath = RbacOR.rptIssueAccessAll) public WebElement rptIssueAccessAll;
	@FindBy(xpath = RbacOR.rptIssueAccessLmt) public WebElement rptIssueAccessLmt;
	@FindBy(xpath = RbacOR.rptIssueAccessViewDet) public WebElement rptIssueAccessViewDet;
	@FindBy(xpath = RbacOR.rptIssuesOnlyAccess) public WebElement rptIssuesOnlyAccess;
	@FindBy(xpath = RbacOR.rptIssuesExceptAccess) public WebElement rptIssuesExceptAccess;
	@FindBy(xpath = RbacOR.rptSelectIssues) public WebElement rptSelectIssues;
	@FindBy(xpath = RbacOR.rptIssuesHasChild) public WebElement rptIssuesHasChild;
	@FindBy(xpath = RbacOR.rptIssuesChildren) public WebElement rptIssuesChildren;
	@FindBy(xpath = RbacOR.rptIssuesSelected) public WebElement rptIssuesSelected;
	@FindBy(xpath = RbacOR.rptIssuesSave) public WebElement rptIssuesSave;
	@FindBy(xpath = RbacOR.rptIssuesClearAll) public WebElement rptIssuesClearAll;
	@FindBy(xpath = RbacOR.rptInstRptAdminYes) public WebElement rptInstRptAdminYes;
	@FindBy(xpath = RbacOR.rptInstRptAdminNo) public WebElement rptInstRptAdminNo;
	@FindBy(xpath = RbacOR.rptLocAccessAll) public WebElement rptLocAccessAll;
	@FindBy(xpath = RbacOR.rptLocAccessLmt) public WebElement rptLocAccessLmt;
	@FindBy(xpath = RbacOR.rptLocAccessViewDet) public WebElement rptLocAccessViewDet;
	@FindBy(xpath = RbacOR.rptLocOnlyAccess) public WebElement rptLocOnlyAccess;
	@FindBy(xpath = RbacOR.rptLocExceptAccess) public WebElement rptLocExceptAccess;
	@FindBy(xpath = RbacOR.rptSelectLoc) public WebElement rptSelectLoc;
	@FindBy(xpath = RbacOR.rptLocHasChild) public WebElement rptLocHasChild;
	@FindBy(xpath = RbacOR.rptLocNoChild) public WebElement rptLocNoChild;
	@FindBy(xpath = RbacOR.rptLocSelected) public WebElement rptLocSelected;
	@FindBy(xpath = RbacOR.rptLocSave) public WebElement rptLocSave;
	@FindBy(xpath = RbacOR.rptLocClearAll) public WebElement rptLocClearAll;
	@FindBy(xpath = RbacOR.qvRptCaseTypeNonePerms) public WebElement qvRptCaseTypeNonePerms;
	@FindBy(xpath = RbacOR.qvRptGrpPerm) public WebElement qvRptGrpPerm;
	@FindBy(xpath = RbacOR.qvRptOtherPerm) public WebElement qvRptOtherPerm;
	@FindBy(xpath = RbacOR.qvRptIssueAccessPerm) public WebElement qvRptIssueAccessPerm;
	@FindBy(xpath = RbacOR.qvRptLocPerm) public WebElement qvRptLocPerm;
	@FindBy(xpath = RbacOR.qvOtherPerms) public WebElement qvOtherPerms;

	@FindBy(xpath = RbacOR.isConfigAdminYes) public WebElement isConfigAdminYes;
	@FindBy(xpath = RbacOR.isConfigAdminNo) public WebElement isConfigAdminNo;

	@FindBy(xpath = RbacOR.isDataImportAdminYes) public WebElement isDataImportAdminYes;
	@FindBy(xpath = RbacOR.isDataImportAdminNo) public WebElement isDataImportAdminNo;

	@FindBy(xpath = RbacOR.isMngrCaseApproverYes) public WebElement isMngrCaseApproverYes;
	@FindBy(xpath = RbacOR.isMngrCaseApproverNo) public WebElement isMngrCaseApproverNo;

	@FindBy(xpath = RbacOR.isDataShredderNo) public WebElement isDataShredderNo;
	@FindBy(xpath = RbacOR.isDataShredderYes) public WebElement isDataShredderYes;
	@FindBy(xpath = RbacOR.permissionSave) public WebElement permissionSave;
	@FindBy(xpath = RbacOR.managerAccessPerm) public WebElement managerAccessPerm;
	@FindBy(xpath = RbacOR.groupAccessPerm) public WebElement groupAccessPerm;

	@FindBy(xpath = RbacOR.quickViewPerm) public WebElement quickViewPerm;
	@FindBy(xpath = RbacOR.quickViewAssignedUsers) public WebElement quickViewAssignedUsers;
	
	List<String> groupsList = new ArrayList<>();
	List<String> locationsList = new ArrayList<>();
	List<String> caseTypeOptionsList = new ArrayList<>();
	List<String> vcGroupsList = new ArrayList<>();
	List<String> vcLocationsList = new ArrayList<>();
	List<String> vcIssueAccessList = new ArrayList<>();
	List<String> irGroupsList = new ArrayList<>();
	List<String> irLocationsList = new ArrayList<>();
	List<String> irIssueAccessList = new ArrayList<>();
	List<String> caGroupsList = new ArrayList<>();
	List<String> caLocationsList = new ArrayList<>();
	List<String> paGroupsList = new ArrayList<>();
	List<String> paLocationsList = new ArrayList<>();
	List<String> enterCasesOptionsList = new ArrayList<>();
	List<String> rptCaseTypeOptionsList = new ArrayList<>();
	

	public void mpUserRole() throws Throwable {
		try{
			HighlightElementManager.highlightElement(driver, mpUserRoleYes);
			click(mpUserRoleYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables Manager Portal User role", false);
			HighlightElementManager.unHighlightElement(driver, mpUserRoleYes);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to select " + e.getMessage(), true);
			Assert.fail("User is not able to select  " + e.getMessage());	
		}
		
	}

	public void selectGrpAccessAll(String roles, String option) throws Throwable {
		try{
			if(roles.equalsIgnoreCase("Manager Portal")){
				click(mpGrpAccessAll);
			}else if(roles.equalsIgnoreCase("limited User")){
				click(lmtPAGrpAccessAll);
			}else if(roles.equalsIgnoreCase("View Cases")){
				click(vcGrpAccessAll);
			}else if(roles.equalsIgnoreCase("Case Admin")){
				click(caGrpAccessAll);
			}else if(roles.equalsIgnoreCase("Profile Admin")){
				click(paGrpAccessAll);
			}else if(roles.equalsIgnoreCase("insights reporting")){
				click(rptGrpAccessAll);
			}
		
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to select " + roles + e.getMessage(), true);
			Assert.fail("User is not able to select  " + e.getMessage());
		}
		
	}
	public void selectLocAccessAll(String roles, String option) throws Throwable {
		try{
			if(roles.equalsIgnoreCase("Manager Portal")){
				click(mpLocAccessAll);
			}else if(roles.equalsIgnoreCase("limited User")){
				click(lmtPALocAccessAll);
			}else if(roles.equalsIgnoreCase("View Cases")){
				click(vcLocAccessAll);
			}else if(roles.equalsIgnoreCase("Case Admin")){
				click(caLocAccessAll);
			}else if(roles.equalsIgnoreCase("Profile Admin")){
				click(paLocAccessAll);
			}else if(roles.equalsIgnoreCase("insights reporting")){
				click(rptLocAccessAll);
			}
		
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to select " + e.getMessage(), true);
			Assert.fail("User is not able to select  " + e.getMessage());
		}
		
	}
	
	public void selectIssueAccessAll(String roles, String option) throws IOException {
		try{
			if(roles.equalsIgnoreCase("View Cases")){
				click(vcIssueAccessAll);
			}else if(roles.equalsIgnoreCase("Insights Reporting")){
				click(rptIssueAccessAll);
			}
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to select " + e.getMessage(), true);
			Assert.fail("User is not able to select  " + e.getMessage());
		}
		
	}

	public void quickViewPermission() throws Throwable {
		try{
			HighlightElementManager.highlightElement(driver, quickViewPerm);
			click(quickViewPerm);
			ReportingUtils.log(driver, LogStatus.PASS, "User is able to see roles permissions ", true);
			HighlightElementManager.unHighlightElement(driver, quickViewPerm);

		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to see roles permissions " + e.getMessage(), true);
			Assert.fail("User is not able to see roles permissions " + e.getMessage());
		}
		
	}

	public void editPermissions() throws Throwable {
		try{
			HighlightElementManager.highlightElement(driver, mpEditPermi);
			click(mpEditPermi);
			ReportingUtils.log(driver, LogStatus.PASS, "User can edit the role permissions ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User can edit the role permissions " + e.getMessage(), false);
		}
		
	}

	public void selectLmtGrpNEdit(String role) throws Throwable {
		try{
			if(role.equalsIgnoreCase("manager portal")){
				click(mpGrpAccessLmt);
				HighlightElementManager.highlightElement(driver, mpViewDetGrpLmt);
				TimeUnit.SECONDS.sleep(2);
				click(mpViewDetGrpLmt);
				HighlightElementManager.unHighlightElement(driver, mpViewDetGrpLmt);
			}else if(role.equalsIgnoreCase("profile admin")){
				click(paGrpAccessLmt);
				HighlightElementManager.highlightElement(driver, paGrpAccessViewDetail);
				TimeUnit.SECONDS.sleep(2);
				click(paGrpAccessViewDetail);
				HighlightElementManager.unHighlightElement(driver, paGrpAccessViewDetail);
			}else if(role.equalsIgnoreCase("view cases")){
				click(vcGrpAccessLmt);
				HighlightElementManager.highlightElement(driver, vcGrpAccessLmtViewDetails);
				TimeUnit.SECONDS.sleep(2);
				click(vcGrpAccessLmtViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcGrpAccessLmtViewDetails);
			}else if(role.equalsIgnoreCase("case admin")){
				click(caGrpAccessLimited);
				HighlightElementManager.highlightElement(driver, caGrpAccessLmtViewDetail);
				TimeUnit.SECONDS.sleep(2);
				click(caGrpAccessLmtViewDetail);
				HighlightElementManager.unHighlightElement(driver, caGrpAccessLmtViewDetail);
			}else if(role.equalsIgnoreCase("insights reporting")){
				click(rptGrpAccessLmt);
				HighlightElementManager.highlightElement(driver, rptGrpAccessViewDet);
				TimeUnit.SECONDS.sleep(2);
				click(rptGrpAccessViewDet);
				HighlightElementManager.unHighlightElement(driver, rptGrpAccessViewDet);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
				Assert.fail("Invalid role - " + role);
			}
			
			ReportingUtils.log(driver, LogStatus.PASS, "Click on Limited Group Access View/Details successful for role - " + role, false);

		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Click on Limited Group Access View/Details successful for role - " + role, false);
			Assert.fail("Click on Limited Group Access View/Details successful for role - " + e.getMessage());
		}
		
	}
	
public void selectLmtIssueNEdit(String role) throws Throwable {
		try{
			if(role.equalsIgnoreCase("View Cases")){
				click(vcIssueAccessLmt);
				HighlightElementManager.highlightElement(driver, vcIssueAccessLmtViewDetails);
				TimeUnit.SECONDS.sleep(1);
				click(vcIssueAccessLmtViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcIssueAccessLmtViewDetails);
			}else if(role.equalsIgnoreCase("Insights Reporting")){
				click(rptIssueAccessLmt);
				HighlightElementManager.highlightElement(driver, rptIssueAccessViewDet);
				TimeUnit.SECONDS.sleep(1);
				click(rptIssueAccessViewDet);
				HighlightElementManager.unHighlightElement(driver, rptIssueAccessViewDet);
			}
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL,"Failed to click on Role and edit details " + e.getMessage(), true);
			Assert.fail("Failed to click on Role and edit details " + e.getMessage());
		}
		
	}
	
public void selectLmtLocNEdit(String role) throws Throwable {
		try{
			if(role.equalsIgnoreCase("manager portal")){
				click(mpLocAccessLmt);
				HighlightElementManager.highlightElement(driver, mpViewDetLocLmt);
				TimeUnit.SECONDS.sleep(2);
				click(mpViewDetLocLmt);
				HighlightElementManager.unHighlightElement(driver, mpViewDetLocLmt);
			}else if(role.equalsIgnoreCase("profile admin")){
				click(paLocAccessLmt);
				HighlightElementManager.highlightElement(driver, paLocAccessViewDetail);
				TimeUnit.SECONDS.sleep(2);
				click(paLocAccessViewDetail);
				HighlightElementManager.unHighlightElement(driver, paLocAccessViewDetail);
			}else if(role.equalsIgnoreCase("view cases")){
				click(vcLocAccessLmt);
				HighlightElementManager.highlightElement(driver, vcLocAccessLmtViewDetails);
				TimeUnit.SECONDS.sleep(2);
				click(vcLocAccessLmtViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcLocAccessLmtViewDetails);
			}else if(role.equalsIgnoreCase("case admin")){
				click(caLocAccessLimited);
				HighlightElementManager.highlightElement(driver, caLocAccessLmtViewDetail);
				TimeUnit.SECONDS.sleep(2);
				click(caLocAccessLmtViewDetail);
				HighlightElementManager.unHighlightElement(driver, caLocAccessLmtViewDetail);
			}else if(role.equalsIgnoreCase("insights reporting")){
				click(rptLocAccessLmt);
				HighlightElementManager.highlightElement(driver, rptLocAccessViewDet);
				TimeUnit.SECONDS.sleep(2);
				click(rptLocAccessViewDet);
				HighlightElementManager.unHighlightElement(driver, rptLocAccessViewDet);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
				Assert.fail("Invalid role - " + role);
			}
			
			ReportingUtils.log(driver, LogStatus.PASS, "Click on Limited Location Access View/Details successful for role - " + role, false);
			
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role + e.getMessage(), false);
			Assert.fail("Invalid role - " + role);
		}
	}

	public void selectOption(String role, String option) throws InterruptedException, IOException {
		try{
			if (option.contains("Group")) {
				if(role.equalsIgnoreCase("manager portal") && option.equalsIgnoreCase("only group selected")){
					groupsList = selectGrpLocOption(role, option, mpGrpOnlyAccess, RbacOR.mpGrpSelected, mpGrpClearAll, mpSelectGrp, mpGrpNoChild, mpGrpHasChild);
				}else if(role.equalsIgnoreCase("manager portal") && option.equalsIgnoreCase("all group except selected")){
					groupsList = selectGrpLocOption(role, option, mpGrpExceptAccess, RbacOR.mpGrpSelected, mpGrpClearAll, mpSelectGrp, mpGrpNoChild, mpGrpHasChild);
				}else if(role.equalsIgnoreCase("profile admin") && option.equalsIgnoreCase("only group selected")){
					paGroupsList = selectGrpLocOption(role, option, paGrpOnlyAccess, RbacOR.paGrpSelected, paGrpClearAll, paSelectGrp, paGrpNoChild, paGrpHasChild);
				}else if(role.equalsIgnoreCase("profile admin") && option.equalsIgnoreCase("all group except selected")){
					paGroupsList = selectGrpLocOption(role, option, paGrpExceptAccess, RbacOR.paGrpSelected, paGrpClearAll, paSelectGrp, paGrpNoChild, paGrpHasChild);
				}else if(role.equalsIgnoreCase("view cases") && option.equalsIgnoreCase("only group selected")){
					vcGroupsList = selectGrpLocOption(role, option, vcGrpOnlyAccess, RbacOR.vcGrpSelected, vcGrpClearAll, vcSelectGrp, vcGrpNoChild, vcGrpHasChild);
				}else if(role.equalsIgnoreCase("view cases") && option.equalsIgnoreCase("all group except selected")){
					vcGroupsList = selectGrpLocOption(role, option, vcGrpExceptAccess, RbacOR.vcGrpSelected, vcGrpClearAll, vcSelectGrp, vcGrpNoChild, vcGrpHasChild);
				}else if(role.equalsIgnoreCase("case admin") && option.equalsIgnoreCase("only group selected")){
					caGroupsList = selectGrpLocOption(role, option, caGrpOnlyAccess, RbacOR.caGrpSelected, caGrpClearAll, caSelectGrp, caGrpNoChild, caGrpHasChild);
				}else if(role.equalsIgnoreCase("case admin") && option.equalsIgnoreCase("all group except selected")){
					caGroupsList = selectGrpLocOption(role, option, caGrpExceptAccess, RbacOR.caGrpSelected, caGrpClearAll, caSelectGrp, caGrpNoChild, caGrpHasChild);
				}else if(role.equalsIgnoreCase("insights reporting") && option.equalsIgnoreCase("only group selected")){
					irGroupsList = selectGrpLocOption(role, option, rptGrpOnlyAccess, RbacOR.rptGrpSelected, rptGrpClearAll, rptSelectGrp, rptGrpNoChild, rptGrpHasChild);
				}else if(role.equalsIgnoreCase("insights reporting") && option.equalsIgnoreCase("all group except selected")){
					irGroupsList = selectGrpLocOption(role, option, rptGrpExceptAccess, RbacOR.rptGrpSelected, rptGrpClearAll, rptSelectGrp, rptGrpNoChild, rptGrpHasChild);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role/option parameters passed - [" + role + ", " + option + "]", false);
					Assert.fail("Invalid role/option parameters passed - [" + role + ", " + option + "]");
				}
				
			} else if (option.contains("Location")) {
				
				if(role.equalsIgnoreCase("manager portal") && option.equalsIgnoreCase("only location selected")){
					locationsList = selectGrpLocOption(role, option, mpLocOnly, RbacOR.mpLocSelected, mpLocClearAll, mpSelectLoc, mpLocNoChild, mpLocHasChild);
				}else if(role.equalsIgnoreCase("manager portal") && option.equalsIgnoreCase("all location except selected")){
					locationsList = selectGrpLocOption(role, option, mpLocExcept, RbacOR.mpLocSelected, mpLocClearAll, mpSelectLoc, mpLocNoChild, mpLocHasChild);
				}else if(role.equalsIgnoreCase("profile admin") && option.equalsIgnoreCase("only location selected")){
					paLocationsList = selectGrpLocOption(role, option, paLocOnlyAccess, RbacOR.paLocSelected, paLocClearAll, paSelectLoc, paLocNoChild, paLocHasChild);
				}else if(role.equalsIgnoreCase("profile admin") && option.equalsIgnoreCase("all location except selected")){
					paLocationsList = selectGrpLocOption(role, option, paLocExceptAccess, RbacOR.paLocSelected, paLocClearAll, paSelectLoc, paLocNoChild, paLocHasChild);
				}else if(role.equalsIgnoreCase("view cases") && option.equalsIgnoreCase("only location selected")){
					vcLocationsList = selectGrpLocOption(role, option, vcLocOnlyAccess, RbacOR.vcLocSelected, vcLocClearAll, vcSelectLoc, vcLocNoChild, vcLocHasChild);
				}else if(role.equalsIgnoreCase("view cases") && option.equalsIgnoreCase("all location except selected")){
					vcLocationsList = selectGrpLocOption(role, option, vcLocExceptAccess, RbacOR.vcLocSelected, vcLocClearAll, vcSelectLoc, vcLocNoChild, vcLocHasChild);
				}else if(role.equalsIgnoreCase("case admin") && option.equalsIgnoreCase("only location selected")){
					caLocationsList = selectGrpLocOption(role, option, caLocOnlyAccess, RbacOR.caLocSelected, caLocClearAll, caSelectLoc, caLocNoChild, caLocHasChild);
				}else if(role.equalsIgnoreCase("case admin") && option.equalsIgnoreCase("all location except selected")){
					caLocationsList = selectGrpLocOption(role, option, caLocExceptAccess, RbacOR.caLocSelected, caLocClearAll, caSelectLoc, caLocNoChild, caLocHasChild);
				}else if(role.equalsIgnoreCase("insights reporting") && option.equalsIgnoreCase("only location selected")){
					irLocationsList = selectGrpLocOption(role, option, rptLocOnlyAccess, RbacOR.rptLocSelected, rptLocClearAll, rptSelectLoc, rptLocNoChild, rptLocHasChild);
				}else if(role.equalsIgnoreCase("insights reporting") && option.equalsIgnoreCase("all location except selected")){
					irLocationsList = selectGrpLocOption(role, option, rptLocExceptAccess, RbacOR.rptLocSelected, rptLocClearAll, rptSelectLoc, rptLocNoChild, rptLocHasChild);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role/option parameters passed - [" + role + ", " + option + "]", false);
					Assert.fail("Invalid role/option parameters passed - [" + role + ", " + option + "]");
				}
				
			} else if (option.contains("Issue")) {
				
				if(role.equalsIgnoreCase("view cases") && option.equalsIgnoreCase("only Issue selected")){
					vcIssueAccessList = selectIssueOption(role, option, vcIssuesOnlyAccess, RbacOR.vcIssuesSelected, vcIssueAccessClearAll, vcSelectIssues, vcIssuesHasChild);
				}else if(role.equalsIgnoreCase("view cases") && option.equalsIgnoreCase("all Issues except selected")){
					vcIssueAccessList = selectIssueOption(role, option, vcIssuesExceptAccess, RbacOR.vcIssuesSelected, vcIssueAccessClearAll, vcSelectIssues, vcIssuesHasChild);
				}else if(role.equalsIgnoreCase("insights reporting") && option.equalsIgnoreCase("only Issue selected")){
					irIssueAccessList = selectIssueOption(role, option, rptIssuesOnlyAccess, RbacOR.rptIssuesSelected, rptIssuesClearAll, rptSelectIssues, rptIssuesHasChild);
				}else if(role.equalsIgnoreCase("insights reporting") && option.equalsIgnoreCase("all Issues except selected")){
					irIssueAccessList = selectIssueOption(role, option, rptIssuesExceptAccess, RbacOR.rptIssuesSelected, rptIssuesClearAll, rptSelectIssues, rptIssuesHasChild);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role/option parameters passed - [" + role + ", " + option + "]", false);
					Assert.fail("Invalid role/option parameters passed - [" + role + ", " + option + "]");
				}
			}
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid roles and options " + e.getMessage(), true);
			Assert.fail("Invalid roles and options " + e.getMessage());
		}
		

	}

	
	private List<String> selectGrpLocOption(String role, String option, WebElement optionAccess,
		String optionSelected, WebElement optionClearAll, WebElement selectOption, WebElement optionNoChild,
		WebElement optionHasChild) throws IOException {
		List<String> optionList = new ArrayList<>();
		click(optionAccess);
		ReportingUtils.log(driver, LogStatus.PASS, "User selects view cases with option - " + option + ", for role - " + role,	false);
		
		List<WebElement> we = driver.findElements(By.xpath(optionSelected));
		if (we.size() > 0) {
			click(optionClearAll);
		}
		
		click(selectOption);
		click(optionNoChild);
		click(selectOption);
		click(optionHasChild);
		
		List<WebElement> elemList = driver.findElements(By.xpath(optionSelected));
		for (WebElement e : elemList) {
			optionList.add(e.getText());
		}
		
		return optionList;
	}

	private List<String> selectIssueOption(String role, String option, WebElement optionAccess,
			String optionSelected, WebElement optionClearAll, WebElement selectOption, WebElement optionHasChild) throws IOException {
			List<String> optionList = new ArrayList<>();
			click(optionAccess);
			ReportingUtils.log(driver, LogStatus.PASS, "User selects view cases with option - " + option + ", for role - " + role,	false);
			
			List<WebElement> we = driver.findElements(By.xpath(optionSelected));
			if (we.size() > 0) {
				click(optionClearAll);
			}
			
			click(selectOption);
			click(optionHasChild);
			
			List<WebElement> elemList = driver.findElements(By.xpath(optionSelected));
			for (WebElement e : elemList) {
				optionList.add(e.getAttribute("data-text"));
			}
			
			return optionList;
		}
	
	public void savePermissions() throws Throwable {
		try{
			HighlightElementManager.highlightElement(driver, permissionSave);
			click(permissionSave);
			ReportingUtils.log(driver, LogStatus.PASS, "Role permission saved successfully ", true);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.PASS, "Failed to save permission " + e.getMessage(), true);
			Assert.fail("Failed to saved permission " + e.getMessage());
		}
		
	}

	public void saveMPRolePermi(String permission) throws Throwable {
		try{
			if (permission.equalsIgnoreCase("group")) {
				HighlightElementManager.highlightElement(driver, mpGrpSave);
				click(mpGrpSave);
			} else if (permission.equalsIgnoreCase("location")) {
				HighlightElementManager.highlightElement(driver, mpLocSave);
				click(mpLocSave);
			}
			ReportingUtils.log(driver, LogStatus.PASS, "Permissions saved successfully for " + permission, true);
		}catch(Exception e){
			ReportingUtils.log(driver,LogStatus.FAIL, "Failed to save MP user permission . " + e.getMessage(), true);
			Assert.fail("Failed to save MP user permissions. " + e.getMessage());
		}
	}

	public void enableLmtUser() throws Throwable {
		try{
			HighlightElementManager.highlightElement(driver, limitedRoleYes);
			click(limitedRoleYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enabled Limited user roles ", false);
			HighlightElementManager.unHighlightElement(driver, limitedRoleYes);
		}catch(Exception e){
			ReportingUtils.log(driver,LogStatus.FAIL, "Failed to Limited user . " + e.getMessage(), true);
			Assert.fail("Failed to save Limited user permissions. " + e.getMessage());
		}
		
	}
	
	public void saveLmtUserpermi(String option) throws InterruptedException, IOException {
		try{
			if (option.equalsIgnoreCase("group")) {
				HighlightElementManager.highlightElement(driver, lmtPAGrpPermSave);
				ReportingUtils.log(driver, LogStatus.PASS, "User saves " + option + "permissions", true);
				click(lmtPAGrpPermSave);
			} else if (option.equalsIgnoreCase("location")) {
				HighlightElementManager.highlightElement(driver, lmtPALocPermSave);
				click(lmtPALocPermSave);
				click(lmtPALocAccessViewDet);
				paLocationsList.clear();
				List<WebElement> we = driver.findElements(By.xpath(RbacOR.lmtPALocSelected));
				for(WebElement e : we){
					paLocationsList.add(e.getText().trim());
				}
				click(lmtPALocPermSave);
				ReportingUtils.log(driver,LogStatus.PASS, "Successfully saved Limited user permissions. ", false);
			}
		}catch(Exception e){
			ReportingUtils.log(driver,LogStatus.FAIL, "Failed to save Limited user permissions. " + e.getMessage(), true);
			Assert.fail("Failed to save Limited user permissions. " + e.getMessage());
		}
		

	}

	public void profileAdminLocAccess(String option) throws InterruptedException, IOException {
		try {
			click(lmtPALocAccessLmt);
			HighlightElementManager.highlightElement(driver, lmtPALocAccessViewDet);
			click(lmtPALocAccessViewDet);
			HighlightElementManager.unHighlightElement(driver, lmtPALocAccessViewDet);
			ReportingUtils.log(driver, LogStatus.PASS,
					"User select, profile admin group access as " + option + " and click on edit details", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to select profile admin group access as " + option + " and click on edit details",
					true);
		}
	}

	public void enableConfigAdmin() throws IOException {
		try {
			HighlightElementManager.highlightElement(driver, lmtConfigAdminYes);
			click(lmtConfigAdminYes);
			HighlightElementManager.unHighlightElement(driver, lmtConfigAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables configuration admin for Limited user", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to enable configuration admin for Limited user", true);
		}
	}

	public void enableDataIntegAdmin() throws IOException {
		try {
			HighlightElementManager.highlightElement(driver, lmtDataInteAdminYes);
			click(lmtDataInteAdminYes);
			HighlightElementManager.unHighlightElement(driver, lmtDataInteAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable data integration admin for Limited user", true);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to enable data integration admin for Limited user", true);
		}
	}

	public void enableRoleToEnterCases() throws IOException {
		try {
			click(enterCasesRoleYes);
			HighlightElementManager.highlightElement(driver, enterCaseViewDetails);
			click(enterCaseViewDetails);
			HighlightElementManager.unHighlightElement(driver, enterCaseViewDetails);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables the role to enter cases and edit detail", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to enables the role to enter cases and edit detail", true);
		}

	}

	public void saveCaseEntryPermi() throws IOException {
		try {
			HighlightElementManager.highlightElement(driver, enterCaseSave);
			ReportingUtils.log(driver, LogStatus.PASS, "User save case entry permissions ", true);
			click(enterCaseSave);
			HighlightElementManager.unHighlightElement(driver, enterCaseSave);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to save case entry permissions", true);
		}

	}

	public void enableViewCases() throws IOException {
		try {
			click(viewCasesYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables View cases permission", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.PASS, "User is not able to enables View Cases permission", true);
		}

	}

	public void vcLmtNEditDetails(String accessType) throws IOException {
		
		try {
			if (accessType.contains("Case type")) {
				click(vcCaseTypeLmt);
				HighlightElementManager.highlightElement(driver, vcCaseTypeViewDetails);
				click(vcCaseTypeViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcCaseTypeViewDetails);
			} else if (accessType.contains("Group access")) {
				click(vcGrpAccessLmt);
				HighlightElementManager.highlightElement(driver, vcGrpAccessLmtViewDetails);
				click(vcGrpAccessLmtViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcGrpAccessLmtViewDetails);
			} else if (accessType.contains("Issue access")) {
				click(vcIssueAccessLmt);
				HighlightElementManager.highlightElement(driver, vcIssueAccessLmtViewDetails);
				click(vcIssueAccessLmtViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcIssueAccessLmtViewDetails);
			} else if (accessType.contains("Location access")) {
				click(vcLocAccessLmt);
				HighlightElementManager.highlightElement(driver, vcLocAccessLmtViewDetails);
				click(vcLocAccessLmtViewDetails);
				HighlightElementManager.unHighlightElement(driver, vcLocAccessLmtViewDetails);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, "Invalid access type " + accessType, false);
				Assert.fail("Invalid access type " + accessType);
			}
			ReportingUtils.log(driver, LogStatus.PASS,
					"User selects option view cases " + accessType + " and Edit details", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to select option view cases " + accessType + " and Edit details", true);
		}

	}

	public void caseTypeOptions(List<String> optionList) throws IOException {
		caseTypeOptionsList.clear();
		caseTypeOptionsList.addAll(optionList);
		resetVcCaseTypeOptions();
		for(String option : optionList){
			try {
				if(option.equalsIgnoreCase("er")){
					click(vcCaseTypeER);
				}else if(option.equalsIgnoreCase("inv")){
					click(vcCaseTypeINV);
				}else if(option.equalsIgnoreCase("ei")){
					click(vcCaseTypeEI);
				}else if(option.equalsIgnoreCase("ph")){
					click(vcCaseTypePH);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Invalid option - " + option,
							false);
					Assert.fail("Invalid option - " + option);
				}
				
				ReportingUtils.log(driver, LogStatus.PASS, "User selects options from Case Type permission - " + option, false);
			} catch (Exception e) {
				ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to selects options from Case Type permission ", false);
			}
		}
	}

	private void resetVcCaseTypeOptions() throws IOException {
		try {
			if (vcCaseTypeER.getAttribute("prevval") != null) {
				click(vcCaseTypeER);
			}

			if (vcCaseTypeINV.getAttribute("prevval") != null) {
				click(vcCaseTypeINV);
			}
			if (vcCaseTypeEI.getAttribute("prevval") != null) {
				click(vcCaseTypeEI);
			}

			if (vcCaseTypePH.getAttribute("prevval") != null) {
				click(vcCaseTypePH);
			}

		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, e.getMessage(), false);
			Assert.fail(e.getMessage());
		}
// TODO Auto-generated method stub
		
	}

	public void saveVCPermission(String caseType) throws IOException {
		try {
			if (caseType.equalsIgnoreCase("Case Type")) {
				click(vcCaseTypeSave);
				HighlightElementManager.highlightElement(driver, vcCaseTypeSave);
			} else if (caseType.equalsIgnoreCase("Group access")) {
				click(vcGrpSave);
				HighlightElementManager.highlightElement(driver, vcGrpSave);
			} else if (caseType.equalsIgnoreCase("Issue access")) {
				click(vcIssuesSave);
				HighlightElementManager.highlightElement(driver, vcIssuesSave);
			} else if (caseType.equalsIgnoreCase("Location access")) {
				click(vcLocSave);
				HighlightElementManager.highlightElement(driver, vcLocSave);
				click(vcLocAccessLmtViewDetails);
				vcLocationsList.clear();
				List<WebElement> we = driver.findElements(By.xpath(RbacOR.vcLocSelected));
				for(WebElement e : we){
					vcLocationsList.add(e.getText().trim());
				}
				click(vcLocSave);
			}
			ReportingUtils.log(driver, LogStatus.PASS, "User saved the " + caseType + "permissions", true);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to save the " + caseType + "permissions",
					true);
		}

	}

	public void enablesCaseAdminRole() throws IOException {
		try {
			click(caViewCasePermYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables Case Admin permission ", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.PASS, "User is not able to enable Case Admin permission ", true);
		}
	}

	public void caLmtEditDetails(String option) throws IOException {
		try {
			if (option.contains("Group access")) {
				click(caGrpAccessLimited);
				click(caGrpAccessLmtViewDetail);
			} else if (option.contains("Location access")) {
				click(caLocAccessLimited);
				click(caLocAccessLmtViewDetail);
			}
			ReportingUtils.log(driver, LogStatus.PASS, "User enables " + option + "Limited and edit details ", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to enables " + option + "Limited and edit details ", true);
		}
	}

	public void saveCaGrpLocPermi(String option) throws IOException {
		try {
			if (option.equalsIgnoreCase("Group access")) {
				click(caGrpSave);
				HighlightElementManager.highlightElement(driver, caGrpSave);
			} else if (option.equalsIgnoreCase("Location access")) {
				click(caLocSave);
				HighlightElementManager.highlightElement(driver, caLocSave);
				click(caLocAccessLmtViewDetail);
				caLocationsList.clear();
				List<WebElement> we = driver.findElements(By.xpath(RbacOR.caLocSelected));
				for(WebElement e : we){
					caLocationsList.add(e.getText().trim());
				}
				click(caLocSave);
			}
			ReportingUtils.log(driver, LogStatus.PASS, "User save " + option + "permission succesfully", true);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.PASS, "User is unable to save " + option + "permission", true);
		}

	}

	public void enableProfileAdminRole() throws IOException {
		try {
			click(profileAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables Profile Admin role sucessfully", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.PASS, "User is not able to enables Profile Admin role ", true);
		}
	}


	public void savePAPermi(String option) throws IOException {
		try {
			if (option.equalsIgnoreCase("Group access")) {
				click(paGrpSave);
			} else if (option.equalsIgnoreCase("Location access")) {
				click(paLocSave);
				click(paLocAccessViewDetail);
				paLocationsList.clear();
				List<WebElement> we = driver.findElements(By.xpath(RbacOR.paLocSelected));
				for(WebElement e : we){
					paLocationsList.add(e.getText().trim());
				}
				click(paLocSave);
			}
			ReportingUtils.log(driver, LogStatus.PASS, "User saved " + option + "permissions", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to save " + option + " permissions", true);
		}

	}

	public void enablesCofigAdmin() throws IOException {
		try {
			click(isConfigAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables configuration Admin Role sucessfully ", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able enable configuration Admin Role ", true);
		}
	}

	public void enableDataInteAdminRole() throws IOException {
		try {
			click(isDataImportAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables Data Integration Admin Role sucessfully ", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able enable Data Integration Admin Role ", true);
		}

	}

	public void verifyGrpAccess(String role, String option) throws IOException {
		if(role.equalsIgnoreCase("limited user")) {
			verifyGroupsList(option, qcLmtPAGrpPerm,groupsList);
		}else if (role.equalsIgnoreCase("manager portal")) {
			verifyGroupsList(option, qvMpGrpPerm,groupsList);
		}else if (role.equalsIgnoreCase("View Cases")) {
			verifyGroupsList(option, qvVcGrpPerm, vcGroupsList);
		}else if (role.equalsIgnoreCase("Insights Reporting")) {
			verifyGroupsList(option, qvRptGrpPerm, irGroupsList);
		}else if (role.equalsIgnoreCase("Case Admin")) {
			verifyGroupsList(option, qvCaGrpPerm,caGroupsList );
		}else if (role.equalsIgnoreCase("Profile Admin")) {
			verifyGroupsList(option, qvPaGrpPerm, paGroupsList);
		}else{
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
			Assert.fail("Invalid role - " + role);
		}

	}

	public void verifyLocAccess(String role, String option) throws IOException {
		if (role.equalsIgnoreCase("limited user")) {
			verifyLocationsList(option, qcLmtPALocPerm, locationsList);
		} else if (role.equalsIgnoreCase("manager portal")) {
			verifyLocationsList(option, qvMpLocPerm, locationsList);
		}else if (role.equalsIgnoreCase("View Cases")) {
			verifyLocationsList(option, qvVcLocPerm, vcLocationsList);
		}else if (role.equalsIgnoreCase("Insights Reporting")) {
			verifyLocationsList(option, qvRptLocPerm, irLocationsList);
		}else if (role.equalsIgnoreCase("Case Admin")) {
			verifyLocationsList(option, qvCaLocPerm,caLocationsList );
		}else if (role.equalsIgnoreCase("Profile Admin")) {
			verifyLocationsList(option, qvPaLocPerm, paLocationsList);
		}else{
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
			Assert.fail("Invalid role - " + role);
		}
		
	}
	
	public void verifyIssueAccess(String role, String option) throws IOException {
		if(role.equalsIgnoreCase("View Cases")) {
			verifyIssueAccessList(option, qvVcIssueAccessPerm, vcIssueAccessList);
		}else if (role.equalsIgnoreCase("Insights Reporting")) {
			verifyIssueAccessList(option, qvRptIssueAccessPerm, irIssueAccessList);
		}else{
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
			Assert.fail("Invalid role - " + role);
		}
		
	}
	
	
	private void verifyIssueAccessList(String option, WebElement issueAccessElement, List<String> expectedIssueAccessList) throws IOException {
		String issueAccess = issueAccessElement.getText();
		HighlightElementManager.highlightElement(driver, issueAccessElement);
		if(option.equalsIgnoreCase("all")){
			if(issueAccess.equalsIgnoreCase("all")){
				ReportingUtils.log(driver, LogStatus.PASS, "Expected Issues - " + option 
						+ " matched with Actual Issues - " + issueAccess, true);
			}else {
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Issues - " + option 
						+ " did not match with Actual Issues - " + issueAccess, true);
			}
		}else if (option.equalsIgnoreCase("only Issue selected")) {
			List<String> actualIssueAccessList = new ArrayList<>();
			if(issueAccess.contains(", ")){
				actualIssueAccessList = Arrays.asList(issueAccess.split(", "));
			}else{
				actualIssueAccessList = Arrays.asList(issueAccess.split(","));
			}
			
			if(expectedIssueAccessList.size() != actualIssueAccessList.size()){
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Issues - " + expectedIssueAccessList 
						+ " but actual Issues list is - " + actualIssueAccessList, true);
			}else{
				if(expectedIssueAccessList.containsAll(expectedIssueAccessList)){
					ReportingUtils.log(driver, LogStatus.PASS, "Expected Issues - " + expectedIssueAccessList 
								+ " matched with actual Issues list - " + actualIssueAccessList, true);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Issues - " + expectedIssueAccessList 
							+ " did not match with actual Issues list - " + actualIssueAccessList, true);
				}
			}

		} else if (option.equalsIgnoreCase("All Issues Except selected")) {
			if(!issueAccess.startsWith("All Issues Except - ")){
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Issues are not excluded - " + issueAccess, true);
			}else{
				List<String> actualIssueAccessList = new ArrayList<>();
				if(issueAccess.contains(", ")){
					actualIssueAccessList = Arrays.asList(issueAccess.substring(issueAccess.indexOf("- ") + 2).split(", "));
				}else{
					actualIssueAccessList = Arrays.asList(issueAccess.substring(issueAccess.indexOf("- ") + 2).split(","));
				} 
				if(expectedIssueAccessList.size() != actualIssueAccessList.size()){
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Issues excluded are - " + expectedIssueAccessList 
							+ " but actual Issues excluded are - " + actualIssueAccessList, true);
				}else{
					if(expectedIssueAccessList.containsAll(actualIssueAccessList)){
						ReportingUtils.log(driver, LogStatus.PASS, "Expected Issues excluded - " + expectedIssueAccessList 
									+ " matched with actual Issues excluded - " + actualIssueAccessList, true);
					}else{
						ReportingUtils.log(driver, LogStatus.FAIL, "Expected Issues excluded - " + expectedIssueAccessList 
								+ " did not match with actual Issues excluded - " + actualIssueAccessList, true);
					}
				}
			}

		} else {
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid option - " + option, false);
		}
		HighlightElementManager.unHighlightElement(driver, issueAccessElement);
		
	}

	public void verifyGroupsList(String option, WebElement grpWebElement, List<String> expectedGroupList) throws IOException{
		String groups = grpWebElement.getText();
		HighlightElementManager.highlightElement(driver, grpWebElement);
		if(option.equalsIgnoreCase("all")){
			if(groups.equalsIgnoreCase("all")){
				ReportingUtils.log(driver, LogStatus.PASS, "Expected Group Access - " + option 
						+ " matched with Actual Group Access - " + groups, true);
			}else {
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Group Access - " + option 
						+ " did not match with Actual Group Access - " + groups, true);
			}
		}else if (option.equalsIgnoreCase("only Group selected")) {
			List<String> actualGroupList = Arrays.asList(groups.split(", "));
			if(expectedGroupList.size() != actualGroupList.size()){
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Groups - " + expectedGroupList 
						+ " but actual groups list is - " + actualGroupList, true);
			}else{
				if(expectedGroupList.containsAll(actualGroupList)){
					ReportingUtils.log(driver, LogStatus.PASS, "Expected Groups - " + expectedGroupList 
								+ " matched with actual groups list - " + actualGroupList, true);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Groups - " + expectedGroupList 
							+ " did not match with actual groups list - " + actualGroupList, true);
				}
			}

		} else if (option.equalsIgnoreCase("All Group Except selected")) {
			if(!groups.startsWith("All Groups Except - ")){
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Groups are not excluded - " + groups, true);
			}else{
				List<String> actualGroupList = Arrays.asList(groups.substring(groups.indexOf("- ") + 2).split(", "));
				if(expectedGroupList.size() != actualGroupList.size()){
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Groups excluded are - " + expectedGroupList 
							+ " but actual groups excluded are - " + actualGroupList, true);
				}else{
					if(expectedGroupList.containsAll(actualGroupList)){
						ReportingUtils.log(driver, LogStatus.PASS, "Expected Groups excluded - " + expectedGroupList 
									+ " matched with actual groups excluded - " + actualGroupList, true);
					}else{
						ReportingUtils.log(driver, LogStatus.FAIL, "Expected Groups excluded - " + expectedGroupList 
								+ " did not match with actual groups excluded - " + actualGroupList, true);
					}
				}
			}

		} else {
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid option - " + option, false);
		}
		HighlightElementManager.unHighlightElement(driver, grpWebElement);
	}
	
	public void verifyLocationsList(String option, WebElement locPerm, List<String> expectedLocList) throws IOException{
		String locations = locPerm.getText();
		HighlightElementManager.highlightElement(driver, locPerm);
		if(option.equalsIgnoreCase("all")){
			if(locations.equalsIgnoreCase("all")){
				ReportingUtils.log(driver, LogStatus.PASS, "Expected Location Access - " + option 
						+ " matched with Actual Location Access - " + locations, true);
			}else {
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Location Access - " + option 
						+ " did not match with Actual Location Access - " + locations, true);
			}
		}else if (option.equalsIgnoreCase("only Location selected")) {
			List<String> actualLocList = Arrays.asList(locations.split(", "));
			
			if(expectedLocList.size() != actualLocList.size()){
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Locations - " + expectedLocList 
						+ " but actual locations list is - " + actualLocList, true);
			}else{
				if(expectedLocList.containsAll(actualLocList)){
					ReportingUtils.log(driver, LogStatus.PASS, "Expected Locations - " + expectedLocList 
								+ " matched with actual locations list - " + actualLocList, true);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Locations - " + expectedLocList 
							+ " did not match with actual locations list - " + actualLocList, true);
				}
			}

		} else if (option.equalsIgnoreCase("All Location Except selected")) {
			if(!locations.startsWith("All Locations Except - ")){
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Locations are not excluded - " + locations, true);
			}else{
				List<String> actualLocList = Arrays.asList(locations.substring(locations.indexOf("- ") + 2).split(", "));
				if(expectedLocList.size() != actualLocList.size()){
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Locations excluded are - " + expectedLocList 
							+ " but actual locations excluded are - " + actualLocList, true);
				}else{
					if(expectedLocList.containsAll(actualLocList)){
						ReportingUtils.log(driver, LogStatus.PASS, "Expected Locations excluded - " + expectedLocList 
									+ " matched with actual locations excluded - " + actualLocList, true);
					}else{
						ReportingUtils.log(driver, LogStatus.FAIL, "Expected Locations excluded - " + expectedLocList 
								+ " did not match with actual locations excluded - " + actualLocList, true);
					}
				}
			}

		} else {
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid option - " + option, false);
		}
		HighlightElementManager.unHighlightElement(driver, locPerm);
	}

	public void verifyOtherRole(String role, String otherRole) throws IOException {
		HighlightElementManager.highlightElement(driver, qvLmtOtherPerms);
		if(role.equalsIgnoreCase("limited user")){
			if(qvLmtOtherPerms.getText().contains(otherRole)){
				ReportingUtils.log(driver, LogStatus.PASS, otherRole + " is enabled for limited user role", true);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, otherRole + " is not enabled for limited user role", true);
			}
		}else{
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
		}
		HighlightElementManager.unHighlightElement(driver, qvLmtOtherPerms);
	}

	public void enableProfileAdmin() throws IOException {
		try{
			click(lmtProfileAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User click on Profile Admin ", false);
			}catch(Exception e){
				ReportingUtils.log(driver, LogStatus.FAIL, "Unable to click on Profile Admin", true);
			}
			
	}
	
	public void enterCasesOptions(List<String> optionList) throws IOException {
		enterCasesOptionsList.clear();
		enterCasesOptionsList.addAll(optionList);
		for(String option : optionList){
			try{			
				if(option.equalsIgnoreCase("ER with Search")){
					if(enterCaseER.getAttribute("prevval") == null){
						click(enterCaseER);
					}
					click(enterCaseERInvPartyYes);
				}else if(option.equalsIgnoreCase("ER without Search")){
					if(enterCaseER.getAttribute("prevval") == null){
						click(enterCaseER);
					}
					click(enterCaseERInvPartyNo);
				}else if(option.equalsIgnoreCase("INV Intake Only")){
					if(enterCaseInv.getAttribute("prevval") == null){
						click(enterCaseInv);
					}
					click(enterCaseInvIntakeOnly);
				}else if(option.equalsIgnoreCase("INV Investigation Team")){
					if(enterCaseInv.getAttribute("prevval") == null){
						click(enterCaseInv);
					}
					click(enterCaseInvInvTeam);
				}else if(option.equalsIgnoreCase("INV Legal Advisor")){
					if(enterCaseInv.getAttribute("prevval") == null){
						click(enterCaseInv);
					}
					click(enterCaseInvLegalAdv);
				}else if(option.equalsIgnoreCase("ei")){
					if(enterCaseEI.getAttribute("prevval") == null){
						click(enterCaseEI);
					}
				}else if(option.equalsIgnoreCase("ph")){
					if(enterCasePHInt.getAttribute("prevval") == null){
						click(enterCasePHInt);
					}
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Invalid option - " + option, false);
					Assert.fail("Invalid option - " + option);
				}
				
				ReportingUtils.log(driver, LogStatus.PASS, "User selects options from Enter Cases permission - " + option, false);
			} catch (Exception e) {
				ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to selects options from Enter Cases permission ", false);
			}
		}
	}

	public void verifyEnterCasePerms() throws IOException {
		
		try{
			String enterCasePerms = qvEnterCasePerms.getText();
			List<String> actualCasesOptionsList = Arrays.asList(enterCasePerms.split(", "));
			HighlightElementManager.highlightElement(driver, qvEnterCasePerms);
			if(enterCasesOptionsList.size() == 4 && !enterCasesOptionsList.contains("without")){
				if(enterCasePerms.equals("All")){
					ReportingUtils.log(driver, LogStatus.PASS, "Enter Case contains expected permission - " + enterCasePerms, true);	
				}
			}else{
				if(enterCasesOptionsList.size() != actualCasesOptionsList.size()){
					ReportingUtils.log(driver, LogStatus.FAIL, "Expected Enter Cases permissions - " + enterCasesOptionsList + 
							", did not match with Actual Enter Cases permissions - " + actualCasesOptionsList, true);
				}else{
					if(enterCasesOptionsList.containsAll(actualCasesOptionsList)){
						ReportingUtils.log(driver, LogStatus.PASS, "Expected Enter Cases permissions - " + enterCasesOptionsList + 
								", matched with Actual Enter Cases permissions - " + actualCasesOptionsList, true);
					}else{
						ReportingUtils.log(driver, LogStatus.FAIL, "Expected Enter Cases permissions - " + enterCasesOptionsList + 
								", did not match with Actual Enter Cases permissions - " + actualCasesOptionsList, true);
					}
				}
			}
			HighlightElementManager.unHighlightElement(driver, qvEnterCasePerms);
			
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Unable to get Enter Cases permissions. " + e.getMessage(), true);
			Assert.fail("Unable to get Enter Cases permissions. " + e.getMessage());
		}
		
	}

	public void cancelCaseEntryPermi() throws IOException {
		try{
			click(enterCaseCancel);
			ReportingUtils.log(driver, LogStatus.PASS, "User click on Cancel button", true);
			TimeUnit.SECONDS.sleep(2);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Unable to click on cancel button. " + e.getMessage(), true);
			Assert.fail("Unable to cancel Enter case permissions. " + e.getMessage());
		}
		
	}

	public void caseTypePermValidation(String role) throws IOException {
		List<String> actualCaseTypePerms = new ArrayList<>();
		if(role.equalsIgnoreCase("View Cases")){
			String caseTypePerms = qvVcCaseTypePerms.getText();
			actualCaseTypePerms = Arrays.asList(caseTypePerms.split(", "));
			if(caseTypeOptionsList.containsAll(actualCaseTypePerms)){
				ReportingUtils.log(driver, LogStatus.PASS, "Expected Case Type permissions - " + caseTypeOptionsList + ", matched with actual Case Type permissions - " + actualCaseTypePerms, true);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Case Type permissions - " + caseTypeOptionsList + ", did not match with actual Case Type permissions - " + actualCaseTypePerms, true);
			}
			
		}else if(role.equalsIgnoreCase("Insights Reporting")){
			String caseTypePerms = qvRptCaseTypeNonePerms.getText();
			actualCaseTypePerms = Arrays.asList(caseTypePerms.split(", "));
			if(rptCaseTypeOptionsList.containsAll(actualCaseTypePerms)){
				ReportingUtils.log(driver, LogStatus.PASS, "Expected Case Type permissions - " + rptCaseTypeOptionsList + ", matched with actual Case Type permissions - " + actualCaseTypePerms, true);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, "Expected Case Type permissions - " + rptCaseTypeOptionsList + ", did not match with actual Case Type permissions - " + actualCaseTypePerms, true);
			}
			
		}else{
			ReportingUtils.log(driver, LogStatus.FAIL, "Invalid role - " + role, false);
			Assert.fail("Invalid role - " + role);
		}
		
		
		
		
	}

	public void enableInsightsRpt() throws IOException {
		try {
			click(rptAccessYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enables Insights Reporting Role sucessfully ", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to Insights Reporting Role ", true);
		}

		
	}

	public void instRptLmtNEditDetails(String accessType) throws IOException {
		
		try {
			if (accessType.contains("Case type")) {
				click(rptCaseTypeLmt);
				HighlightElementManager.highlightElement(driver, rptCaseTypeLmtViewDetail);
				click(rptCaseTypeLmtViewDetail);
				HighlightElementManager.unHighlightElement(driver, rptCaseTypeLmtViewDetail);
			} else if (accessType.contains("Group access")) {
				click(rptGrpAccessLmt);
				HighlightElementManager.highlightElement(driver, rptGrpAccessViewDet);
				click(rptGrpAccessViewDet);
				HighlightElementManager.unHighlightElement(driver, rptGrpAccessViewDet);
			} else if (accessType.contains("Issue access")) {
				click(rptIssueAccessLmt);
				HighlightElementManager.highlightElement(driver, rptIssueAccessViewDet);
				click(rptIssueAccessViewDet);
				HighlightElementManager.unHighlightElement(driver, rptIssueAccessViewDet);
			} else if (accessType.contains("Location access")) {
				click(rptLocAccessLmt);
				HighlightElementManager.highlightElement(driver, rptLocAccessViewDet);
				click(rptLocAccessViewDet);
				HighlightElementManager.unHighlightElement(driver, rptLocAccessViewDet);
			}else{
				ReportingUtils.log(driver, LogStatus.FAIL, "Invalid access type " + accessType, false);
				Assert.fail("Invalid access type " + accessType);
			}
			ReportingUtils.log(driver, LogStatus.PASS,
					"User selects option view cases " + accessType + " and Edit details", false);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL,
					"User is not able to select option view cases " + accessType + " and Edit details", true);
		}

	}

	public void rptCaseTypeOptions(List<String> optionList) throws IOException {
		rptCaseTypeOptionsList.clear();
		rptCaseTypeOptionsList.addAll(optionList);
		resetRptCaseTypeOptions();
		for(String option : optionList){
			try {
				if(option.equalsIgnoreCase("er")){
					click(rptCaseTypeER);
				}else if(option.equalsIgnoreCase("inv")){
					click(rptCaseTypeINV);
				}else if(option.equalsIgnoreCase("ei")){
					click(rptCaseTypeEI);
				}else if(option.equalsIgnoreCase("ph")){
					click(rptCaseTypePH);
				}else{
					ReportingUtils.log(driver, LogStatus.FAIL, "Invalid option - " + option,
							false);
					Assert.fail("Invalid option - " + option);
				}
				
				ReportingUtils.log(driver, LogStatus.PASS, "User selects options from Case Type permission - " + option, false);
			} catch (Exception e) {
				ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to selects options from Case Type permission ", false);
			}
		}
	}
	
	private void resetRptCaseTypeOptions() throws IOException {
		try {
			if (rptCaseTypeER.getAttribute("prevval") != null) {
				click(rptCaseTypeER);
			}

			if (rptCaseTypeINV.getAttribute("prevval") != null) {
				click(rptCaseTypeINV);
			}
			if (rptCaseTypeEI.getAttribute("prevval") != null) {
				click(rptCaseTypeEI);
			}

			if (rptCaseTypePH.getAttribute("prevval") != null) {
				click(rptCaseTypePH);
			}

		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, e.getMessage(), false);
			Assert.fail(e.getMessage());
		}

	}

	public void saveRptPermission(String caseType) throws IOException {
		try {
			if (caseType.equalsIgnoreCase("Case Type")) {
				click(rptCaseTypeSave);
				HighlightElementManager.highlightElement(driver, rptCaseTypeSave);
			} else if (caseType.equalsIgnoreCase("Group access")) {
				click(rptGrpSave);
				HighlightElementManager.highlightElement(driver, rptGrpSave);
			} else if (caseType.equalsIgnoreCase("Issue access")) {
				click(rptIssuesSave);
				HighlightElementManager.highlightElement(driver, rptIssuesSave);
			} else if (caseType.equalsIgnoreCase("Location access")) {
				click(rptLocSave);
				HighlightElementManager.highlightElement(driver, rptLocSave);
				click(rptLocAccessViewDet);
				irLocationsList.clear();
				List<WebElement> we = driver.findElements(By.xpath(RbacOR.rptLocSelected));
				for(WebElement e : we){
					irLocationsList.add(e.getText().trim());
				}
				click(rptLocSave);
			}
			ReportingUtils.log(driver, LogStatus.PASS, "User saved the " + caseType + "permissions", true);
		} catch (Exception e) {
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to save the " + caseType + "permissions",
					true);
		}

	}

	public void enableAnalytics() throws IOException {
		try{
			click(rptInsightAnltsYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable Insights Analytics successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to User enable Insights Analytics ", false);
		}
		
	}

	public void enableDashboard() throws IOException {
		try{
			click(rptInsightDBYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable Dashboard successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to enable Dashboard Analytics ", false);
		}
	}

	public void enableBenchmark() throws IOException {
		try{
			click(rptInsightBMYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable Benchmarks Analytics successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to enable Benchmarks Analytics ", false);
		}
	}

	public void enableINVPartyDetail() throws IOException {
		try{
			click(rptInvPrtDtlsYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable Involved party details successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to enable Involved party details ", false);
		}
	}

	public void enableEvntBsdInsights() throws IOException {
		try{
			click(rptEvntBsdInstYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable event based insights successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to enableevent based insights ", false);
		}
		
	}

	public void enableMnrgCaseApprover() throws IOException {
		try{
			click(isMngrCaseApproverYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable Manager case Approver successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to enable Manager case Approver ", false);
		}
	}


	public void verifyOtherPerms(String role) throws IOException {
		String actualOtherPerms = qvOtherPerms.getText();
		HighlightElementManager.highlightElement(driver, qvOtherPerms);
		if(actualOtherPerms.contains(role)){
			ReportingUtils.log(driver, LogStatus.PASS, "Actual Other Permissions - " + actualOtherPerms + " contain expected role - " + role , true);
		}else{
			ReportingUtils.log(driver, LogStatus.PASS, "Actual Other Permissions - " + actualOtherPerms + " does not contain expected role - " + role , true);
		}
		HighlightElementManager.unHighlightElement(driver, qvOtherPerms);
	}

	public void enableOtherConfigAdmin() throws IOException {
		try{
			click(isConfigAdminYes);
			ReportingUtils.log(driver, LogStatus.PASS, "User enable Configuration Admin successfully ", false);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "User is not able to enable Configuration Admin ", false);
		}
	}


}
