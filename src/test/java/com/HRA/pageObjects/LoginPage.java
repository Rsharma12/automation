package com.HRA.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	//id = "Email"

	@FindBy(xpath = ".//input[@id='Email']")
	WebElement email;

	public WebElement email() {
		return email;
	}
//	id = "Password"
	@FindBy(xpath = "//input[@id='Password']")
	WebElement password;

	public WebElement password() {
		return password;
	}

	@FindBy(xpath = "//input[@value='Login']")
	WebElement login;

	public WebElement login() {
		return login;
	}
	
	@FindBy(xpath = "//input[@class='c-btn jq-login-button']")
    WebElement newLogin;
	public WebElement newLogin()
	{
		return newLogin;
	}
	
	
	@FindBy(xpath = "//input[@value='I Agree']")
	WebElement iAgree;

	public WebElement iAgree() {
		return iAgree;
	}
	
	@FindBy(xpath = "//input[contains(@value,'I Agree')]")
	WebElement newIagree;
	public WebElement newIagree()
	{
		return newIagree;
	}

//	@FindBy(xpath = "//span[contains(text(),'kaur')]")
	@FindBy(xpath = "//a[@class='c-header__userName js-header-menulink jq-userName']/span")
	WebElement name;

	public WebElement name() {
		return name;
	}

	@FindBy(xpath = "//a[@class='userLogout']")
	WebElement logout;

	public WebElement logout() {
		return logout;
	}
	
	@FindBy(xpath = "//*[@id=\"headerRightCol\"]/li[3]/div/div/ul/li[1]/a")
	WebElement Preference;

	public WebElement Preference() {
		return Preference;
	}
	
	@FindBy(xpath = "//*[@id=\"prePassword\"]")
	WebElement ChangePassword;

	public WebElement ChangePassword() {
		return ChangePassword;
	}

	@FindBy(xpath = "//*[@id=\"OldPassword\"]")
	WebElement TemporaryOldPassword;

	public WebElement TemporaryOldPassword() {
		return TemporaryOldPassword;
	}
	
	@FindBy(xpath = "//*[@id=\"Password\"]")
	WebElement NewPassword;

	public WebElement NewPassword() {
		return NewPassword;
	}

	@FindBy(xpath = "//*[@id=\"ConfirmPassword\"]")
	WebElement Reenternewpassword;

	public WebElement Reenternewpassword() {
		return Reenternewpassword;
	}

	@FindBy(xpath = "//*[@id=\"change-password\"]/div[2]/input")
	WebElement Savebuttonpreference;

	public WebElement Savebuttonpreference() {
		return Savebuttonpreference;
	}
	
	@FindBy(xpath = "//*[@id=\"change-password\"]/div[1]/div[1]/div[2]/span/span")
	WebElement Blankpassword;

	public WebElement Blankpassword() {
		return Blankpassword;
	}
	
	@FindBy(xpath = "//*[@id=\"change-password\"]/div[1]/div[3]/div[2]/span/span")
	WebElement Mismatchpassword;

	public WebElement Mismatchpassword() {
		return Mismatchpassword;
	}

	
	@FindBy(xpath = "")
	WebElement Changepassword;

	public WebElement Changepassword() {
		return Changepassword;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
