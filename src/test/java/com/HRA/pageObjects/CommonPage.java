package com.HRA.pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.HRA.helper.HighlightElementManager;
import com.HRA.helper.ReportingUtils;
import com.HRA.or.CommonOR;
import com.HRA.testBase.TestBase;
import com.relevantcodes.extentreports.LogStatus;

import lombok.Data;
@Data

public class CommonPage extends TestBase {
	
	WebDriver driver;

	public CommonPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = CommonOR.myCases) public WebElement myCases;
	@FindBy(xpath = CommonOR.myCases) public WebElement myPriorityCases;
	@FindBy(xpath = CommonOR.myCases) public WebElement allCases;
	@FindBy(xpath = CommonOR.cases) public WebElement cases;
	@FindBy(xpath = CommonOR.searchCaseHeader) public WebElement searchCaseHeader;
	@FindBy(xpath = CommonOR.searchCaseType) public WebElement searchCaseType;
	@FindBy(xpath = CommonOR.search) public WebElement search;
	@FindBy(xpath = CommonOR.selectFromSearch) public WebElement selectFromSearch;
	@FindBy(xpath = CommonOR.openCases) public WebElement openCases;
	@FindBy(xpath = CommonOR.employeeRelations) public WebElement employeeRelations;
	@FindBy(xpath = CommonOR.investigation) public WebElement investigation;
	@FindBy(xpath = CommonOR.caseLink) public WebElement caseLink;
	
	@FindBy(xpath = CommonOR.adminstrationTab) public WebElement adminstrationTab;
	@FindBy(xpath = CommonOR.roles) public WebElement roles;
	@FindBy(xpath = CommonOR.roleName) public WebElement roleName;
	@FindBy(xpath = CommonOR.searchRoleName) public WebElement searchRoleName;
	@FindBy(xpath = CommonOR.activeRoleYes) public WebElement activeRoleYes;
	@FindBy(xpath = CommonOR.activeRoleNo) public WebElement activeRoleNo;
	@FindBy(xpath = CommonOR.createNewRole) public WebElement createNewRole;
	@FindBy(xpath = CommonOR.saveNewRole) public WebElement saveNewRole;
	@FindBy(xpath = CommonOR.searchRole) public WebElement searchRole;
	
	
	@FindBy(xpath = CommonOR.caseNumberHeader)
	public WebElement caseNumberHeader;
	
//	private String caseCount = "";
	String strRoleName = "TestRole_";
	
	public void userClickOnTab(String caseType) throws Throwable {
		if (caseType.equalsIgnoreCase("open cases")) {
			click(openCases);
		} else if (caseType.equalsIgnoreCase("ER")) {
			click(employeeRelations);
		} else if (caseType.equalsIgnoreCase("INV")) {
			click(investigation);
		}
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(caseNumberHeader));
		ReportingUtils.log(driver, LogStatus.PASS, "Clicked on " + caseType, true);
	}

	public void selectCase(String caseType) throws Throwable {
		click(caseLink);
		ReportingUtils.log(driver, LogStatus.PASS, "Click on " + caseType + " case successful.", true);
	}
	
	public void adminTab() throws Throwable {
		HighlightElementManager.highlightElement(driver, adminstrationTab);
		click(adminstrationTab);
		HighlightElementManager.unHighlightElement(driver, adminstrationTab);
		HighlightElementManager.highlightElement(driver, roles);
		click(roles);
		HighlightElementManager.unHighlightElement(driver, roles);
		ReportingUtils.log(driver, LogStatus.PASS, "User select administration tab and selected roles ", false);
		
	}
	
	public void createActiveNewRole() throws Throwable {
		HighlightElementManager.highlightElement(driver, createNewRole);
		click(createNewRole);
		click(roleName);
		strRoleName = strRoleName + System.currentTimeMillis();
		input(roleName, strRoleName);
		click(activeRoleYes);
		ReportingUtils.log(driver, LogStatus.INFO, "User creates an active new role ", true);
		click(saveNewRole);
		ReportingUtils.log(driver, LogStatus.PASS, "Active new role created " + strRoleName, false);
		
	}
	
	public void searchRole() throws Throwable {
		click(searchRoleName);
		HighlightElementManager.highlightElement(driver, searchRole);
		input(searchRoleName, strRoleName);
		click(searchRole);
		ReportingUtils.log(driver, LogStatus.PASS, "User search the role successfully ", false);
		HighlightElementManager.unHighlightElement(driver, searchRole);
	}

	public void userClickOnCaseCategoryTab(String casesCategory) throws Throwable {
		
		if (casesCategory.equalsIgnoreCase("My Cases")) {
			click(myCases);
			HighlightElementManager.highlightElement(driver, myCases);
		} else if (casesCategory.equalsIgnoreCase("My Priority Cases")) {
			click(myPriorityCases);
			HighlightElementManager.highlightElement(driver, myPriorityCases);
		} else if (casesCategory.equalsIgnoreCase("All cases")) {
			click(allCases);
			HighlightElementManager.highlightElement(driver, allCases);
		}
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(caseNumberHeader));
		ReportingUtils.log(driver, LogStatus.PASS, " when user clicked on " + casesCategory, true);
	}

	
}
