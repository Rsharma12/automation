package com.HRA.or;

public class CaseInsightOR {
	
	private CaseInsightOR() {
	    throw new IllegalStateException("All Cases Object Repository class");
	}

	//CaseInsights Bar
	public static final String caseInsights = "//span[contains(text(),'Case Insights')]";
	public static final String caseInsightsPlusBtn = "//span[@class='type-role-plus-btn']";
	public static final String caseInsightsMinusBtn = "//span[contains(@class,'type-role-minus-btn')]";
	public static final String caseTypeHeader = "//span[text()='Case Insights']/ancestor::div/following-sibling::div/ul/li[1]";
	public static final String monthsHeader = "//span[text()='Case Insights']/ancestor::div/following-sibling::div/ul/li[2]";
	
	public static final String erCaseType = "//label[contains(text(),'ER')]";
	public static final String invCaseType = "//label[contains(text(),'INV')]";
	public static final String allCaseType = "//label[contains(text(),'All')]";
	
	public static final String caseInsightsApply = "//a[contains(@class,'caseinsightsapply')]";
	public static final String months3 = "//div[@class='box-minmax']/span[text()='3']";
	public static final String months6 = "//div[@class='box-minmax']/span[text()='6']";
	public static final String months12 = "//div[@class='box-minmax']/span[text()='12']";
	public static final String months24 = "//div[@class='box-minmax']/span[text()='24']";
	public static final String rollingMonthsSelection = "//input[@id='rs-range-line']";
	
	public static final String groupTile = "//span[contains(text(),'Group')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]";
	public static final String groupName = "//span[contains(text(),'Group')]/ancestor::div/following-sibling::div/span[contains(@class,'sub-title-bt')]/a";
	public static final String groupGraph = "(//span[contains(text(),'Group')]/following-sibling::span/i)[1]";
	
	
	public static final String locationTile = "//span[contains(text(),'Location')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]";
	public static final String locationName = "//span[contains(text(),'Location')]/ancestor::div/following-sibling::div/span[contains(@class,'sub-title-bt')]/a";
	public static final String locationGraph = "(//span[contains(text(),'Location')]/following-sibling::span/i)[1]";
	
	
	public static final String issueCategoryTile = "//span[contains(text(),'Issue Category')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]";
	public static final String issueCategoryName= "//span[contains(text(),'Issue Category')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]/a";
	public static final String issueCategoryFooter= "//span[text()='Issue Category']/ancestor::div[1]//following-sibling::div[contains(@class,'action-footer')]/span/strong";
	public static final String issueCategoryDownArrow = "//span[text()='Issue Category']/ancestor::div[contains(@class,'tiels-header')]/following-sibling::div[contains(@class,'tiels-footer')]/div[@class='action-arrow']/i";
	public static final String issueCategoryGraph = "(//span[contains(text(),'Issue Category')]/following-sibling::span/i)[1]";
	public static final String issueCategoryTags = "//div[@id='arrow-pop-issuecategory']//a";
	public static final String issueCatFiltersTags = "//h4[text()='Issue Category']/following-sibling::ul/li";
	
	public static final String involvedPartyHeader = "//span[contains(text(),'Involved Party')]";
	public static final String involvedPartyTile = "//span[contains(text(),'Involved Party')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]";
	public static final String involvedPartyDownArrow = "//span[text()='Involved Party']/ancestor::div[contains(@class,'tiels-header')]/following-sibling::div[contains(@class,'tiels-footer')]/div[@class='action-arrow']/i";
	public static final String involvedPartyFooter = "//span[text()='Involved Party']/ancestor::div[1]//following-sibling::div[contains(@class,'action-footer')]/span/strong";
	public static final String involvedPartyTags = "//div[@id='arrow-pop-involvedparty']//a";
	public static final String selectInvolvedParty = ""; //select involve party from list
	public static final String involvedPartyGraph = "(//span[contains(text(),'Involved Party')]/following-sibling::span/i)[1]";
	public static final String invPartyFiltersTags = "//h4[contains(text(),'Involved Party')]/following-sibling::ul/li";
	
	public static final String actionHeader = "//span[text()='Action']";
	public static final String actionFooter = "//span[text()='Action']/ancestor::div[1]//following-sibling::div[contains(@class,'action-footer')]/span/strong";
	public static final String actionDownArrow = "//span[text()='Action']/ancestor::div[contains(@class,'tiels-header')]/following-sibling::div[contains(@class,'tiels-footer')]/div[@class='action-arrow']/i";
	public static final String actionTags = "//div[@id='arrow-pop-action']//a";
	public static final String actionTile = "//span[contains(text(),'Action')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]";
	public static final String actionGraph = "(//span[contains(text(),'Action')]/following-sibling::span/i)[1]";
	public static final String actionName = "";
	public static final String actionFiltersTags = "//h4[contains(text(),'Action')]/following-sibling::ul/li";
	
	public static final String notificationMethodHeader = "//span[text()='Notification Method']";
	public static final String notificationMethodTile = "//span[contains(text(),'Notification Method')]/ancestor::div/following-sibling::div/span[contains(@class,'heading5')]";
	public static final String notificationMethodGraph = "(//span[contains(text(),'Notification Method')]/following-sibling::span/i)[1]";
	public static final String notifMethodName = "//span[contains(text(),'Notification Method')]/ancestor::div/following-sibling::div/span[contains(@class,'sub-title-bt')]/a";
	
	public static final String maxGraph = "//div[@class='expend-grap-icon']/i";
	public static final String closeMaxGraph = "//h4[text()='Line Chart']/following-sibling::a";
	public static final String closeGraph = "//span[contains(@class,'jq-graph-popup-btn')]/i";
	public static final String graphData = "//*[name()='g'][contains(@class,'fusioncharts-datalabels')]/*[name()='text']";
	
		
	public static final String rightDirectionArrow = "//button[contains(@class,'owl-next')]//img";
	public static final String leftDirectionArrow = "//button[contains(@class,'owl-prev disabled')]//img";
		
	//ribbon
	public static final String caseLog = "//span[contains(@class,'fa fa-clipboard')]";
	public static final String caseNumber = "";
	public static final String caseLogCategory = "//select[@id='tasktracker-TaskCategoryList']";
	
	public static final String scheduleReminder = "//span[@class='fa fa-calendar-minus-o']";
	public static final String scheduleReminderTitle = "//h4[contains(text(),'Schedule Reminder')]";
	
	public static final String selectedTags = "//span[contains(text(),'Selected Tags')]";
	public static final String tagGroup = "//h4[contains(text(),'Group')]/following-sibling::ul/li";
	public static final String tagLocation = "//h4[contains(text(),'Location')]/following-sibling::ul/li";
	public static final String tagNotifMethod = "//h4[contains(text(),'Notification Method')]/following-sibling::ul/li";
	public static final String closeTagGroup = "//h4[contains(text(),'Group')]/following-sibling::span/i";
	public static final String closeTagLocation = "//h4[contains(text(),'Location')]/following-sibling::span/i";
	public static final String closeTagIssueCategory = "//h4[contains(text(),'Issue Category')]/following-sibling::span/i";
	public static final String closeTagInvolvedParty = "//h4[contains(text(),'Involved Party')]/following-sibling::span/i";
	public static final String closeTagAction = "//h4[contains(text(),'Action')]/following-sibling::span/i";
	public static final String closeNotifMethod = "//h4[contains(text(),'Notification Method')]/following-sibling::span/i";
	
	public static final String applyTag = "//a[contains(@class,'case-insights-search')]";
	public static final String filteredResults = "";
	public static final String filteredCasesCount = "//span[text()='Filtered Results']/ancestor::h6/following-sibling::div//span[contains(@class,'filteredCasesCount')]";
	public static final String filteredCasesStatus = "//span[text()='Filtered Results']/ancestor::h6/following-sibling::div//span[contains(@class,'filteredcasestatus')]";
	public static final String filteredResultRows = "//div[contains(@class,'admingrid-panel')][not(@style='display: none;')]//table/tbody/tr";
	public static final String filteredResultsFooter = "//div[contains(text(),'Showing')]";
	
	public static final String switchHandle = "//span[@class='c_switch-handle']";
	public static final String openCases = "//span[@title='open']";
	public static final String closedCases = "//span[@title='closed']";
	
	
	
	
	
	
}
