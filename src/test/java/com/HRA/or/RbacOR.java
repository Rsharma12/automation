package com.HRA.or;

public class RbacOR {
	private RbacOR() {
	    throw new IllegalStateException("All Cases Object Repository class");
	}


	public static final String mpEditPermi = "//a[contains(text(),'Edit Permissions')]";
	
	public static final String mpUserRoleYes = "//li[text()='Yes']/input[@name='IsManagerAccess']";
	public static final String mpUserRoleNo = "//li[text()='No']/input[@name='IsManagerAccess']";
	public static final String mpGrpAccessAll = "//li[text()='All']/input[@name='IsManagerAccessAllGroups']";
	public static final String mpGrpAccessLmt = "//li[text()='Limited']/input[@name='IsManagerAccessAllGroups']";
	public static final String mpViewDetGrpLmt = "//label[text()='Group Access']/following-sibling::div/ul/li[contains(@class,'access-disable')]//a[contains(@class,'TreeDropDownRequired')][contains(text(),'View')]";
	public static final String mpViewDetLocLmt = "//label[text()='Location Access']/following-sibling::div/ul/li[contains(@class,'access-disable')]//a[contains(@class,'TreeDropDownRequired')][contains(text(),'View')]";
	public static final String mpLocAccessAll = "//li[text()='All']/input[@name='IsManagerAccessAllLocations']";
	public static final String mpLocAccessLmt = "//li[text()='Limited']/input[@name='IsManagerAccessAllLocations']";
	public static final String mpGrpOnlyAccess = "(//input[@name='IsExcludedGroupManagerAccess'])[1]";
	public static final String mpGrpExceptAccess = "(//input[@name='IsExcludedGroupManagerAccess'])[2]";
	public static final String mpSelectGrp = "//div[@id='manageraccess-group']//i[contains(@class,'icon--blue')]";
	public static final String mpGrpNoChild = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String mpGrpHasChild = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String mpGrpChildren = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String mpGrpSelected = "//div[@id='manageraccess-group']//div[contains(@class,'selectedGroupsInRole')]//li";
	public static final String mpGrpSave = "//div[@id='manageraccess-group']//input[@name='Save']";
	public static final String mpGrpClearAll = "//div[@id='manageraccess-group']//a[contains(text(),'Clear All')]";
	public static final String mpLocOnly = "(//input[@name='IsExcludedLocationManagerAccess'])[1]";
	public static final String mpLocExcept = "(//input[@name='IsExcludedLocationManagerAccess'])[2]";
	public static final String mpSelectLoc = "//div[@id='manageraccess-location']//i[contains(@class,'list-ul')]";
	public static final String mpLocNoChild = "(//span[contains(text(),'Select Location')]/ancestor::div[1]/following-sibling::div//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String mpLocHasChild = "(//span[contains(text(),'Select Location')]/ancestor::div[1]/following-sibling::div//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'haschild')]/following-sibling::div[1])[2]";
	public static final String mpLocChildren = "(//span[contains(text(),'Select Location')]/ancestor::div[1]/following-sibling::div//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'haschild')]/following-sibling::div[1])[2]/following-sibling::div//div[contains(@class,'statictree-data')]";
	public static final String mpLocSelected = "//div[@id='manageraccess-location']//div[contains(@class,'selectedLocationsInRole')]//li";
	public static final String mpLocSave = "//div[@id='manageraccess-location']//input[@name='Save']";
	public static final String mpLocClearAll = "//div[@id='manageraccess-location']//a[contains(text(),'Clear All')]";
	public static final String qvMpGrpPerm = "//div[contains(@class,'rolemanageraccess')]//span[contains(text(),'Group Access')]/following-sibling::span";
	public static final String qvMpLocPerm = "//div[contains(@class,'rolemanageraccess')]//span[contains(text(),'Location Access')]/following-sibling::span";
	
	//Role is a Limited user
	public static final String limitedRoleYes = "//li[text()='Yes']/input[@name='IsLimitedUserAccess']";
	public static final String limitedRoleNo = "//li[text()='NO']/input[@name='IsLimitedUserAccess']";
	public static final String lmtProfileAdminYes = "//li[contains(text(),'Yes')]//input[@name='IsProfileAdmin']";
	public static final String lmtProfileAdminNo = "//li[contains(text(),'No')]//input[@name='IsProfileAdmin']";
	public static final String lmtPAGrpAccessAll = "//li[contains(text(),'All')]//input[@name='IsProfileAccessAllGroups']";
	public static final String lmtPAGrpAccessLmt = "//li[contains(text(),'Limited')]//input[@name='IsProfileAccessAllGroups']";
	public static final String lmtPAGrpAccessViewDet = "(//label[contains(text(),'Profile Admin')]/ancestor::div[contains(@class,'selectRole')]//a)[1]";
	public static final String lmtPAGrpOnly = "(//li//input[@name='IsExcludedGroupProfileAccess'])[1]";
	public static final String lmtPAGrpClearAll = "//div[@id='group-profile-access']//a[contains(text(),'Clear All')]";
	public static final String lmtPAGrpExcept = "(//li//input[@name='IsExcludedGroupProfileAccess'])[2]";
	public static final String lmtPASelect = "//div[@id='group-profile-access']//i[contains(@class,'icon--blue')]";
	public static final String lmtPAGrpNoChild = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String lmtPAGrpHasChild = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String lmtPAGrpChildren = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String lmtPAGrpSelected = "//div[@id='group-profile-access']//div[contains(@class,'selectedGroupsInRole')]//li";
	public static final String lmtPAGrpPermSave = "//div[@id='group-profile-access']//input[@name='Save']";
	public static final String lmtPALocAccessAll = "//li[contains(text(),'All')]//input[@name='IsProfileAccessAllLocations']";
	public static final String lmtPALocAccessLmt = "//li[contains(text(),'Limited')]//input[@name='IsProfileAccessAllLocations']";
	public static final String lmtPALocAccessViewDet = "(//label[contains(text(),'Profile Admin')]/ancestor::div[contains(@class,'selectRole')]//a)[2]";
	public static final String lmtPALocOnly = "(//li//input[@name='IsExcludedLocationProfileAccess'])[1]";
	public static final String lmtPALocExcept = "(//li//input[@name='IsExcludedLocationProfileAccess'])[2]";
	public static final String lmtPALocSelect = "//div[@id='location-profile-access']//i[@class='fa fa-list-ul']";
	public static final String lmtPALocNoChild = "(//span[contains(text(),'Select Location')]/ancestor::div[1]/following-sibling::div//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String lmtPALocpHasChild = "(//span[contains(text(),'Select Location')]/ancestor::div[1]/following-sibling::div//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'haschild')]/following-sibling::div[1])[2]";
	public static final String lmtPALocChildren = "(//span[contains(text(),'Select Location')]/ancestor::div[1]/following-sibling::div//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'haschild')]/following-sibling::div[1])[2]/following-sibling::div//div[contains(@class,'statictree-data')]";
	public static final String lmtPALocSelected = "//div[@id='location-profile-access']//div[contains(@class,'selectedLocationsInRole')]//li";
	public static final String lmtConfigAdminYes = "//li[contains(text(),'Yes')]//input[@name='IsConfigAdmin']";
	public static final String lmtConfigAdminNo = "//li[contains(text(),'No')]//input[@name='IsConfigAdmin']";
	public static final String lmtDataInteAdminYes = "//li[contains(text(),'Yes')]//input[@name='IsDataImportAdmin']";
	public static final String lmtDataInteAdminNo = "//li[contains(text(),'Yes')]//input[@name='IsDataImportAdmin']";
	public static final String lmtPALocClearAll = "//div[@id='location-profile-access']//a[contains(text(),'Clear All')]";
	public static final String lmtPALocPermSave = "//div[@id='location-profile-access']//input[@name='Save']";
	public static final String qvLmtPAGrpPerm = "//span[contains(@class,'ProfileGroupAccess')]";
	public static final String qvLmtPALocPerm = "//span[contains(@class,'ProfileLocationAccess')]";
	public static final String qvLmtOtherPerms = "//span[contains(@class,'jq-OtherPermissions')]";
	
	//permSave
	//This Role can Enter Cases	
	public static final String enterCasesRoleYes = "//li[text()='Yes']/input[@name='IsCreateCaseAccess']";
	public static final String enterCasesRoleNo = "//li[text()='No']/input[@name='IsCreateCaseAccess']";
	public static final String enterCaseViewDetails = "//label[contains(text(),'Enter case')]/following-sibling::div//li[contains(@class,'access-disable')]//a";
	public static final String enterCaseER = "//input[@id='IsERCaseAccess']";
	public static final String enterCaseERInvPartyYes = "//input[@id='role-ERSearchInvPartyYes']";
	public static final String enterCaseERInvPartyNo = "//input[@id='role-ERSearchInvPartyNo']";
	public static final String enterCaseInv = "//input[@id='IsINVCaseAccess']";
	public static final String enterCaseInvIntakeOnly = "//input[@id='role-intakeonly']";
	public static final String enterCaseInvInvTeam = "//input[@id='role-investigationteam']";
	public static final String enterCaseInvLegalAdv = "//input[@id='role-legaladvisor']";
	public static final String enterCaseEI = "//input[@name='IsEIAccess']";
	public static final String enterCasePHInt = "//input[@name='IsPHAccess']";
	public static final String enterCaseSave = "//div[@id='case-entry-access']//a[text()='Save']";
	public static final String enterCaseCancel = "//div[@id='quick-view-permissions-modal']//a[contains(@class,'close')][contains(text(),'Cancel')]";
	public static final String qvEnterCasePerms = "//span[contains(@class,'caseaccesspermission')]";
	
//	This Role can View cases
	public static final String viewCasesYes = "//li[text()='Yes']/input[@name='IsCaseViewAccess']";
	public static final String viewCasesNo = "//li[text()='No']/input[@name='IsCaseViewAccess']";
	public static final String vcCaseTypeAll = "//li[text()='All']/input[@name='IsViewAccessAllCaseTypes']";
	public static final String vcCaseTypeLmt = "//li[text()='Limited']/input[@name='IsViewAccessAllCaseTypes']";
	public static final String vcCaseTypeViewDetails = "(//label[contains(text(),'View cases')]/ancestor::div[contains(@class,'selectRole')]//a)[1]";
	public static final String vcCaseTypeER = "//input[contains(@name,'IsViewERAccess')]";
	public static final String vcCaseTypeINV = "//input[contains(@name,'IsViewINVAccess')]";
	public static final String vcCaseTypeEI = "//input[contains(@name,'IsViewEIAccess')]";
	public static final String vcCaseTypePH = "//input[contains(@name,'IsViewPHAccess')]";
	public static final String vcCaseTypeSave = "//div[@id='roles-viewaccesscasetypes-modal']//a[contains(@class,'btnsavedetail jq-btnvalidate')][contains(text(),'Save')]";
	public static final String vcGrpAccessAll = "//li[contains(text(),'All')]//input[@name='IsCaseViewAccessAllGroups']";
	public static final String vcGrpAccessLmt = "//li[text()='Limited']/input[@name='IsCaseViewAccessAllGroups']";
	public static final String vcGrpAccessLmtViewDetails = "(//label[contains(text(),'View cases')]/ancestor::div[contains(@class,'selectRole')]//a)[2]";
	public static final String vcGrpOnlyAccess = "//input[@value='false'][@name='IsExcludedGroupViewAccess']";
	public static final String vcGrpExceptAccess = "//input[@value='true'][@name='IsExcludedGroupViewAccess']";
	public static final String vcSelectGrp = "//div[@id='group-access']//i[contains(@class,'icon--blue')]";
	public static final String vcGrpAccessclearAll = "//div[@id='group-access']//a[contains(text(),'Clear All')]";
	public static final String vcGrpNoChild = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String vcGrpHasChild = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String vcGrpChildren = "(//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String vcGrpSelected = "//div[@id='group-access']//div[contains(@class,'selectedGroupsInRole')]//li";
	public static final String vcGrpSave = "//div[@id='group-access']//input[@name='Save']";
	public static final String vcGrpClearAll = "//div[@id='group-access']//a[contains(@class,'c-btn-default jq-clearAll')][contains(text(),'Clear All')]";
	public static final String vcIssueAccessAll = "//li[contains(text(),'All')]//input[@name='IsCaseViewAllAccessIssueCategories']";
	public static final String vcIssueAccessLmt = "//li[text()='Limited']/input[@name='IsCaseViewAllAccessIssueCategories']";
	public static final String vcIssueAccessLmtViewDetails = "(//label[contains(text(),'View cases')]/ancestor::div[contains(@class,'selectRole')]//a)[3]";
	public static final String vcIssuesOnlyAccess = "//input[@value='false'][@name='IsExcludedIssueViewAccess']";
	public static final String vcIssuesExceptAccess = "//input[@value='true'][@name='IsExcludedIssueViewAccess']";
	public static final String vcSelectIssues = "//div[@id='issue-access']//i[contains(@class,'icon--blue')]";
	public static final String vcIssuesNoChild = "";
	public static final String vcIssuesHasChild = "(//div[@id='issue-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String vcIssuesChildren = "(//div[@id='issue-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String vcIssuesSelected = "//div[@id='issue-access']//div[contains(@class,'selectedIssuesInRole')]//li/a";
	public static final String vcIssueAccessClearAll = "//div[@class='o-col-row']//a[contains(text(),'Clear All')]";
	public static final String vcIssuesSave = "//div[@id='issue-access']//input[@name='Save']";
	public static final String vcLocAccessAll = "//li[contains(text(),'All')]//input[@name='IsCaseViewAccessAllLocations']";
	public static final String vcLocAccessLmt = "//li[text()='Limited']/input[@name='IsCaseViewAccessAllLocations']";
	public static final String vcLocAccessLmtViewDetails = "(//label[contains(text(),'View cases')]/ancestor::div[contains(@class,'selectRole')]//a)[4]";
	public static final String vcLocOnlyAccess = "//input[@value='false'][@name='IsExcludedLocationViewAccess']";
	public static final String vcLocExceptAccess = "//input[@value='true'][@name='IsExcludedLocationViewAccess']";
	public static final String vcSelectLoc = "//div[@id='location-access']//i[@class='fa fa-list-ul']";
	public static final String vcLocHasChild = "(//div[@id='location-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String vcLocNoChild = "(//div[@id='location-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String vcLocSelected = "//div[@id='location-access']//div[contains(@class,'selectedLocationsInRole')]//li";
	public static final String vcLocClearAll = "//div[@id='location-access']//a[contains(text(),'Clear All')]";
	public static final String vcLocSave = "//div[@id='location-access']//input[@name='Save']";
	public static final String qvVcCaseTypePerms = "//span[contains(@class,'ViewCaseTypeAccess')]";
	public static final String qvVcGrpPerm = "//div[contains(@class,'rolemoreoption')]//span[contains(@class,'ViewGroupAccess')]";
	public static final String qvVcIssueAccessPerm = "//span[contains(@class,'ViewIssueAccess')]";
	public static final String qvVcLocPerm = "//div[contains(@class,'rolemoreoption')]//span[contains(@class,'ViewLocationAccess')]";
	
	
	public static final String caGrpAccessEdit = "";
	
	//This Role is a case admin
	public static final String caViewCasePermYes = "//li[contains(text(),'Yes')]//input[contains(@name,'IsCaseAdmin')]";
	public static final String caViewCasePermNo = "//li[contains(text(),'No')]//input[contains(@name,'IsCaseAdmin')]";
	public static final String caGrpAccessAll = "//li[text()='All']/input[@name='IsAdminAccessAllGroups']";
	public static final String caGrpAccessLimited = "//li[text()='Limited']/input[@name='IsAdminAccessAllGroups']";
	public static final String caGrpAccessLmtViewDetail = "(//label[contains(text(),'Case Admin')]/ancestor::div[contains(@class,'selectRole')]//a)[1]";
	public static final String caGrpOnlyAccess = "//input[@value='false'][@name='IsExcludedGroupAdminAccess']";
	public static final String caGrpExceptAccess = "//input[@value='true'][@name='IsExcludedGroupAdminAccess']";
	public static final String caSelectGrp = "//div[@id='group-admin-access']//i[contains(@class,'icon--blue')]";
	public static final String caGrpNoChild = "(//div[@id='group-admin-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String caGrpHasChild = "(//div[@id='group-admin-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String caGrpChildren = "(//div[@id='group-admin-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String caGrpSelected = "//div[@id='group-admin-access']//div[contains(@class,'selectedGroupsInRole')]//li";
	public static final String caGrpClearAll = "//div[@id='group-admin-access']//a[contains(text(),'Clear All')]";
	public static final String caGrpSave = "//div[@id='group-admin-access']//input[@name='Save']";
	public static final String caLocAccessAll = "//li[contains(text(),'All')]//input[contains(@name,'IsAdminAccessAllLocations')]";
	public static final String caLocAccessLimited = "//li[contains(text(),'Limited')]//input[@name='IsAdminAccessAllLocations']";
	public static final String caLocAccessLmtViewDetail = "(//label[contains(text(),'Case Admin')]/ancestor::div[contains(@class,'selectRole')]//a)[2]";
	public static final String caLocOnlyAccess = "//input[@value='false'][@name='IsExcludedLocationAdminAccess']";
	public static final String caLocExceptAccess = "//input[@value='true'][@name='IsExcludedLocationAdminAccess']";
	public static final String caSelectLoc = "//div[@id='location-admin-access']//i[contains(@class,'fa fa-list-ul')]";
	public static final String caLocNoChild = "(//div[@id='location-admin-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String caLocHasChild = "(//div[@id='location-admin-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String caLocSelected = "//div[@id='location-admin-access']//div[contains(@class,'selectedLocationsInRole')]//li";
	public static final String caLocSave = "//div[@id='location-admin-access']//div[contains(@class,'c-modal__btn')]//input[contains(@name,'Save')]";
	public static final String caLocClearAll = "//div[@id='location-admin-access']//a[contains(text(),'Clear All')]";
	public static final String qvCaGrpPerm = "//span[contains(@class,'AdminGroupAccess')]";
	public static final String qvCaLocPerm = "//span[contains(@class,'AdminLocationAccess')]";
	
//	This role is a Profile Admin
	public static final String profileAdminYes = "//li[text()='Yes']/input[@name='IsProfileAdmin']";
	public static final String profileAdminNo = "//li[text()='No']/input[@name='IsProfileAdmin']";
	public static final String paCaseViewYes = "//li[text()='Yes']/input[@name='IsProfileSameAsViewCasePermission']";
	public static final String paCaseViewNo = "//li[text()='No']/input[@name='IsProfileSameAsViewCasePermission']";
	public static final String paGrpAccessAll = "//li[text()='All']/input[@name='IsProfileAccessAllGroups']";
	public static final String paGrpAccessLmt = "//li[text()='Limited']/input[@name='IsProfileAccessAllGroups']";
	public static final String paGrpAccessViewDetail = "(//label[contains(text(),'Profile Admin')]/ancestor::div[contains(@class,'selectRole')]//a)[1]";
	public static final String paGrpOnlyAccess = "//input[@value='false'][@name='IsExcludedGroupProfileAccess']";
	public static final String paGrpExceptAccess = "//input[@value='true'][@name='IsExcludedGroupProfileAccess']";
	public static final String paSelectGrp = "//div[@id='group-profile-access']//i[contains(@class,'icon--blue')]";
	public static final String paGrpNoChild = "(//div[@id='group-profile-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String paGrpHasChild = "(//div[@id='group-profile-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String paGrpChildren = "(//div[@id='group-profile-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String paGrpSelected = "//div[@id='group-profile-access']//div[contains(@class,'selectedGroupsInRole')]//li";
	public static final String paGrpClearAll = "//div[@id='group-profile-access']//a[contains(text(),'Clear All')]";
	public static final String paGrpSave = "//div[@id='group-profile-access']//input[@name='Save']";
	public static final String paLocAccessAll = "//li[contains(text(),'All')]//input[contains(@name,'IsProfileAccessAllLocations')]";
	public static final String paLocAccessLmt = "//li[contains(text(),'Limited')]//input[@name='IsProfileAccessAllLocations']";
	public static final String paLocAccessViewDetail = "(//label[contains(text(),'Profile Admin')]/ancestor::div[contains(@class,'selectRole')]//a)[2]";
	public static final String paLocOnlyAccess = "//input[@value='false'][@name='IsExcludedLocationProfileAccess']";
	public static final String paLocExceptAccess = "//input[@value='true'][@name='IsExcludedLocationProfileAccess']";
	public static final String paSelectLoc = "//div[@id='location-profile-access']//i[contains(@class,'fa fa-list-ul')]";
	public static final String paLocNoChild = "(//div[@id='location-profile-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String paLocHasChild = "(//div[@id='location-profile-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
//	public static final String paLocChildren = "(//div[@id='location-profile-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String paLocSelected = "//div[@id='location-profile-access']//div[contains(@class,'selectedLocationsInRole')]//li";
	public static final String paLocClearAll = "//div[@id='location-profile-access']//a[contains(text(),'Clear All')]";
	public static final String paLocSave = "//div[@id='location-profile-access']//div[contains(@class,'c-modal__btn')]//input[contains(@name,'Save')]";
	public static final String qvPaGrpPerm = "//span[contains(@class,'ProfileGroupAccess')]";
	public static final String qvPaLocPerm = "//span[contains(@class,'ProfileLocationAccess')]";
	
//	This role can access Insights reporting
	public static final String rptAccessYes = "//li[text()='Yes']/input[@name='IsReportAccess']";
	public static final String rptAccessNo = "//li[text()='No']/input[@name='IsReportAccess']";
	public static final String rptCaseTypeAll = "//li[text()='All']/input[@name='IsReportAccessAllCaseTypes']";
	public static final String rptCaseTypeLmt = "//li[text()='Limited']/input[@name='IsReportAccessAllCaseTypes']";
	public static final String rptCaseTypeLmtViewDetail = "(//label[contains(text(),'Insights reporting')]/ancestor::div[contains(@class,'selectRole')]//a)[1]";
	public static final String rptCaseTypeER = "//input[contains(@name,'IsReportERAccess')]";
	public static final String rptCaseTypeINV = "//input[contains(@name,'IsReportINVAccess')]";
	public static final String rptCaseTypeEI = "//input[contains(@name,'IsReportEIAccess')]";
	public static final String rptCaseTypePH = "//input[contains(@name,'IsReportPHAccess')]";
	public static final String rptCaseTypeSave = "//form[@id='frmdetail']//div[contains(@class,'content')]//a[contains(@class,'btnsavedetail')][contains(text(),'Save')]";
	
	public static final String rptInsightAnltsNo = "//li[text()='No']/input[@name='IsInsightsAnalytics']";
	public static final String rptInsightAnltsYes= "//li[text()='Yes']/input[@name='IsInsightsAnalytics']";
	public static final String rptInsightDBNo = "//li[text()='No']/input[@name='IsInsightsDashboard']";
	public static final String rptInsightDBYes = "//li[text()='Yes']/input[@name='IsInsightsDashboard']";
	public static final String rptInsightBMYes = "//li[text()='Yes']/input[@name='IsBenchMarkingReport']";
	public static final String rptInsightBMNo = "//li[text()='No']/input[@name='IsBenchMarkingReport']";
	public static final String rptInvPrtDtlsYes = "//li[text()='Yes']/input[@name='IsInvolvedPartyReportAccess']";
	public static final String rptInvPrtDtlsNo = "//li[text()='No']/input[@name='IsInvolvedPartyReportAccess']";
	public static final String rptEvntBsdInstYes = "//li[text()='Yes']/input[@name='IsEventAnalyticsEnabled']";
	public static final String rptEvntBsdInstNo = "//li[text()='No']/input[@name='IsEventAnalyticsEnabled']";
	public static final String rptCaseViewAccessYes = "//li[text()='Yes']/input[@name='IsSameAsAdminCasePermission']";
	public static final String rptCaseViewAccessNo = "//li[text()='No']/input[@name='IsSameAsAdminCasePermission']";
	public static final String rptGrpAccessAll = "//li[text()='All']/input[@name='IsReportAccessAllGroups']";
	public static final String rptGrpAccessLmt = "//li[text()='Limited']/input[@name='IsReportAccessAllGroups']";
	public static final String rptGrpAccessViewDet = "(//label[contains(text(),'Insights reporting')]/ancestor::div[contains(@class,'selectRole')]//a)[2]";
	public static final String rptGrpOnlyAccess = "//input[@value='false'][@name='IsExcludedGroupReportAccess']";
	public static final String rptGrpExceptAccess = "//input[@value='true'][@name='IsExcludedGroupReportAccess']";
	public static final String rptSelectGrp = "//div[@id='group-report-access']//i[contains(@class,'icon--blue')]";
	public static final String rptGrpNoChild = "(//div[@id='group-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";
	public static final String rptGrpHasChild = "(//div[@id='group-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String rptGrpChildren = "(//div[@id='group-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String rptGrpSelected = "//div[@id='group-report-access']//div[contains(@class,'selectedGroupsInRole')]//li";
	public static final String rptGrpClearAll = "//div[@id='group-report-access']//a[contains(@class,'c-btn-default jq-clearAll')][contains(text(),'Clear All')]";
	public static final String rptGrpSave = "//div[@id='group-report-access']//input[@name='Save']";
	public static final String rptIssueAccessAll = "//li[text()='All']/input[@name='IsReportAccessAllIssueCategories']";
	public static final String rptIssueAccessLmt = "//li[text()='Limited']/input[@name='IsReportAccessAllIssueCategories']";
	public static final String rptIssueAccessViewDet = "(//label[contains(text(),'Insights reporting')]/ancestor::div[contains(@class,'selectRole')]//a)[3]";
	public static final String rptIssuesOnlyAccess = "//input[@value='false'][@name='IsExcludedIssueReportAccess']";
	public static final String rptIssuesExceptAccess = "//input[@value='true'][@name='IsExcludedIssueReportAccess']";
	public static final String rptSelectIssues = "//div[@id='issue-report-access']//i[contains(@class,'icon--blue')]";
	public static final String rptIssuesHasChild = "(//div[@id='issue-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String rptIssuesChildren = "(//div[@id='issue-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]/following-sibling::div//div[contains(@class,'statictree-data')]";	
	public static final String rptIssuesSelected = "//div[@id='issue-report-access']//div[contains(@class,'selectedIssuesInRole')]//li/a";
	public static final String rptIssuesSave = "//div[@id='issue-report-access']//input[@name='Save']";
	public static final String rptIssuesClearAll = "//div[@id='issue-report-access']//a[@class='c-btn-default jq-clearAll'][contains(text(),'Clear All')]";
	public static final String rptInstRptAdminYes = "//li[text()='Yes']/input[@name='IsReportingAdmin']";
	public static final String rptInstRptAdminNo = "//li[text()='No']/input[@name='IsReportingAdmin']";
	public static final String rptLocAccessAll = "//li[contains(text(),'All')]//input[@name='IsReportAccessAllLocations']";
	public static final String rptLocAccessLmt = "//li[contains(text(),'Limited')]//input[@name='IsReportAccessAllLocations']";
	public static final String rptLocAccessViewDet = "(//label[contains(text(),'Insights reporting')]/ancestor::div[contains(@class,'selectRole')]//a)[4]";
	public static final String rptLocOnlyAccess = "//input[@value='false'][@name='IsExcludedLocationReportAccess']";
	public static final String rptLocExceptAccess = "//input[@value='true'][@name='IsExcludedLocationReportAccess']";
	public static final String rptSelectLoc = "//div[@id='location-report-access']//i[contains(@class,'fa fa-list-ul')]";
	public static final String rptLocHasChild = "(//div[@id='location-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-haschild')]/following-sibling::div)[1]";
	public static final String rptLocNoChild = "(//div[@id='location-report-access']//div[@class='tree-node'][@isenabled='true']/div[contains(@class,'tree-node-nochild')]/following-sibling::div)[1]";	
	public static final String rptLocSelected = "//div[@id='location-report-access']//div[contains(@class,'selectedLocationsInRole')]//li";
	public static final String rptLocSave = "//div[@id='location-report-access']//input[@name='Save']";
	public static final String rptLocClearAll ="//div[@id='location-report-access']//a[@class='c-btn-default jq-clearAll'][contains(text(),'Clear All')]";
	
	public static final String qvRptCaseTypeNonePerms = "//span[contains(@class,'ReportCaseTypeAccess')]";
	public static final String qvRptGrpPerm = "//span[contains(@class,'ReportGroupAccess')]";
	public static final String qvRptOtherPerm = "//span[contains(@class,'ReportOtherAccess')]";
	public static final String qvRptIssueAccessPerm = "//span[contains(@class,'ReportIssueAccess')]";
	public static final String qvRptLocPerm = "//span[contains(@class,'ReportLocationAccess')]";
	public static final String qvOtherPerms = "//strong[contains(text(),'Other Permissions:')]/following-sibling::span";

	
	public static final String isConfigAdminYes = "//li[text()='Yes']/input[@name='IsConfigAdmin']";
	public static final String isConfigAdminNo = "//li[text()='No']/input[@name='IsConfigAdmin']";
	
	public static final String isDataImportAdminYes = "//li[text()='Yes']/input[@name='IsDataImportAdmin']";
	public static final String isDataImportAdminNo = "//li[text()='No']/input[@name='IsDataImportAdmin']";
	
	public static final String isMngrCaseApproverYes = "//li[text()='Yes']/input[@name='IsManagerCaseApprover']";
	public static final String isMngrCaseApproverNo = "//li[text()='No']/input[@name='IsManagerCaseApprover']";
	public static final String isDataShredderNo = "//li[text()='No']/input[@name='IsDataShredderUnChecked']";
	public static final String isDataShredderYes = "//li[text()='Yes']/input[@name='IsDataShredderUnChecked']";
	
	public static final String permissionSave = "//a[contains(@class,'btnrolepermsave')]";
	
	public static final String quickViewPerm = "//a[contains(@class,'quickviewpermissions')]";
	public static final String quickViewAssignedUsers= "//a[contains(@class,'quickviewassignedusers')]";
	
	public static final String managerAccessPerm = "//strong[text()='Manager Access:']/following-sibling::span";
	public static final String groupAccessPerm = "//span[contains(text(),'Group Access:')]/following-sibling::span";

}
