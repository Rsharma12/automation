package com.HRA.or;

public class CommonOR {
	private CommonOR() {
	    throw new IllegalStateException("All Cases Object Repository class");
	}
//https://qa-demo.hracuity.net/
	public static final String caseNumberHeader = "//a[contains(text(),'Case Number')]";
	
	
	public static final String myCases = "//a[@id='tabcase1']";
	public static final String myPriorityCases = "//a[@id='tabcase2']";
	public static final String allCases = "//a[@id='tabcase0']";
	public static final String openCases = "//div[@id='casetab0']"; //div[@id='casetab0']
	public static final String employeeRelations = "//div[@id='casetab2']";
	public static final String investigation = "//div[@id='casetab1']";
	
	public static final String cases = "//a[@data-searchtarget='menu'][text()='Cases']";
	public static final String searchCaseHeader = "//h4[contains(text(),'Search Cases')]";
	public static final String searchCaseType = "//select[@id='CaseType']";
	public static final String search = "//input[@id='btn-search-grid']";
	public static final String selectFromSearch = "//h6[contains(text(),'Search Result')]/ancestor::div/following-sibling::div//table/tbody/tr[2]/td[1]/a";
	public static final String caseLink = "//h4[contains(text(),'MY CASES')]/ancestor::div/following-sibling::div//table/tbody/tr[2]/td[1]/a";
	
	public static final String adminstrationTab = "//a[contains(text(),'Administration')]";
	public static final String roles = "//a[contains(text(),'Roles')]";
	public static final String searchRoleName = "//input[@id='search-RoleName']";
	public static final String roleName = "//input[@id='RoleName']";
	public static final String activeRoleYes = "//label[contains(text(),'Active Role')]/following-sibling::div/input[@type='radio'][@value='true']";
	public static final String activeRoleNo = "//label[contains(text(),'Active Role')]/following-sibling::div/input[@type='radio'][@value='false']";
	public static final String createNewRole = "//a[text()='Create New']";
	public static final String saveNewRole = "//a[contains(@class,'btnadminsave')]";
	public static final String searchRole = "//input[@value='Search']";
	
}
