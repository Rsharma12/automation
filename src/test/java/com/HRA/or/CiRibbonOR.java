package com.HRA.or;

public class CiRibbonOR {
	private CiRibbonOR() {
	    throw new IllegalStateException("All Cases Object Repository class");
	}
	
	public static final String selectDate = "//a[contains(@class,'ui-state-highlight')]";
	
	public static final String chat = "//i[@class='fa fa-comment-o']";
	public static final String chatTextBox = "//div[@class='chatinput-field']//div[@data-text='Type a new message']";
	public static final String reply = "(//div[contains(@class,'chat-replay')]/i)[1]";
	public static final String replyBox = "//div[@class='reply-box']//div[@data-text='Type a new message']";
	
	public static final String caseHealth = "//span[@class='fa fa-thermometer-empty']"; //#take screen shot only
	public static final String caseHealthPercentage = "//h5[contains(text(),'Health Progress')]/span";
	
	public static final String caseLog = "//span[@class='fa fa-clipboard']";
	public static final String calendarIcon = "//table[@id='jq-tbltasktracker']//i[@class='fa fa-calendar']";
	public static final String caseLogCategory = "//select[@id='tasktracker-TaskCategoryList']";
	public static final String hoursSpent = "//input[@id='tasktracker-timespent']";
	public static final String notes = "//textarea[@id='tasktracker-Notes']";
	public static final String saveCaseLog = "//div[@class='c-modal__btn']//input[@class='c-btn c-btn--small jq-btnsave jq-btnvalidate']";
	
	public static final String expense = "//span[@class='fa fa-usd']";
	public static final String expenseCalenderIcon = "//tr[@class='jq-casetools-row']//span[@class='c-form__inputIcon']";
	public static final String expenseCategory = "//select[@id='expensetracker-ExpenseCategoryList']";
	public static final String expenseAmount = "//input[@id='expensetracker-ExpenseAmt']";
	public static final String expenseSpecific = "//input[@id='expensetracker-ExpenseSpecifics']";
	public static final String expenseLetterTmplt = "";
	public static final String expenseSave = "//div[@class='c-hra-tool_main c-hra-tool-modal__content fade in']//input[@class='c-btn c-btn--small jq-btnsave jq-btnvalidate']";
	
	public static final String letterTemplate = "//span[@class='fa fa-file-text-o']";
	public static final String template = "//select[@id='LetterTemplate-TemplateCategories']";
	public static final String involvedParty = "//select[@id='LetterTemplate-CasePartiesList']";
	public static final String generateLetter = "//input[@class='c-btn c-btn--small jq-btnvalidate jq-generatelettertemplate']";
	public static final String sendOption = "//a[contains(text(),'Send Option')]";
	public static final String optionEmail = "//a[contains(text(),'Send as Email')]";
	public static final String sendEmailToOthers = "//label[text()='Send email to Other:']/preceding-sibling::label/input";
	public static final String personEmailId = "//div[contains(@class,'email-send-other')]//input[contains(@class,' personsSearchCallback ')]";
	public static final String sendMail = "//input[@class='c-btn c-btn--small jq-lettertemplate-send-button']";
	public static final String mailOk = "//div[@id='modal-confirm-alert']//a[@id='jq-modalhref-target']";
	public static final String letterSave = "//input[contains(@class,'savelettertemplate')]";
	
	
	public static final String scheduleReminder = "//span[@class='fa fa-calendar-minus-o']";
	public static final String reminderDate = "//input[@id='ReminderDate']";
	public static final String selectReminderDate = "//a[contains(@class,'ui-state-active')]";
	public static final String reminderNotes = "//textarea[@id='Notes']";
	public static final String sendToOthers = "//input[@id='chbSendToOther']";
	public static final String personEmail = "//input[contains(@class,' personsSearch jq-personsdisplay')]";
	public static final String reminderMessage = "//textarea[@id='Message']";
	public static final String reminderSave = "//div[@class='c-modal__btn jq-new-reminder']//input[@class='c-btn c-btn--small jq-btnsave jq-btnvalidate']";
	
	public static final String caseReport = "//span[@class='hraicon hraicon-report']";
	
	public static final String caseFolder = "//span[contains(text(),'Case Folder')]";
	public static final String caseFolderInvolvedParty = "//h4[contains(text(),'Involved Parties')]";
	public static final String involvedPartyOpen= "//h4[text()='Involved Parties']/ancestor::div[1][contains(@class,'is-open')]";
	public static final String involvedPartyClose = "//h4[text()='Involved Parties']/ancestor::div[1][not(contains(@class,'is-open'))]";
	public static final String addInvolvedParty = "//a[@class='linkGreen jq-btnvalidate jq-addanother jq-add-new-details']//strong[contains(text(),'+ Add Involved Party')]";
	public static final String addINVComplainant = "//a[contains(@class,'addnewdetailsCaseFolder')][contains(text(),'Complainant')]";
	public static final String addINVPerson = "//div[@id='party-internal']//input[@id='Person']";
	public static final String saveInvParty = "//input[contains(@class,'small jq-btnsave jq-btnvalidate')]";
	public static final String involvedPartyGearIcon = "(//div[contains(@id,'InvolvedParties')]//span[contains(@class,'hraicon-gear')])[1]";
	public static final String invPrtyEditView = "//ul[contains(@style,'block')]//a[contains(text(),'View / Edit Complainant')]";
	public static final String invPrtyRoleList = "//select[@id='involvedparty-RoleList']";
	public static final String invPrtyContactNum= "//div[@id='party-internal']//textarea[contains(@name,'AdditionalInfo')]";
	public static final String invPrtySave = "//div[@id='jq-invparty-btnsavepanel']//input";
	
	public static final String issuesOpen = "//h4[contains(text(),'Issues')]/ancestor::div[1][contains(@class,'is-open')]";
	public static final String issuesClose = "//h4[contains(text(),'Issues')]/ancestor::div[1][not(contains(@class,'is-open'))]";
	public static final String issueExplain = "//textarea[contains(@name,'BriefExplanation')]";
	public static final String issuesResolved = "//textarea[@name='BriefExplanation']";
	public static final String issuesCateg = "//form[@name='frmaddissue']//i[@class='fa fa-list-ul icon--blue']";
	public static final String saveIssues = "//input[@class='c-btn c-btn--small jq-btnsave jq-btnvalidate']";
	
	
	public static final String issueAllegation = "//h4[contains(text(),'Issues')]";
	public static final String documents = "//h4[contains(text(),'Documents')]";
	public static final String addDocuments = "";
	public static final String selectDoc = "//select[@id='documentmaterial-EvidenceTypeList']";
	public static final String uploadDoc = ""; //#radiobutton
	public static final String selectTypeOfDoc = "";
	public static final String saveCaseFolderDoc = "";
	
	public static final String addPolicy = "";
	public static final String selectPolicy = "//select[@id='relevantpolicy-PolicieGuidelineList']";
	public static final String policySpecificTxt = "";
	public static final String uploadDocLib = "";
	public static final String selectDocLib = "//div[@id='relevantpolicy-modal']//select[@id='DocumentLibrary']";
	public static final String savePolicy = "//div[@id='relevantpolicy-modal']//input[@id='jq-btnsavedocument']";
	
	public static final String additionalInformation = "//i[@class='fa fa-info-circle']";
	
	
//	
}
