package com.HRA.testrunner;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.HRA.enums.Browsers;
import com.HRA.helper.ReportingUtils;
import com.HRA.testBase.TestBase;
import com.relevantcodes.extentreports.ExtentReports;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;


@CucumberOptions(
//					features = "src/test/resources/features/CaseDetails/CiRibbon.feature", 
//					features = "src/test/resources/features/RBAC/ProfileAdminRole.feature",
					features = "src/test/resources/features/RBAC",
//					features = "src/test/resources/features/RBAC/OtherRoles.feature",
					glue = {"com/HRA/stepdef"}, 
					plugin = { "pretty","html:target/cucumber-htmlreport", "json:target/cucumber-report.json", "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" },
//					tags = {"@ReportingInsights"},
					monochrome = true)
public class TestRunner {

	private TestNGCucumberRunner testNGCucumberRunner;

	@BeforeClass(alwaysRun = true)
	public void setUpClass() throws Exception {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
		ReportingUtils.report = new ExtentReports("C:\\ExtentReport" + "\\ExtentReportResults.html");
		TestBase testBase = new TestBase();
		testBase.selectBrowser(Browsers.CHROME.name());
//		testBase.selectBrowser(Browsers.IE.name());
//		MyScreenRecorder.startRecording("AutomationTest");
	}

	@Test(groups = "cucumber", description = "Runs cucumber Features", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) {
		testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	}

	@DataProvider
	public Object[][] features() {
		return testNGCucumberRunner.provideFeatures();
	}

	@AfterClass(alwaysRun = true)
	public void testDownClass() throws Exception {
//		MyScreenRecorder.stopRecording();
		ReportingUtils.report.flush();
		testNGCucumberRunner.finish();
		TestBase.driver.quit();
	}

}
