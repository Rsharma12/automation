package com.HRA.stepdef;

import java.util.List;

import com.HRA.pageObjects.RbacPage;
import com.HRA.testBase.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RbacStepDef extends TestBase{
	RbacPage rbacPage = new RbacPage(driver);

	@When("^user enables Manager Portal User role$")
	public void user_enables_Manager_Portal_User_role() throws Throwable {
	  rbacPage.mpUserRole();
	}

	@When("^user selects \"([^\"]*)\" Group access as \"([^\"]*)\"$")
	public void user_selects_Group_access_as(String roles, String option) throws Throwable {
				rbacPage.selectGrpAccessAll(roles, option);
	}
	@When("^user selects \"([^\"]*)\" Location access as \"([^\"]*)\"$")
	public void user_selects_Location_access_as(String roles, String option) throws Throwable {
				rbacPage.selectLocAccessAll(roles, option);
	}
		
	@When("^user selects \"([^\"]*)\" Issue access as \"([^\"]*)\"$")
	public void user_selects_Issue_access_as(String roles, String option) throws Throwable {
				rbacPage.selectIssueAccessAll(roles, option);
	}
	
	@When("^user click on quick view permissions$")
	public void user_click_on_quick_view_permissions() throws Throwable {
	  rbacPage.quickViewPermission();
	}

	@When("^user click on Edit Permissions$")
	public void user_click_on_Edit_Permissions() throws Throwable {
	  rbacPage.editPermissions();
	}

	@When("^user selects Group access as Limited and edit details for \"([^\"]*)\" role$")
	public void user_selects_Group_access_as_Limited_and_edit_details_for_role(String role) throws Throwable {
		  rbacPage.selectLmtGrpNEdit(role);
		}
	
	@When("^user selects Issue access as Limited and edit details for \"([^\"]*)\" role$")
	public void user_selects_Issue_access_as_Limited_and_edit_details_for_role(String role) throws Throwable {
	   rbacPage.selectLmtIssueNEdit(role);
	}
	
	@When("^user selects \"([^\"]*)\" role can view cases with \"([^\"]*)\"$")
	public void user_selects_role_can_view_cases_with(String role, String option) throws Throwable {
	   rbacPage.selectOption(role, option);
	}

	@When("^user saves role permissions$")
	public void user_saves_role_permissions() throws Throwable {
	   rbacPage.savePermissions();
	}
	
	@When("^user selects Location access as Limited and edit details for \"([^\"]*)\" role$")
	public void user_selects_Location_access_as_Limited_and_edit_details_for_role(String role) throws Throwable {
		  rbacPage.selectLmtLocNEdit(role);
		}

	@Then("^verify \"([^\"]*)\" role has Limited Group Access with \"([^\"]*)\" option$")
	public void verify_role_has_Limited_Group_Access_with_option(String role, String option) throws Throwable {
		rbacPage.verifyGrpAccess(role, option);
	}

	@Then("^verify \"([^\"]*)\" role has Limited Location Access with \"([^\"]*)\" option$")
	public void verify_role_has_Limited_Location_Access_with_option(String role, String option) throws Throwable {
	   rbacPage.verifyLocAccess(role, option);
	}
	
	@Then("^verify \"([^\"]*)\" role has Limited Issue Access with \"([^\"]*)\" option$")
	public void verify_role_has_Limited_Issue_Access_with_option(String role, String option) throws Throwable {
	   rbacPage.verifyIssueAccess(role, option);
	}
	
	@When("^save manager portal \"([^\"]*)\" permissions$")
	public void save_manager_portal_permission(String permission) throws Throwable {
	   rbacPage.saveMPRolePermi(permission);
	}
	
	@When("^user enables Limited User role$")
	public void user_enables_Limited_User_role() throws Throwable {
		rbacPage.enableLmtUser();
	}
	
	@When("^user enables Profile Admin role$")
	public void user_enables_Profile_Admin_role() throws Throwable {
		rbacPage.enableProfileAdmin();
	}

	@When("^save limited user \"([^\"]*)\" permissions$")
	public void save_limited_user_permissions(String option) throws Throwable {
	rbacPage.saveLmtUserpermi(option);
	}
	
	@When("^user select profile admin Location access as \"([^\"]*)\" and edit details$")
	public void user_select_profile_admin_Location_access_as_and_edit_details(String option) throws Throwable {
		rbacPage.profileAdminLocAccess(option);
	}
	
	@When("^user enables configuration admin$")
	public void user_enables_configuration_admin() throws Throwable {
	   rbacPage.enableConfigAdmin();
	}
	
	@When("^user enable data integration admin$")
	public void user_enable_data_integration_admin() throws Throwable {
	   rbacPage.enableDataIntegAdmin();
	}
//role to enter cases
	@When("^user enables the role to enter cases and edit details$")
	public void user_enables_the_role_to_enter_cases_and_edit_details() throws Throwable {
	   rbacPage.enableRoleToEnterCases();
	}

	@When("^user selects options to enter cases for role$")
	public void user_selects_options_to_enter_Cases_for_role(DataTable dt) throws Throwable {
		List<String> optionList = dt.asList(String.class);
	   rbacPage.enterCasesOptions(optionList);
	}
	
	@When("^save case entry permissions$")
	public void save_permissions() throws Throwable {
	   rbacPage.saveCaseEntryPermi();
	}
	
	
	@When("^user click on cancel$")
	public void user_click_on_cancel() throws Throwable {
	   rbacPage.cancelCaseEntryPermi();
	}
	
	@Then("^verify enter cases permissions$")
	public void verify_enter_case_permissions() throws Throwable {
	   rbacPage.verifyEnterCasePerms();
	}

	@When("^user enables view cases$")
	public void user_enables_view_cases() throws Throwable {
	    rbacPage.enableViewCases();
	}

	@When("^user selects option view cases \"([^\"]*)\" and edit details$")
	public void user_selects_option_view_cases_and_edit_details(String accessType) throws Throwable {
	  rbacPage.vcLmtNEditDetails(accessType);
	}

	@When("^user selects Case Type options for View Case$")
	public void user_selects_Case_Type_options_for_View_Case(DataTable dt) throws Throwable {
		List<String> optionList = dt.asList(String.class);
	   rbacPage.caseTypeOptions(optionList);
	}

	@When("^save view case \"([^\"]*)\" permissions$")
	public void save_view_case_permissions(String caseType) throws Throwable {
	    rbacPage.saveVCPermission(caseType);
	}

	
	@Then("^verify Case Type permissions for \"([^\"]*)\" role$")
	public void verify_Case_Type_permissions_for_role(String role) throws Throwable {
	   rbacPage.caseTypePermValidation(role);
	}
	
	@When("^user enables Case Admin role$")
	public void user_enables_Case_Admin_role() throws Throwable {
	   rbacPage.enablesCaseAdminRole();
	}

	@When("^user selects option case admin \"([^\"]*)\" and edit details$")
	public void user_selects_option_case_admin_and_edit_details(String option) throws Throwable {
	   rbacPage.caLmtEditDetails(option);
	}

	@When("^save case admin \"([^\"]*)\" permissions$")
	public void save_case_admin_permissions(String option) throws Throwable {
	   rbacPage.saveCaGrpLocPermi(option);
	}
	
	@When("^user enables Profile admin role$")
	public void user_enables_Profile_admin_role() throws Throwable {
	    rbacPage.enableProfileAdminRole();
	}


	@When("^save profile admin \"([^\"]*)\" permissions$")
	public void save_profile_admin_permissions(String option) throws Throwable {
	   rbacPage.savePAPermi(option);
	}

	@When("^user enables configuration admin roles$")
	public void user_enables_configuration_admin_roles() throws Throwable {
	    rbacPage.enablesCofigAdmin();
	}
	@When("^user enables Data Integration Admin role$")
	public void user_enables_Data_Integration_Admin_role() throws Throwable {
	  rbacPage.enableDataInteAdminRole();
	}
	
	@Then("^verify \"([^\"]*)\" role has \"([^\"]*)\" role enabled$")
	public void verify_role_has_role_enabled(String role, String otherRole) throws Throwable {
	  rbacPage.verifyOtherRole(role, otherRole);
	}

	@Then("^verify \"([^\"]*)\" role has Group Access \"([^\"]*)\"$")
	public void verify_role_has_Group_Access(String role, String option) throws Throwable {
		rbacPage.verifyGrpAccess(role, option);
	}
	
	@Then("^verify \"([^\"]*)\" role has Location Access \"([^\"]*)\"$")
	public void verify_role_has_Location_Access(String role, String option) throws Throwable {
		rbacPage.verifyLocAccess(role, option);
	}
	
	@When("^user enables insights reporting$")
	public void user_enables_insights_reporting() throws Throwable {
	  rbacPage.enableInsightsRpt();
	}
	
	@When("^user selects option insights reporting \"([^\"]*)\" and edit details$")
	public void user_selects_option_insights_reporting_and_edit_details(String accessType) throws Throwable {
	  rbacPage.instRptLmtNEditDetails(accessType);
	}
	@When("^user selects Case Type options for Insights Reporting$")
	public void user_selects_Case_Type_options_for_Insights_Reporting(DataTable dt) throws Throwable {
		List<String> optionList = dt.asList(String.class);
	   rbacPage.rptCaseTypeOptions(optionList);
	}
	
	@When("^save insights reporting \"([^\"]*)\" permissions$")
	public void save_insights_reporting_permissions(String caseType) throws Throwable {
	    rbacPage.saveRptPermission(caseType);
	}
	
	@When("^user enables Insights Analytics$")
	public void user_enables_Insights_Analytics() throws Throwable {
		rbacPage.enableAnalytics();
	}

	@When("^user enables Insights Dashboard$")
	public void user_enables_Insights_Dashboard() throws Throwable {
		rbacPage.enableDashboard();
	}

	@When("^user enables Insights Benchmark$")
	public void user_enables_Insights_Benchmark() throws Throwable {
		rbacPage.enableBenchmark();
	}

	@When("^user enables Insights Reporting Involved Party Details$")
	public void user_enables_Insights_Reporting_Involved_Party_Details() throws Throwable {
		rbacPage.enableINVPartyDetail();
	}

	@When("^user enables Insights Reporting Event Based Insights$")
	public void user_enables_Insights_Reporting_Event_Based_Insights() throws Throwable {
		rbacPage.enableEvntBsdInsights();
	}

	@When("^user enables configuration admin role$")
	public void user_enables_configuration_admin_role() throws Throwable {
		rbacPage.enableOtherConfigAdmin();
	}
	
	@When("^user enables manager case approver$")
	public void user_enables_manager_case_approver() throws Throwable {
		rbacPage.enableMnrgCaseApprover();
	}
	
	@Then("^verify Configuration Admin permission$")
	public void verify_Configuration_Admin_permission() throws Throwable {
		rbacPage.verifyOtherPerms("Configuration Admin");
	}
	
	@Then("^verify Data Import Admin permission$")
	public void verify_Data_Import_Admin_permission() throws Throwable {
		rbacPage.verifyOtherPerms("Data Import Admin");
	}
	
	@Then("^verify Manager Case Approver permission$")
	public void verify_Manager_Case_Approver_permission() throws Throwable {
		rbacPage.verifyOtherPerms("Manager Case Approver");
	}

}
