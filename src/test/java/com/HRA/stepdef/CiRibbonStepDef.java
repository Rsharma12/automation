package com.HRA.stepdef;

import java.util.concurrent.TimeUnit;

import com.HRA.pageObjects.CiRibbonPage;
import com.HRA.testBase.TestBase;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CiRibbonStepDef extends TestBase{
	
	CiRibbonPage ciRibbonPage = new CiRibbonPage(driver);

	@When("^type a \"([^\"]*)\" and press enter to send message$")
	public void type_a_and_press_enter_to_send_message(String message) throws Throwable {
		ciRibbonPage.chatBox(message);
		TimeUnit.SECONDS.sleep(3);
	}

	@When("^close \"([^\"]*)\" popup window$")
	public void close_popup_window(String icon) throws Throwable {
		ciRibbonPage.closePopupWindow(icon);
	}

	@When("^user click on \"([^\"]*)\" icon$")
	public void user_click_on_icon(String icon) throws Throwable {
		ciRibbonPage.clickOnRibbonIcon(icon);
		
	}

	@When("^user get health percentage$")
	public void user_get_health_percentage() throws Throwable {
		ciRibbonPage.getHealthPercentage();
	}

	@When("^fill mandatory information of case log \"([^\"]*)\"$")
	public void fill_mandatory_information_of_case_log(String icon) throws Throwable {
		ciRibbonPage.fillInfoCaseLog(icon);
	}

	
	@When("^fill mandatory information of expenses \"([^\"]*)\"$")
	public void fill_mandatory_information_of_expenses(String expense) throws Throwable {
		ciRibbonPage.fillExpenseInfo(expense);
	}

	@Then("^validate \"([^\"]*)\"$")
	public void validate(String arg1) throws Throwable {
	   
	}

	@When("^fill mandatory information to generate \"([^\"]*)\"$")
	public void fill_mandatory_information_to_generate(String letterTemplate) throws Throwable {
		ciRibbonPage.genrateLetterTmplt(letterTemplate);
	}

	@Then("^validate$")
	public void validate() throws Throwable {
	  
	}

	@When("^fill mandatory info to set reminder$")
	public void fill_mandatory_info_to_set_reminder() throws Throwable {
		ciRibbonPage.setReminderInfo();
	}

	@When("^add involved parties$")
	public void add_involved_parties() throws Throwable {
		ciRibbonPage.addInvParty();
	}

	@When("^add issues$")
	public void add_issues() throws Throwable {

	}

	@When("^add documents$")
	public void add_documents() throws Throwable {
	  
	}

	@Then("^validate additional information is added$")
	public void validate_additional_information_is_added() throws Throwable {
	   
	}
	
	
	
}
