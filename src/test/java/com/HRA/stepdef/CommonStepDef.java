package com.HRA.stepdef;

import java.io.FileInputStream;
import java.util.Properties;
import com.HRA.helper.HighlightElementManager;
import com.HRA.helper.ReportingUtils;
import com.HRA.helper.StringEncrypt;
import com.HRA.pageObjects.CommonPage;
import com.HRA.pageObjects.LoginPage;
import com.HRA.testBase.TestBase;
import com.relevantcodes.extentreports.LogStatus;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CommonStepDef extends TestBase{
	
	CommonPage commonPage = new CommonPage(driver);
	
		@Given("^User is in HRAcuity demo$")
		public void user_is_in_HRAcuity_demo() throws Throwable {
			String key = "qa";
			String path = "src/test/resources/properties/qa/LoginPage.properties";
//			String path = "src/test/resources/properties/UAT/LoginPage.properties";
//			String path = "src/test/resources/properties/Prod/LoginPage.properties";
			Properties prop = new Properties();
			FileInputStream  input = new FileInputStream(path);
			prop.load(input);
			String url = prop.getProperty("url");
			String userName = prop.getProperty("userName");		
			String password = StringEncrypt.decryptXOR(prop.getProperty("password"),key);
			
			driver.get(url);
			
//			TimeUnit.SECONDS.sleep(5);

			LoginPage pf = new LoginPage(driver);
			pf.email().clear();
			pf.email().sendKeys(userName);

			pf.password().clear();
			pf.password().sendKeys(password);
			pf.login().click();
			pf.iAgree().click();
			Thread.sleep(2000);
			
			System.out.println(pf.name().getText());
			HighlightElementManager.highlightElement(driver, pf.name());
			ReportingUtils.log(driver, LogStatus.PASS, "Login test passed, logged by " + pf.name().getText(), true);
			HighlightElementManager.unHighlightElement(driver, pf.name());

		}

		
		@When("^user click on \"([^\"]*)\" tab$")
		public void user_click_on_case_tab(String caseType) throws Throwable {
			commonPage.userClickOnTab(caseType);
		}
	
		@When("^select \"([^\"]*)\" case to open case detail page$")
		public void select_case_to_open_case_detail_page(String caseType) throws Throwable {
			commonPage.selectCase(caseType);
		}
		
		@When("^user go to administration tab and select roles$")
		public void user_go_to_administration_tab_and_select_roles() throws Throwable {
			commonPage.adminTab();
		}

		@When("^user creates an active new role$")
		public void user_creates_an_active_new_role() throws Throwable {
			commonPage.createActiveNewRole();
		}
		@When("^user search for the role$")
		public void user_search_for_the_role() throws Throwable {
			commonPage.searchRole();
		}
		
		@When("^user click on case category \"([^\"]*)\" tab$")
		public void user_click_on_case_category_tab(String casesCategory) throws Throwable {
			commonPage.userClickOnCaseCategoryTab(casesCategory);
		}

}
