package com.HRA.stepdef;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.HRA.pageObjects.CaseInsightsPage;
import com.HRA.testBase.TestBase;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class CaseInsightsStepDef extends TestBase{
	
	CaseInsightsPage caseInsightsPage = new CaseInsightsPage(driver);
	
	@When("^user select case type \"([^\"]*)\" from case insights section$")
	public void user_select_case_type_fron_case_insights_section(String caseType) throws Throwable {
		caseInsightsPage.caseTypeNTimeFrame(caseType);
		}

	@Then("^verify selected case type \"([^\"]*)\" on the case insights bar$")
	public void verify_selected_case_type_on_the_case_insights_bar(String caseType) throws Throwable {
		caseInsightsPage.verifyCaseType(caseType);
		}

	@When("^user selects Time Frame \"([^\"]*)\"$")
	public void user_selects_Time_Frame(String rollingMonths) throws Throwable {
		caseInsightsPage.timeFrameSelection(rollingMonths);
		TimeUnit.SECONDS.sleep(2);
	}

	@Then("^verify rolling month \"([^\"]*)\" is visible on the case insights bar$")
	public void verify_rolling_month_is_visible_on_the_case_insights_bar(String rollingMonths) throws Throwable {
		caseInsightsPage.verifyRollingMonths(rollingMonths);
	}

	@When("^user click on \"([^\"]*)\" and get the count of cases$")
	public void user_click_on_and_get_the_count_of_cases(String tiles) throws Throwable {
		caseInsightsPage.clickOnTiles(tiles);
		caseInsightsPage.getCountOfCases(tiles);
	}

	@Then("^verify \"([^\"]*)\" is selected as a tag$")
	public void verify_is_selected_as_a_tag(String tiles) throws Throwable {
		caseInsightsPage.verifySelectedTags(tiles);
	}

	@When("^user click on apply in Selected Tags section$")
	public void user_click_on_apply_in_Selected_Tags_section() throws InterruptedException, IOException{
		caseInsightsPage.applyTagFilter();
	}
	
	@Then("^verify output in filtered results section$")
	public void verify_output_in_filtered_results_section() throws IOException, InterruptedException{
		caseInsightsPage.verifyFilteredResult();
	}
	
	@When("^user removes \"([^\"]*)\" from selected tag$")
	public void user_removes_from_selected_tag(String tiles) throws Throwable{
		caseInsightsPage.removeTag(tiles);
	}
	

	@When("^user get the total count of cases from \"([^\"]*)\" tiles$")
	public void user_get_the_total_count_of_cases_from_tiles(String tiles) throws Throwable{
		if(tiles.equalsIgnoreCase("Involved Party") || tiles.equalsIgnoreCase("Action") || tiles.equalsIgnoreCase("Notification Method")){
		caseInsightsPage.scrollToTiles(tiles);
		}
		caseInsightsPage.getCountOfCases(tiles);
		TimeUnit.SECONDS.sleep(2);
	}

}