package com.HRA.testBase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.HRA.enums.Browsers;
import com.HRA.enums.OS;
import com.HRA.helper.ReportingUtils;
import com.relevantcodes.extentreports.LogStatus;

public class TestBase extends VariableBase {

	public static WebDriver driver;

	public WebDriver selectBrowser(String browser) {
		if (System.getProperty("os.name").toLowerCase().contains(OS.WINDOW.name().toLowerCase())) {
			if (browser.equalsIgnoreCase(Browsers.CHROME.name())) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/src/test/resources/drivers/chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().window().maximize();
			} else if (browser.equalsIgnoreCase(Browsers.IE.name())) {
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "/src/test/resources/drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
		} else if (System.getProperty("os.name").toLowerCase().contains(OS.MAC.name().toLowerCase())) {
			if (browser.equalsIgnoreCase(Browsers.CHROME.name())) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/src/test/resources/drivers/chromedriver");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase(Browsers.IE.name())) {
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "/src/test/resources/drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
		}
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		return driver;
	}
	
	public static void click(WebElement we) throws IOException{
		try{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(we));
			Actions action = new Actions(driver);
			action.moveToElement(we).click().build().perform();
			TimeUnit.SECONDS.sleep(1);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Failed to click on Web element - " + e.getMessage(), false);
			Assert.fail("Failed to click on Web element - " + e.getMessage());
		}
		
	}
	
	public static void input(WebElement we, CharSequence... keysToSend) throws IOException{
		try{
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOf(we));
			we.sendKeys(keysToSend);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Failed to input in Web element - " + e.getMessage(), false);
			Assert.fail("Failed to input in Web element - " + e.getMessage());
		}
		
		
	}
	
	public static void scrollToElement(WebElement we) throws IOException{
		try{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);", we);
		}catch(Exception e){
			ReportingUtils.log(driver, LogStatus.FAIL, "Failed to scroll to Web element - " + e.getMessage(), false);
			Assert.fail("Failed to scroll to Web element - " + e.getMessage());
		}
		
	}

}
