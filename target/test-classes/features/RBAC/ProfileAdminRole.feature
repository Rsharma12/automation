Feature: Roles and permissions for Profile Admin
@ProfileAdmin
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	
@ProfileAdmin
Scenario: Role is a profile admin
When user go to administration tab and select roles
And user creates an active new role
And user enables Profile admin role
And user selects "Profile Admin" Group access as "All"
And user selects "Profile Admin" Location access as "All"
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Group Access "All"
And verify "Profile Admin" role has Location Access "All"
#
When user click on Edit Permissions
And user selects Group access as Limited and edit details for "Profile Admin" role
When user selects "Profile Admin" role can view cases with "only Group selected"
And save profile admin "Group access" permissions
When user selects Location access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "only Location selected"
And save profile admin "Location access" permissions
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Limited Group Access with "only Group selected" option
And verify "Profile Admin" role has Limited Location Access with "only Location selected" option
 #
When user click on Edit Permissions
And user selects Group access as Limited and edit details for "Profile Admin" role
When user selects "Profile Admin" role can view cases with "All Group Except selected"
And save profile admin "Group access" permissions
When user selects Location access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "All Location Except selected"
And save profile admin "Location access" permissions
When user enables configuration admin roles
And user enables Data Integration Admin role
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Limited Group Access with "All Group Except selected" option
And verify "Profile Admin" role has Limited Location Access with "All Location Except selected" option
And user click on cancel

#@ProfileAdmin
Scenario: Create Profile Admin User role with Group as ALL and Location access as Limited
When user go to administration tab and select roles
And user creates an active new role
And user enables Profile admin role
And user selects "Profile Admin" Group access as "All"
When user selects Location access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "only Location selected"
And save profile admin "Location access" permissions
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Group Access "All"
And verify "Profile Admin" role has Limited Location Access with "only Location selected" option

