
@tag
Feature: Roles and permission for Limited user

@LmtUserRole
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo

@LmtUserRole
Scenario: Create Limited User role with All permissions for Grp and location, Limited permission for Only grp and location, Limited permission for except grp and location
When user go to administration tab and select roles
And user creates an active new role
And user enables Limited User role
And user enables Profile Admin role
And user selects "Profile Admin" Group access as "All"
And user selects "Profile Admin" Location access as "All"
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Group Access "All"
And verify "Profile Admin" role has Location Access "All"
#
When user click on Edit Permissions
And user selects Group access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "only Group selected"
And save limited user "group" permissions
When user selects Location access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "only Location selected"
And save limited user "location" permissions
And user enables configuration admin
And user enable data integration admin
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Limited Group Access with "only Group selected" option
And verify "Profile Admin" role has Limited Location Access with "only Location selected" option 
And verify "limited user" role has "Configuration Admin" role enabled
And verify "limited user" role has "Data Import Admin" role enabled

When user click on Edit Permissions
And user selects Group access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "All Group Except selected"
And save limited user "group" permissions
When user selects Location access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "All Location Except selected"
And save limited user "location" permissions
And user enables configuration admin
And user enable data integration admin
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions 
Then verify "Profile Admin" role has Limited Group Access with "All Group Except selected" option 
And verify "Profile Admin" role has Limited Location Access with "All Location Except selected" option
And verify "limited user" role has "Configuration Admin" role enabled
And verify "limited user" role has "Data Import Admin" role enabled
And user click on cancel

#@LmtUserRole
Scenario: Create Limited User role with Group as All, Location as Limited permissions and Enables Configuration role
#When user click on Edit Permissions
When user go to administration tab and select roles
And user creates an active new role
And user enables Limited User role
And user enables Profile Admin role
And user selects "Profile Admin" Group access as "All"
When user selects Location access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "All Location Except selected"
And save limited user "location" permissions
And user enables configuration admin
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Group Access "All"
And verify "Profile Admin" role has Limited Location Access with "All Location Except selected" option
And verify "limited user" role has "Configuration Admin" role enabled
And user click on cancel

#@LmtUserRole
Scenario: Create Limited User role with Group as Limited, Location as All permissions and Enables Configuration role
#When user click on Edit Permissions
When user go to administration tab and select roles
And user creates an active new role
And user enables Limited User role
And user enables Profile Admin role
And user selects Group access as Limited and edit details for "Profile Admin" role
And user selects "Profile Admin" role can view cases with "All Group Except selected"
And save limited user "group" permissions
And user selects "Profile Admin" Location access as "All"
And user enable data integration admin
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Profile Admin" role has Limited Group Access with "All Group Except selected" option
And verify "Profile Admin" role has Location Access "All"
And verify "limited user" role has "Data Import Admin" role enabled
And user click on cancel

