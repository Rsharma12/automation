Feature: Roles and permission for Case Admin
@CaseAdmin
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	
@CaseAdmin
Scenario: Create Case Admin User role with Group and Location access
When user go to administration tab and select roles
And user creates an active new role
When user enables Case Admin role
And user selects "Case Admin" Group access as "All"
And user selects "Case Admin" Location access as "All"
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Case Admin" role has Group Access "All"
And verify "Case Admin" role has Location Access "All"
#
When user click on Edit Permissions
And user selects Group access as Limited and edit details for "Case Admin" role
And user selects Group access as Limited and edit details for "Case Admin" role
When user selects "Case Admin" role can view cases with "only Group selected"
And save case admin "Group access" permissions
When user selects Location access as Limited and edit details for "Case Admin" role
And user selects "Case Admin" role can view cases with "only Location selected"
And save case admin "Location access" permissions
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Case Admin" role has Limited Group Access with "only Group selected" option
And verify "Case Admin" role has Limited Location Access with "only Location selected" option
 #
When user click on Edit Permissions
And user selects Group access as Limited and edit details for "Case Admin" role
When user selects "Case Admin" role can view cases with "All Group Except selected"
And save case admin "Group access" permissions
When user selects Location access as Limited and edit details for "Case Admin" role
And user selects "Case Admin" role can view cases with "All Location Except selected"
And save case admin "Location access" permissions
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Case Admin" role has Limited Group Access with "All Group Except selected" option
And verify "Case Admin" role has Limited Location Access with "All Location Except selected" option
And user click on cancel
 
@CaseAdmin
Scenario: Create Case Admin User role with Group as ALL and Location access as Limited
When user go to administration tab and select roles
And user creates an active new role
When user enables Case Admin role
And user selects "Case Admin" Group access as "All" 
When user selects Location access as Limited and edit details for "Case Admin" role
And user selects "Case Admin" role can view cases with "only Location selected"
And save case admin "Location access" permissions
And user saves role permissions
And user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify "Case Admin" role has Group Access "All"
And verify "Case Admin" role has Limited Location Access with "only Location selected" option

