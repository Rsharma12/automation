
@tag
Feature: Roles and permission to enter cases
@EnterCases
Scenario: Login into HR Acuity site
	Given User is in HRAcuity demo
	
@EnterCases
Scenario: Create role and permission to enter cases for all cases
When user go to administration tab and select roles
And user creates an active new role
And user enables the role to enter cases and edit details
And user selects options to enter cases for role
|ER with Search|INV Investigation Team|EI|PH|
And save case entry permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify enter cases permissions
And user click on cancel

@EnterCases	
Scenario: Create role and permission to enter cases for ER and INV cases
When user go to administration tab and select roles
And user creates an active new role
And user enables the role to enter cases and edit details
And user selects options to enter cases for role
|ER with Search|INV Intake Only|
And save case entry permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify enter cases permissions
And user click on cancel

@EnterCases	
Scenario: Create role and permission to enter cases for ER and PH cases
When user go to administration tab and select roles
And user creates an active new role
And user enables the role to enter cases and edit details
And user selects options to enter cases for role
|ER with Search|PH|
And save case entry permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify enter cases permissions
And user click on cancel

@EnterCases	
Scenario: Create role and permission to enter cases for INV EI and PH cases
When user go to administration tab and select roles
And user creates an active new role
And user enables the role to enter cases and edit details
And user selects options to enter cases for role
|INV Legal Advisor|EI|PH|
And save case entry permissions
And user saves role permissions
When user go to administration tab and select roles
And user search for the role
And user click on quick view permissions
Then verify enter cases permissions
And user click on cancel


#And verify "enter case" role has "ER with Search" permission
#And verify "enter case" role has "ER without Search" permission
#And verify "enter case" role has "<investigation_option>" permission
#And verify "enter case" role has "EI" permission
#And verify "enter case" role has "PH" permission
