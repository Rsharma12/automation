$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("CaseAdminRoles.feature");
formatter.feature({
  "line": 1,
  "name": "Roles and permission for Case Admin",
  "description": "",
  "id": "roles-and-permission-for-case-admin",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2234888,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "roles-and-permission-for-case-admin;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 2,
      "name": "@CaseAdmin"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 14393676373,
  "status": "passed"
});
formatter.after({
  "duration": 2635109,
  "status": "passed"
});
formatter.before({
  "duration": 137521,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Create Case Admin User role with Group and Location access",
  "description": "",
  "id": "roles-and-permission-for-case-admin;create-case-admin-user-role-with-group-and-location-access",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@CaseAdmin"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user enables Case Admin role",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "user selects \"Case Admin\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user selects \"Case Admin\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "verify \"Case Admin\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "verify \"Case Admin\" role has Location Access \"All\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 19,
      "value": "#"
    }
  ],
  "line": 20,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "user selects Group access as Limited and edit details for \"Case Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user selects Group access as Limited and edit details for \"Case Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user selects \"Case Admin\" role can view cases with \"only Group selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "save case admin \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "user selects Location access as Limited and edit details for \"Case Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "user selects \"Case Admin\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "save case admin \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "verify \"Case Admin\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "verify \"Case Admin\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 34,
      "value": "#"
    }
  ],
  "line": 35,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "user selects Group access as Limited and edit details for \"Case Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "user selects \"Case Admin\" role can view cases with \"All Group Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "save case admin \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "user selects Location access as Limited and edit details for \"Case Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "user selects \"Case Admin\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "save case admin \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "verify \"Case Admin\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "verify \"Case Admin\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10861429878,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7340799299,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Case_Admin_role()"
});
formatter.result({
  "duration": 1141677386,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 43
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1159508537,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1135323576,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3587304861,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10808862047,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6593506694,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5559551414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 43
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4472414751,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.verify_role_has_Location_Access(String,String)"
});
formatter.result({
  "duration": 4460409543,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3510121173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8372474202,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8347010299,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10914999139,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 17
    }
  ],
  "location": "RbacStepDef.save_case_admin_permissions(String)"
});
formatter.result({
  "duration": 3486932356,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8286105892,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10878747969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 17
    }
  ],
  "location": "RbacStepDef.save_case_admin_permissions(String)"
});
formatter.result({
  "duration": 5904051784,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3646269520,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10701032393,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6601544252,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5588318637,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4466534505,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4466929437,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3515610719,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8383037911,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6990939499,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 17
    }
  ],
  "location": "RbacStepDef.save_case_admin_permissions(String)"
});
formatter.result({
  "duration": 3490128832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8353783372,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6926198264,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 17
    }
  ],
  "location": "RbacStepDef.save_case_admin_permissions(String)"
});
formatter.result({
  "duration": 5786419722,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3534651347,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10685400517,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6530216831,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5570292137,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4481688938,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4439410834,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3484342030,
  "status": "passed"
});
formatter.after({
  "duration": 545147,
  "status": "passed"
});
formatter.before({
  "duration": 132231,
  "status": "passed"
});
formatter.scenario({
  "line": 51,
  "name": "Create Case Admin User role with Group as ALL and Location access as Limited",
  "description": "",
  "id": "roles-and-permission-for-case-admin;create-case-admin-user-role-with-group-as-all-and-location-access-as-limited",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 50,
      "name": "@CaseAdmin"
    }
  ]
});
formatter.step({
  "line": 52,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "user enables Case Admin role",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "user selects \"Case Admin\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "user selects Location access as Limited and edit details for \"Case Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 57,
  "name": "user selects \"Case Admin\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "save case admin \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "verify \"Case Admin\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 64,
  "name": "verify \"Case Admin\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10714941381,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7402790472,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Case_Admin_role()"
});
formatter.result({
  "duration": 1155091652,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 43
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1119721672,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8341831057,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10789365839,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 17
    }
  ],
  "location": "RbacStepDef.save_case_admin_permissions(String)"
});
formatter.result({
  "duration": 5838136686,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3605838073,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10681921594,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6563232033,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5620759072,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 43
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4440752542,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Admin",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4445116533,
  "status": "passed"
});
formatter.after({
  "duration": 133290,
  "status": "passed"
});
formatter.uri("EnterCasesRole.feature");
formatter.feature({
  "line": 3,
  "name": "Roles and permission to enter cases",
  "description": "",
  "id": "roles-and-permission-to-enter-cases",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@tag"
    }
  ]
});
formatter.before({
  "duration": 312419,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "roles-and-permission-to-enter-cases;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@EnterCases"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 10166510457,
  "status": "passed"
});
formatter.after({
  "duration": 232727,
  "status": "passed"
});
formatter.before({
  "duration": 245421,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Create role and permission to enter cases for all cases",
  "description": "",
  "id": "roles-and-permission-to-enter-cases;create-role-and-permission-to-enter-cases-for-all-cases",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@EnterCases"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user enables the role to enter cases and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user selects options to enter cases for role",
  "rows": [
    {
      "cells": [
        "ER with Search",
        "INV Investigation Team",
        "EI",
        "PH"
      ],
      "line": 14
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "save case entry permissions",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "verify enter cases permissions",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10714978053,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7359208036,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_the_role_to_enter_cases_and_edit_details()"
});
formatter.result({
  "duration": 6446187079,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_options_to_enter_Cases_for_role(DataTable)"
});
formatter.result({
  "duration": 6803873924,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.save_permissions()"
});
formatter.result({
  "duration": 5544380057,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3594275400,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10789896881,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6584505433,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5537007534,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_enter_case_permissions()"
});
formatter.result({
  "duration": 4457290996,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3498345167,
  "status": "passed"
});
formatter.after({
  "duration": 221091,
  "status": "passed"
});
formatter.before({
  "duration": 109311,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Create role and permission to enter cases for ER and INV cases",
  "description": "",
  "id": "roles-and-permission-to-enter-cases;create-role-and-permission-to-enter-cases-for-er-and-inv-cases",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 23,
      "name": "@EnterCases"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "user enables the role to enter cases and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "user selects options to enter cases for role",
  "rows": [
    {
      "cells": [
        "ER with Search",
        "INV Intake Only"
      ],
      "line": 29
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "save case entry permissions",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "verify enter cases permissions",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10679341847,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7341471387,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_the_role_to_enter_cases_and_edit_details()"
});
formatter.result({
  "duration": 6376123086,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_options_to_enter_Cases_for_role(DataTable)"
});
formatter.result({
  "duration": 4590652610,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.save_permissions()"
});
formatter.result({
  "duration": 5542082049,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3511401527,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10755844278,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6588605667,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5546208024,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_enter_case_permissions()"
});
formatter.result({
  "duration": 4452240812,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3485567728,
  "status": "passed"
});
formatter.after({
  "duration": 320529,
  "status": "passed"
});
formatter.before({
  "duration": 143162,
  "status": "passed"
});
formatter.scenario({
  "line": 39,
  "name": "Create role and permission to enter cases for ER and PH cases",
  "description": "",
  "id": "roles-and-permission-to-enter-cases;create-role-and-permission-to-enter-cases-for-er-and-ph-cases",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 38,
      "name": "@EnterCases"
    }
  ]
});
formatter.step({
  "line": 40,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user enables the role to enter cases and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user selects options to enter cases for role",
  "rows": [
    {
      "cells": [
        "ER with Search",
        "PH"
      ],
      "line": 44
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "save case entry permissions",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "verify enter cases permissions",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10707206016,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7438385069,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_the_role_to_enter_cases_and_edit_details()"
});
formatter.result({
  "duration": 6335262503,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_options_to_enter_Cases_for_role(DataTable)"
});
formatter.result({
  "duration": 3406063819,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.save_permissions()"
});
formatter.result({
  "duration": 5517392141,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3522968783,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10688101213,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6533937649,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5586883837,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_enter_case_permissions()"
});
formatter.result({
  "duration": 4413194091,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3489592501,
  "status": "passed"
});
formatter.after({
  "duration": 290556,
  "status": "passed"
});
formatter.before({
  "duration": 106138,
  "status": "passed"
});
formatter.scenario({
  "line": 54,
  "name": "Create role and permission to enter cases for INV EI and PH cases",
  "description": "",
  "id": "roles-and-permission-to-enter-cases;create-role-and-permission-to-enter-cases-for-inv-ei-and-ph-cases",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 53,
      "name": "@EnterCases"
    }
  ]
});
formatter.step({
  "line": 55,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "user enables the role to enter cases and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "user selects options to enter cases for role",
  "rows": [
    {
      "cells": [
        "INV Legal Advisor",
        "EI",
        "PH"
      ],
      "line": 59
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "save case entry permissions",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "verify enter cases permissions",
  "keyword": "Then "
});
formatter.step({
  "line": 66,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10699555279,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7348666190,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_the_role_to_enter_cases_and_edit_details()"
});
formatter.result({
  "duration": 6337576377,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_options_to_enter_Cases_for_role(DataTable)"
});
formatter.result({
  "duration": 4547358614,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.save_permissions()"
});
formatter.result({
  "duration": 5520933477,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3616276954,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10726220197,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6577539126,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5582581553,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_enter_case_permissions()"
});
formatter.result({
  "duration": 4444289293,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3480544343,
  "status": "passed"
});
formatter.after({
  "duration": 154094,
  "status": "passed"
});
formatter.uri("InsightsReportingRoles.feature");
formatter.feature({
  "line": 2,
  "name": "Verifing the role can access insights reporting",
  "description": "",
  "id": "verifing-the-role-can-access-insights-reporting",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    }
  ]
});
formatter.before({
  "duration": 110721,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "verifing-the-role-can-access-insights-reporting;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@ReportingInsights"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 9534190828,
  "status": "passed"
});
formatter.after({
  "duration": 83217,
  "status": "passed"
});
formatter.before({
  "duration": 102965,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Create role who can access insights reporting",
  "description": "",
  "id": "verifing-the-role-can-access-insights-reporting;create-role-who-can-access-insights-reporting",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@ReportingInsights"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user enables insights reporting",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "user selects option insights reporting \"Case type Limited\" and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user selects Case Type options for Insights Reporting",
  "rows": [
    {
      "cells": [
        "ER",
        "PH",
        "INV"
      ],
      "line": 15
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "save insights reporting \"Case Type\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user selects \"Insights Reporting\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "user selects \"Insights Reporting\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user selects \"Insights Reporting\" Issue access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user enables Insights Analytics",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "user enables Insights Dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user enables Insights Benchmark",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user enables Insights Reporting Involved Party Details",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "user enables Insights Reporting Event Based Insights",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "verify Case Type permissions for \"Insights Reporting\" role",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "verify \"Insights Reporting\" role has Limited Group Access with \"All\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "verify \"Insights Reporting\" role has Limited Issue Access with \"All\" option",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "verify \"Insights Reporting\" role has Limited Location Access with \"All\" option",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "user selects option insights reporting \"Case type Limited\" and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "user selects Case Type options for Insights Reporting",
  "rows": [
    {
      "cells": [
        "ER",
        "PH"
      ],
      "line": 37
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "save insights reporting \"Case Type\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "user selects Group access as Limited and edit details for \"Insights Reporting\" role",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "user selects \"Insights Reporting\" role can view cases with \"only Group selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "save insights reporting \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user selects Issue access as Limited and edit details for \"Insights Reporting\" role",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user selects \"Insights Reporting\" role can view cases with \"only Issue selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "save insights reporting \"Issue access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "user selects Location access as Limited and edit details for \"Insights Reporting\" role",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "user selects \"Insights Reporting\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "save insights reporting \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "verify Case Type permissions for \"Insights Reporting\" role",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "verify \"Insights Reporting\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 54,
  "name": "verify \"Insights Reporting\" role has Limited Issue Access with \"only Issue selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "verify \"Insights Reporting\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "user selects option insights reporting \"Case type Limited\" and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "user selects Case Type options for Insights Reporting",
  "rows": [
    {
      "cells": [
        "ER",
        "INV"
      ],
      "line": 60
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "save insights reporting \"Case Type\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "user selects Group access as Limited and edit details for \"Insights Reporting\" role",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "user selects \"Insights Reporting\" role can view cases with \"All Group Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "save insights reporting \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "user selects Issue access as Limited and edit details for \"Insights Reporting\" role",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "user selects \"Insights Reporting\" role can view cases with \"All Issues Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "save insights reporting \"Issue access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "user selects Location access as Limited and edit details for \"Insights Reporting\" role",
  "keyword": "When "
});
formatter.step({
  "line": 69,
  "name": "user selects \"Insights Reporting\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "save insights reporting \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": "verify Case Type permissions for \"Insights Reporting\" role",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "verify \"Insights Reporting\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 77,
  "name": "verify \"Insights Reporting\" role has Limited Issue Access with \"All Issues Except selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 78,
  "name": "verify \"Insights Reporting\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10708972981,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7389069076,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_insights_reporting()"
});
formatter.result({
  "duration": 1274543925,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case type Limited",
      "offset": 40
    }
  ],
  "location": "RbacStepDef.user_selects_option_insights_reporting_and_edit_details(String)"
});
formatter.result({
  "duration": 6406779631,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_Case_Type_options_for_Insights_Reporting(DataTable)"
});
formatter.result({
  "duration": 3462199782,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Type",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3542070414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 51
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1116778023,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 54
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1132471254,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 51
    }
  ],
  "location": "RbacStepDef.user_selects_Issue_access_as(String,String)"
});
formatter.result({
  "duration": 1129402426,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Insights_Analytics()"
});
formatter.result({
  "duration": 1123510897,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Insights_Dashboard()"
});
formatter.result({
  "duration": 1086581996,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Insights_Benchmark()"
});
formatter.result({
  "duration": 1099557959,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Insights_Reporting_Involved_Party_Details()"
});
formatter.result({
  "duration": 1106398028,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Insights_Reporting_Event_Based_Insights()"
});
formatter.result({
  "duration": 1150901149,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3924210913,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10666274203,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6520028661,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5966068346,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 34
    }
  ],
  "location": "RbacStepDef.verify_Case_Type_permissions_for_role(String)"
});
formatter.result({
  "duration": 411820648,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 64
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4439451031,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 64
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Issue_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4462798173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 67
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4457874931,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3421462614,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case type Limited",
      "offset": 40
    }
  ],
  "location": "RbacStepDef.user_selects_option_insights_reporting_and_edit_details(String)"
});
formatter.result({
  "duration": 6443364730,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_Case_Type_options_for_Insights_Reporting(DataTable)"
});
formatter.result({
  "duration": 5645959923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Type",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3532765901,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8323739323,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10849591105,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3553640138,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Issue_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 7357820486,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "only Issue selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 8509537945,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Issue access",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3508579178,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8772245918,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10852605982,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 5825034839,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3523530151,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10678833373,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6582917597,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5582909135,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 34
    }
  ],
  "location": "RbacStepDef.verify_Case_Type_permissions_for_role(String)"
});
formatter.result({
  "duration": 861048345,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 64
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4403032720,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "only Issue selected",
      "offset": 64
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Issue_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4433280934,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 67
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4465440334,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3514526421,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case type Limited",
      "offset": 40
    }
  ],
  "location": "RbacStepDef.user_selects_option_insights_reporting_and_edit_details(String)"
});
formatter.result({
  "duration": 6481898040,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_Case_Type_options_for_Insights_Reporting(DataTable)"
});
formatter.result({
  "duration": 4521825598,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Type",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3557364130,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8351229718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6904553205,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3471536029,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Issue_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 7689053279,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "All Issues Except selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 4691824146,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Issue access",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 3490671158,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8393629122,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6921576862,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 25
    }
  ],
  "location": "RbacStepDef.save_insights_reporting_permissions(String)"
});
formatter.result({
  "duration": 5800514892,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3638971401,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10761339113,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6540314732,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5571683211,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 34
    }
  ],
  "location": "RbacStepDef.verify_Case_Type_permissions_for_role(String)"
});
formatter.result({
  "duration": 900219539,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 64
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4406860733,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "All Issues Except selected",
      "offset": 64
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Issue_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4495974874,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insights Reporting",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 67
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4464150108,
  "status": "passed"
});
formatter.after({
  "duration": 218623,
  "status": "passed"
});
formatter.uri("LmtUserRole.feature");
formatter.feature({
  "line": 3,
  "name": "Roles and permission for Limited user",
  "description": "",
  "id": "roles-and-permission-for-limited-user",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@tag"
    }
  ]
});
formatter.before({
  "duration": 97323,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "roles-and-permission-for-limited-user;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@LmtUserRole"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 10299717624,
  "status": "passed"
});
formatter.after({
  "duration": 59592,
  "status": "passed"
});
formatter.before({
  "duration": 52892,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Create Limited User role with All permissions for Grp and location, Limited permission for Only grp and location, Limited permission for except grp and location",
  "description": "",
  "id": "roles-and-permission-for-limited-user;create-limited-user-role-with-all-permissions-for-grp-and-location,-limited-permission-for-only-grp-and-location,-limited-permission-for-except-grp-and-location",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@LmtUserRole"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user enables Limited User role",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user enables Profile Admin role",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user selects \"Profile Admin\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user selects \"Profile Admin\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "verify \"Profile Admin\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "verify \"Profile Admin\" role has Location Access \"All\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 23,
      "value": "#"
    }
  ],
  "line": 24,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "user selects Group access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "user selects \"Profile Admin\" role can view cases with \"only Group selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "save limited user \"group\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "user selects Location access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "user selects \"Profile Admin\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "save limited user \"location\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "user enables configuration admin",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "user enable data integration admin",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "verify \"Profile Admin\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "verify \"Profile Admin\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "verify \"limited user\" role has \"Configuration Admin\" role enabled",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "verify \"limited user\" role has \"Data Import Admin\" role enabled",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "user selects Group access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "user selects \"Profile Admin\" role can view cases with \"All Group Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "save limited user \"group\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "user selects Location access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "user selects \"Profile Admin\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "save limited user \"location\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "user enables configuration admin",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "user enable data integration admin",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "verify \"Profile Admin\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 56,
  "name": "verify \"Profile Admin\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "verify \"limited user\" role has \"Configuration Admin\" role enabled",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "verify \"limited user\" role has \"Data Import Admin\" role enabled",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10724846753,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7301104820,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Limited_User_role()"
});
formatter.result({
  "duration": 5226688120,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Profile_Admin_role()"
});
formatter.result({
  "duration": 1115713119,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1097354453,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 49
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1088651507,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3962580961,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10774950493,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6496014367,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 6057530212,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4454435502,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 49
    }
  ],
  "location": "RbacStepDef.verify_role_has_Location_Access(String,String)"
});
formatter.result({
  "duration": 4434936119,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3655642441,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8323556315,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10793751692,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "group",
      "offset": 19
    }
  ],
  "location": "RbacStepDef.save_limited_user_permissions(String)"
});
formatter.result({
  "duration": 3490141174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8334784706,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10861231706,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "location",
      "offset": 19
    }
  ],
  "location": "RbacStepDef.save_limited_user_permissions(String)"
});
formatter.result({
  "duration": 5819417646,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_configuration_admin()"
});
formatter.result({
  "duration": 5177203576,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enable_data_integration_admin()"
});
formatter.result({
  "duration": 5644330832,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3589869800,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10638276745,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6589300675,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5731414249,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4458234953,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4444497689,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limited user",
      "offset": 8
    },
    {
      "val": "Configuration Admin",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.verify_role_has_role_enabled(String,String)"
});
formatter.result({
  "duration": 4441270536,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limited user",
      "offset": 8
    },
    {
      "val": "Data Import Admin",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.verify_role_has_role_enabled(String,String)"
});
formatter.result({
  "duration": 4877392509,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3496744285,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8323626838,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6880819948,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "group",
      "offset": 19
    }
  ],
  "location": "RbacStepDef.save_limited_user_permissions(String)"
});
formatter.result({
  "duration": 3795685093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8275186746,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6853361642,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "location",
      "offset": 19
    }
  ],
  "location": "RbacStepDef.save_limited_user_permissions(String)"
});
formatter.result({
  "duration": 5446079531,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_configuration_admin()"
});
formatter.result({
  "duration": 5150254801,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enable_data_integration_admin()"
});
formatter.result({
  "duration": 5507904269,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3789646169,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10643347382,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6701182961,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5560450588,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4528325745,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4465832798,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limited user",
      "offset": 8
    },
    {
      "val": "Configuration Admin",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.verify_role_has_role_enabled(String,String)"
});
formatter.result({
  "duration": 4468655146,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limited user",
      "offset": 8
    },
    {
      "val": "Data Import Admin",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.verify_role_has_role_enabled(String,String)"
});
formatter.result({
  "duration": 4457315327,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3477394413,
  "status": "passed"
});
formatter.after({
  "duration": 136110,
  "status": "passed"
});
formatter.before({
  "duration": 47603,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 61,
      "value": "#@LmtUserRole"
    }
  ],
  "line": 62,
  "name": "Create Limited User role with Group as All, Location as Limited permissions and Enables Configuration role",
  "description": "",
  "id": "roles-and-permission-for-limited-user;create-limited-user-role-with-group-as-all,-location-as-limited-permissions-and-enables-configuration-role",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "comments": [
    {
      "line": 63,
      "value": "#When user click on Edit Permissions"
    }
  ],
  "line": 64,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "user enables Limited User role",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "user enables Profile Admin role",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "user selects \"Profile Admin\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 69,
  "name": "user selects Location access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 70,
  "name": "user selects \"Profile Admin\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "save limited user \"location\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "user enables configuration admin",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "verify \"Profile Admin\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 78,
  "name": "verify \"Profile Admin\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 79,
  "name": "verify \"limited user\" role has \"Configuration Admin\" role enabled",
  "keyword": "And "
});
formatter.step({
  "line": 80,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10662099921,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7695470561,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Limited_User_role()"
});
formatter.result({
  "duration": 5236162593,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Profile_Admin_role()"
});
formatter.result({
  "duration": 1153767574,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1104016452,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8370731215,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10698895179,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "location",
      "offset": 19
    }
  ],
  "location": "RbacStepDef.save_limited_user_permissions(String)"
});
formatter.result({
  "duration": 5710987131,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_configuration_admin()"
});
formatter.result({
  "duration": 5206231382,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3506282229,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10678259664,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6540025585,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 6089005182,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4440345622,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4442962746,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limited user",
      "offset": 8
    },
    {
      "val": "Configuration Admin",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.verify_role_has_role_enabled(String,String)"
});
formatter.result({
  "duration": 4478671592,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3623000307,
  "status": "passed"
});
formatter.after({
  "duration": 214744,
  "status": "passed"
});
formatter.before({
  "duration": 95207,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 82,
      "value": "#@LmtUserRole"
    }
  ],
  "line": 83,
  "name": "Create Limited User role with Group as Limited, Location as All permissions and Enables Configuration role",
  "description": "",
  "id": "roles-and-permission-for-limited-user;create-limited-user-role-with-group-as-limited,-location-as-all-permissions-and-enables-configuration-role",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "comments": [
    {
      "line": 84,
      "value": "#When user click on Edit Permissions"
    }
  ],
  "line": 85,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 86,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 87,
  "name": "user enables Limited User role",
  "keyword": "And "
});
formatter.step({
  "line": 88,
  "name": "user enables Profile Admin role",
  "keyword": "And "
});
formatter.step({
  "line": 89,
  "name": "user selects Group access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 90,
  "name": "user selects \"Profile Admin\" role can view cases with \"All Group Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 91,
  "name": "save limited user \"group\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 92,
  "name": "user selects \"Profile Admin\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 93,
  "name": "user enable data integration admin",
  "keyword": "And "
});
formatter.step({
  "line": 94,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 95,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 98,
  "name": "verify \"Profile Admin\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 99,
  "name": "verify \"Profile Admin\" role has Location Access \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 100,
  "name": "verify \"limited user\" role has \"Data Import Admin\" role enabled",
  "keyword": "And "
});
formatter.step({
  "line": 101,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10595654485,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7552648226,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Limited_User_role()"
});
formatter.result({
  "duration": 5212725182,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Profile_Admin_role()"
});
formatter.result({
  "duration": 1147726183,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8382175761,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10862674969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "group",
      "offset": 19
    }
  ],
  "location": "RbacStepDef.save_limited_user_permissions(String)"
});
formatter.result({
  "duration": 3479082039,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 49
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1136565846,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enable_data_integration_admin()"
});
formatter.result({
  "duration": 5531876601,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3530474595,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10647322789,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6561353289,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 6013278861,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4462490691,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 49
    }
  ],
  "location": "RbacStepDef.verify_role_has_Location_Access(String,String)"
});
formatter.result({
  "duration": 4462241743,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limited user",
      "offset": 8
    },
    {
      "val": "Data Import Admin",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.verify_role_has_role_enabled(String,String)"
});
formatter.result({
  "duration": 4468452039,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3778456566,
  "status": "passed"
});
formatter.after({
  "duration": 69113,
  "status": "passed"
});
formatter.uri("MPRoles.feature");
formatter.feature({
  "line": 2,
  "name": "Roles and permission for new user",
  "description": "",
  "id": "roles-and-permission-for-new-user",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    }
  ]
});
formatter.before({
  "duration": 663978,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "roles-and-permission-for-new-user;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@MPRoles"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 9698224149,
  "status": "passed"
});
formatter.after({
  "duration": 55714,
  "status": "passed"
});
formatter.before({
  "duration": 732387,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Create Manager Portal User role with Group and Location access",
  "description": "",
  "id": "roles-and-permission-for-new-user;create-manager-portal-user-role-with-group-and-location-access",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@MPRoles"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user enables Manager Portal User role",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user selects \"Manager Portal\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user selects \"Manager Portal\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "verify \"manager portal\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "verify \"manager portal\" role has Location Access \"All\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 21,
      "value": "#"
    }
  ],
  "line": 22,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "user selects Group access as Limited and edit details for \"Manager Portal\" role",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "user selects \"Manager Portal\" role can view cases with \"only Group selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "save manager portal \"group\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "user selects Location access as Limited and edit details for \"Manager Portal\" role",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "user selects \"Manager Portal\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "save manager portal \"location\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "verify \"manager portal\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "verify \"manager portal\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "user selects Group access as Limited and edit details for \"Manager Portal\" role",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "user selects \"Manager Portal\" role can view cases with \"All Group Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "save manager portal \"group\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "user selects Location access as Limited and edit details for \"Manager Portal\" role",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "user selects \"Manager Portal\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "save manager portal \"location\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "verify \"manager portal\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "verify \"manager portal\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10728797829,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7356212199,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Manager_Portal_User_role()"
});
formatter.result({
  "duration": 5208633763,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 47
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1097588239,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 50
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1132784378,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3641204526,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10689770150,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6541507989,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5563498612,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 47
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4456716230,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 50
    }
  ],
  "location": "RbacStepDef.verify_role_has_Location_Access(String,String)"
});
formatter.result({
  "duration": 4438836419,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3402760852,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8333562534,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10876433037,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "group",
      "offset": 21
    }
  ],
  "location": "RbacStepDef.save_manager_portal_permission(String)"
});
formatter.result({
  "duration": 3483491164,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8301608358,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10809764396,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "location",
      "offset": 21
    }
  ],
  "location": "RbacStepDef.save_manager_portal_permission(String)"
});
formatter.result({
  "duration": 3495291502,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3538295292,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10693455706,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6538553409,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5588476257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4434225949,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 63
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4871637442,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3441911947,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8355839835,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6968908678,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "group",
      "offset": 21
    }
  ],
  "location": "RbacStepDef.save_manager_portal_permission(String)"
});
formatter.result({
  "duration": 3566866107,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8282944679,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6907000722,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "location",
      "offset": 21
    }
  ],
  "location": "RbacStepDef.save_manager_portal_permission(String)"
});
formatter.result({
  "duration": 3551479652,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3716059530,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10671798658,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6917052430,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5574794001,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4426861890,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 63
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4464995684,
  "status": "passed"
});
formatter.after({
  "duration": 999318,
  "status": "passed"
});
formatter.before({
  "duration": 166083,
  "status": "passed"
});
formatter.scenario({
  "line": 51,
  "name": "Create Manager Portal User role with Group as ALL Access and Location access as Limited",
  "description": "",
  "id": "roles-and-permission-for-new-user;create-manager-portal-user-role-with-group-as-all-access-and-location-access-as-limited",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 50,
      "name": "@MPRoles"
    }
  ]
});
formatter.step({
  "line": 52,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "user enables Manager Portal User role",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "user selects \"Manager Portal\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "user selects Location access as Limited and edit details for \"Manager Portal\" role",
  "keyword": "When "
});
formatter.step({
  "line": 57,
  "name": "user selects \"Manager Portal\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "save manager portal \"location\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "verify \"manager portal\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 64,
  "name": "verify \"manager portal\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10611174582,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7449294695,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Manager_Portal_User_role()"
});
formatter.result({
  "duration": 5207157002,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 47
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1128699307,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8360118493,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10767331139,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "location",
      "offset": 21
    }
  ],
  "location": "RbacStepDef.save_manager_portal_permission(String)"
});
formatter.result({
  "duration": 3535265605,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3505008223,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10763291203,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6577446741,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5825161075,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 47
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4569711023,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 63
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4443316774,
  "status": "passed"
});
formatter.after({
  "duration": 183008,
  "status": "passed"
});
formatter.before({
  "duration": 72287,
  "status": "passed"
});
formatter.scenario({
  "line": 67,
  "name": "Create Manager Portal User role with Group Access is Limited and Location access All",
  "description": "",
  "id": "roles-and-permission-for-new-user;create-manager-portal-user-role-with-group-access-is-limited-and-location-access-all",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 66,
      "name": "@MPRoles"
    }
  ]
});
formatter.step({
  "line": 68,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 69,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "user enables Manager Portal User role",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "user selects Group access as Limited and edit details for \"Manager Portal\" role",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "user selects \"Manager Portal\" role can view cases with \"only Group selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "save manager portal \"group\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 76,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 78,
  "name": "verify \"manager portal\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 79,
  "name": "verify \"manager portal\" role has Location Access \"All\"",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10695998428,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7359699232,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Manager_Portal_User_role()"
});
formatter.result({
  "duration": 5726026257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8369688174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager Portal",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10784824481,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "group",
      "offset": 21
    }
  ],
  "location": "RbacStepDef.save_manager_portal_permission(String)"
});
formatter.result({
  "duration": 3491394376,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3950999952,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10695481139,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6523991374,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5570829525,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 60
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4913904968,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manager portal",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 50
    }
  ],
  "location": "RbacStepDef.verify_role_has_Location_Access(String,String)"
});
formatter.result({
  "duration": 4454946092,
  "status": "passed"
});
formatter.after({
  "duration": 108253,
  "status": "passed"
});
formatter.uri("OtherRoles.feature");
formatter.feature({
  "line": 1,
  "name": "Other permissions for RBAC roles",
  "description": "",
  "id": "other-permissions-for-rbac-roles",
  "keyword": "Feature"
});
formatter.before({
  "duration": 109312,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "other-permissions-for-rbac-roles;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@otherPerms"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 10313862866,
  "status": "passed"
});
formatter.after({
  "duration": 40198,
  "status": "passed"
});
formatter.before({
  "duration": 33499,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Create role for all other permissions like Manager case approver, Case Shredding",
  "description": "",
  "id": "other-permissions-for-rbac-roles;create-role-for-all-other-permissions-like-manager-case-approver,-case-shredding",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@otherPerms"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user enables configuration admin role",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "user enables Data Integration Admin role",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user enables manager case approver",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "verify Configuration Admin permission",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "verify Data Import Admin permission",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "verify Manager Case Approver permission",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10673742284,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7729665620,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_configuration_admin_role()"
});
formatter.result({
  "duration": 1199221350,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Data_Integration_Admin_role()"
});
formatter.result({
  "duration": 1137580326,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_manager_case_approver()"
});
formatter.result({
  "duration": 1148311175,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3521007879,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10747997134,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6807612020,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5577363876,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_Configuration_Admin_permission()"
});
formatter.result({
  "duration": 4446266771,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_Data_Import_Admin_permission()"
});
formatter.result({
  "duration": 4476114412,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.verify_Manager_Case_Approver_permission()"
});
formatter.result({
  "duration": 4445051299,
  "status": "passed"
});
formatter.after({
  "duration": 138226,
  "status": "passed"
});
formatter.uri("ProfileAdminRole.feature");
formatter.feature({
  "line": 1,
  "name": "Roles and permissions for Profile Admin",
  "description": "",
  "id": "roles-and-permissions-for-profile-admin",
  "keyword": "Feature"
});
formatter.before({
  "duration": 87449,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "roles-and-permissions-for-profile-admin;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 2,
      "name": "@ProfileAdmin"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 10790342941,
  "status": "passed"
});
formatter.after({
  "duration": 71934,
  "status": "passed"
});
formatter.before({
  "duration": 25388,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Role is a profile admin",
  "description": "",
  "id": "roles-and-permissions-for-profile-admin;role-is-a-profile-admin",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@ProfileAdmin"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user enables Profile admin role",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user selects \"Profile Admin\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user selects \"Profile Admin\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "verify \"Profile Admin\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "verify \"Profile Admin\" role has Location Access \"All\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 19,
      "value": "#"
    }
  ],
  "line": 20,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "user selects Group access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user selects \"Profile Admin\" role can view cases with \"only Group selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "save profile admin \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "user selects Location access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "user selects \"Profile Admin\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "save profile admin \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "verify \"Profile Admin\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "verify \"Profile Admin\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 33,
      "value": "#"
    }
  ],
  "line": 34,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "user selects Group access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "user selects \"Profile Admin\" role can view cases with \"All Group Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "save profile admin \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "user selects Location access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "user selects \"Profile Admin\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "save profile admin \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "user enables configuration admin roles",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "user enables Data Integration Admin role",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "verify \"Profile Admin\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "verify \"Profile Admin\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "user click on cancel",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10795632200,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7419100784,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Profile_admin_role()"
});
formatter.result({
  "duration": 1179817880,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1141991569,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 49
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1112971167,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3524403230,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10670961192,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6811248209,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5816399594,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4428431389,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 49
    }
  ],
  "location": "RbacStepDef.verify_role_has_Location_Access(String,String)"
});
formatter.result({
  "duration": 4451682972,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3494488592,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8403166714,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10789587988,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 20
    }
  ],
  "location": "RbacStepDef.save_profile_admin_permissions(String)"
});
formatter.result({
  "duration": 1098751523,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8438685852,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10791712155,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 20
    }
  ],
  "location": "RbacStepDef.save_profile_admin_permissions(String)"
});
formatter.result({
  "duration": 3461998437,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 4012495698,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10702732008,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6534984922,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5533906619,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4786931016,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4441468355,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3452108933,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8426024776,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6975503326,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 20
    }
  ],
  "location": "RbacStepDef.save_profile_admin_permissions(String)"
});
formatter.result({
  "duration": 1090938936,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8454495094,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6965977371,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 20
    }
  ],
  "location": "RbacStepDef.save_profile_admin_permissions(String)"
});
formatter.result({
  "duration": 3463495650,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_configuration_admin_roles()"
});
formatter.result({
  "duration": 1197131740,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Data_Integration_Admin_role()"
});
formatter.result({
  "duration": 1172802913,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3520878116,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10663533309,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6637150486,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5884601768,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4470133317,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4431082719,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_cancel()"
});
formatter.result({
  "duration": 3817648564,
  "status": "passed"
});
formatter.after({
  "duration": 49367,
  "status": "passed"
});
formatter.before({
  "duration": 32088,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 51,
      "value": "#@ProfileAdmin"
    }
  ],
  "line": 52,
  "name": "Create Profile Admin User role with Group as ALL and Location access as Limited",
  "description": "",
  "id": "roles-and-permissions-for-profile-admin;create-profile-admin-user-role-with-group-as-all-and-location-access-as-limited",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 53,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 54,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "user enables Profile admin role",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "user selects \"Profile Admin\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "user selects Location access as Limited and edit details for \"Profile Admin\" role",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "user selects \"Profile Admin\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "save profile admin \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "user go to administration tab and select roles",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "verify \"Profile Admin\" role has Group Access \"All\"",
  "keyword": "Then "
});
formatter.step({
  "line": 65,
  "name": "verify \"Profile Admin\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10596379819,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7460273786,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_Profile_admin_role()"
});
formatter.result({
  "duration": 1181190619,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1200065516,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8420119143,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 55
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10719995092,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 20
    }
  ],
  "location": "RbacStepDef.save_profile_admin_permissions(String)"
});
formatter.result({
  "duration": 3417801742,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3534863974,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10699299984,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6530796182,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5928757208,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.verify_role_has_Group_Access(String,String)"
});
formatter.result({
  "duration": 4456638655,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profile Admin",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4445154263,
  "status": "passed"
});
formatter.after({
  "duration": 90623,
  "status": "passed"
});
formatter.uri("ViewCasesRoles.feature");
formatter.feature({
  "line": 2,
  "name": "Roles and permissions to view cases",
  "description": "",
  "id": "roles-and-permissions-to-view-cases",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    }
  ]
});
formatter.before({
  "duration": 69113,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login into HR Acuity site",
  "description": "",
  "id": "roles-and-permissions-to-view-cases;login-into-hr-acuity-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@ViewCases"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User is in HRAcuity demo",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonStepDef.user_is_in_HRAcuity_demo()"
});
formatter.result({
  "duration": 9878322360,
  "status": "passed"
});
formatter.after({
  "duration": 57829,
  "status": "passed"
});
formatter.before({
  "duration": 44430,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Create User role who can view ALL Cases and Case Type is ER, PH",
  "description": "",
  "id": "roles-and-permissions-to-view-cases;create-user-role-who-can-view-all-cases-and-case-type-is-er,-ph",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@ViewCases"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "user creates an active new role",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user enables view cases",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "user selects option view cases \"Case type Limited\" and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user selects Case Type options for View Case",
  "rows": [
    {
      "cells": [
        "ER",
        "PH"
      ],
      "line": 15
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "save view case \"Case Type\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user selects \"View Cases\" Group access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "user selects \"View Cases\" Location access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user selects \"View Cases\" Issue access as \"All\"",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "verify \"View Cases\" role has Limited Group Access with \"All\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "verify \"View Cases\" role has Limited Issue Access with \"All\" option",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "verify \"View Cases\" role has Limited Location Access with \"All\" option",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 27,
      "value": "#"
    }
  ],
  "line": 28,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "user selects option view cases \"Case type Limited\" and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "user selects Case Type options for View Case",
  "rows": [
    {
      "cells": [
        "ER",
        "INV",
        "EI"
      ],
      "line": 31
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "save view case \"Case Type\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "user selects Group access as Limited and edit details for \"View Cases\" role",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "user selects \"View Cases\" role can view cases with \"only Group selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "save view case \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "user selects Issue access as Limited and edit details for \"View Cases\" role",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "user selects \"View Cases\" role can view cases with \"only Issue selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "save view case \"Issue access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "user selects Location access as Limited and edit details for \"View Cases\" role",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "user selects \"View Cases\" role can view cases with \"only Location selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "save view case \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "verify Case Type permissions for \"View Cases\" role",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "verify \"View Cases\" role has Limited Group Access with \"only Group selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "verify \"View Cases\" role has Limited Issue Access with \"only Issue selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "verify \"View Cases\" role has Limited Location Access with \"only Location selected\" option",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 50,
      "value": "#"
    }
  ],
  "line": 51,
  "name": "user click on Edit Permissions",
  "keyword": "When "
});
formatter.step({
  "line": 52,
  "name": "user selects option view cases \"Case type Limited\" and edit details",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "user selects Case Type options for View Case",
  "rows": [
    {
      "cells": [
        "ER",
        "INV"
      ],
      "line": 54
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "save view case \"Case Type\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "user selects Group access as Limited and edit details for \"View Cases\" role",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "user selects \"View Cases\" role can view cases with \"All Group Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "save view case \"Group access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "user selects Issue access as Limited and edit details for \"View Cases\" role",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "user selects \"View Cases\" role can view cases with \"All Issues Except selected\"",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "save view case \"Issue access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "user selects Location access as Limited and edit details for \"View Cases\" role",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "user selects \"View Cases\" role can view cases with \"All Location Except selected\"",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "save view case \"Location access\" permissions",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "user saves role permissions",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "user go to administration tab and select roles",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "user search for the role",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "user click on quick view permissions",
  "keyword": "And "
});
formatter.step({
  "line": 69,
  "name": "verify \"View Cases\" role has Limited Group Access with \"All Group Except selected\" option",
  "keyword": "Then "
});
formatter.step({
  "line": 70,
  "name": "verify \"View Cases\" role has Limited Issue Access with \"All Issues Except selected\" option",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "verify \"View Cases\" role has Limited Location Access with \"All Location Except selected\" option",
  "keyword": "And "
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10736264500,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_creates_an_active_new_role()"
});
formatter.result({
  "duration": 7338897281,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_enables_view_cases()"
});
formatter.result({
  "duration": 1181993881,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case type Limited",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.user_selects_option_view_cases_and_edit_details(String)"
});
formatter.result({
  "duration": 6324518254,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_Case_Type_options_for_View_Case(DataTable)"
});
formatter.result({
  "duration": 2328387171,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Type",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3516114609,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 43
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as(String,String)"
});
formatter.result({
  "duration": 1105849709,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 46
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as(String,String)"
});
formatter.result({
  "duration": 1107901588,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "All",
      "offset": 43
    }
  ],
  "location": "RbacStepDef.user_selects_Issue_access_as(String,String)"
});
formatter.result({
  "duration": 1115346044,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 4005112950,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10600206775,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6555621142,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 6115267058,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4499399493,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Issue_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4471940834,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "All",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4409527930,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3473721551,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case type Limited",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.user_selects_option_view_cases_and_edit_details(String)"
});
formatter.result({
  "duration": 6379190857,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_Case_Type_options_for_View_Case(DataTable)"
});
formatter.result({
  "duration": 5692517391,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Type",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3536884471,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8298506737,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "only Group selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10833629532,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3656760238,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Issue_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 7346181649,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "only Issue selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 8587634558,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Issue access",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3519301212,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8300461647,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "only Location selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 10805032624,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 6220524722,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3552328403,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10653793668,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6500948893,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5548504268,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 34
    }
  ],
  "location": "RbacStepDef.verify_Case_Type_permissions_for_role(String)"
});
formatter.result({
  "duration": 412949375,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "only Group selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4445040720,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "only Issue selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Issue_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4610095926,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "only Location selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4584244144,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_Edit_Permissions()"
});
formatter.result({
  "duration": 3473035005,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case type Limited",
      "offset": 32
    }
  ],
  "location": "RbacStepDef.user_selects_option_view_cases_and_edit_details(String)"
});
formatter.result({
  "duration": 6362954947,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_selects_Case_Type_options_for_View_Case(DataTable)"
});
formatter.result({
  "duration": 5630542086,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case Type",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3518837519,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Group_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8316727529,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "All Group Except selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6959530469,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group access",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3957194027,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.user_selects_Issue_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 7311717543,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "All Issues Except selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 4677557956,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Issue access",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 3484659034,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 62
    }
  ],
  "location": "RbacStepDef.user_selects_Location_access_as_Limited_and_edit_details_for_role(String)"
});
formatter.result({
  "duration": 8637598310,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 14
    },
    {
      "val": "All Location Except selected",
      "offset": 52
    }
  ],
  "location": "RbacStepDef.user_selects_role_can_view_cases_with(String,String)"
});
formatter.result({
  "duration": 6933814092,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Location access",
      "offset": 16
    }
  ],
  "location": "RbacStepDef.save_view_case_permissions(String)"
});
formatter.result({
  "duration": 5820205745,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_saves_role_permissions()"
});
formatter.result({
  "duration": 3553623213,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_go_to_administration_tab_and_select_roles()"
});
formatter.result({
  "duration": 10747054235,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDef.user_search_for_the_role()"
});
formatter.result({
  "duration": 6561059911,
  "status": "passed"
});
formatter.match({
  "location": "RbacStepDef.user_click_on_quick_view_permissions()"
});
formatter.result({
  "duration": 5883935674,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "All Group Except selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Group_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4466917801,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "All Issues Except selected",
      "offset": 56
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Issue_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4445669084,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Cases",
      "offset": 8
    },
    {
      "val": "All Location Except selected",
      "offset": 59
    }
  ],
  "location": "RbacStepDef.verify_role_has_Limited_Location_Access_with_option(String,String)"
});
formatter.result({
  "duration": 4461412388,
  "status": "passed"
});
formatter.after({
  "duration": 90975,
  "status": "passed"
});
});